package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.comparator.Sequenced;
import org.springframework.util.StringUtils;

public class JSONQuestionOptionMapper implements Sequenced {
	
	Integer sequenceNumber;
	Double quantifier;
	String text;
	
	
	public JSONQuestionOptionMapper() {
		
	}
	
	public JSONQuestionOptionMapper(Integer sequenceNumber, Double quantifier, String text) {
		this.sequenceNumber = sequenceNumber;
		this.quantifier = quantifier;
		this.text = text;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public Double getQuantifier() {
		return quantifier;
	}
	public void setQuantifier(Double quantifier) {
		this.quantifier = quantifier;
	}
	public String getText() {
//		return Utility.decode(text);
		return text;
	}
	public void setText(String text) {
		if (!StringUtils.isEmpty(text)){
			this.text = text;
		}
	}
	
	

}

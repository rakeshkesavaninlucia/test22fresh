package org.birlasoft.thirdeye.service.impl;

import java.util.Date;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.repositories.BenchmarkItemRepository;
import org.birlasoft.thirdeye.repositories.BenchmarkItemScoreRepository;
import org.birlasoft.thirdeye.service.BenchmarkItemScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation class for Benchmark Item Score Service.
 */
@Service
@Transactional(value = TransactionManagers.TENANTTRANSACTIONMANAGER)
public class BenchmarkItemScoreServiceImpl implements BenchmarkItemScoreService {

	@Autowired
	private BenchmarkItemRepository benchmarkItemRepository;
	@Autowired
	private BenchmarkItemScoreRepository benchmarkItemScoreRepository;

	@Override
	public BenchmarkItemScore findById(Integer idOfBenchmarkItemScore) {
		BenchmarkItemScore benchmarkItem = benchmarkItemScoreRepository.findOne(idOfBenchmarkItemScore);
		benchmarkItem.getBenchmarkItem().getId();
		return benchmarkItem;
	}

	@Override
	public BenchmarkItemScore saveOrUpdate(BenchmarkItemScore benchmarkItemScoreBean) {
		return benchmarkItemScoreRepository.save(benchmarkItemScoreBean);
	}

	@Override
	public void deleteBenchmarkItemScore(Integer idOfBenchmarkItemScore) {
		BenchmarkItemScore benchmarkItemScore = benchmarkItemScoreRepository.findOne(idOfBenchmarkItemScore);
		benchmarkItemRepository.delete(benchmarkItemScore);
	}

	@Override
	public Set<BenchmarkItemScore> findByBenchmarkItemId(Integer idOfBenchmarkItem) {
		return benchmarkItemScoreRepository.findByBenchmarkItemId(idOfBenchmarkItem);
	}

	@Override
	public BenchmarkItemScore findByBenchmarkItemAndCurrentDate(BenchmarkItem benchmarkItem, Date currentDate) {
		return benchmarkItemScoreRepository.findBISByBenchmarkItemAndCurrentDate(benchmarkItem, currentDate);
	}
}

package org.birlasoft.thirdeye.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.BenchmarkItemRepository;
import org.birlasoft.thirdeye.repositories.BenchmarkItemScoreRepository;
import org.birlasoft.thirdeye.repositories.BenchmarkRepository;
import org.birlasoft.thirdeye.service.BenchmarkService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation class for Benchmark Service.
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class BenchmarkServiceImpl implements BenchmarkService {
	
	private static Logger logger = LoggerFactory.getLogger(BenchmarkServiceImpl.class);
	
	@Autowired
	private BenchmarkRepository benchmarkRepository;
	@Autowired
	private BenchmarkItemRepository benchmarkItemRepository;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private BenchmarkItemScoreRepository benchmarkItemScoreRepository;

	@Override
	public List<Benchmark> findByWorkspaceIn(Set<Workspace> setOfWorkspace) {
		return benchmarkRepository.findByWorkspaceIn(setOfWorkspace);
	}

	@Override
	public Benchmark findOne(Integer benchmarkId) {
		return benchmarkRepository.findOne(benchmarkId);
	}
	
	@Override
	public List<Benchmark> findAllBenchmark(Integer benchmarkId) {
		return benchmarkRepository.findAll();
	}

	@Override
	public List<BenchmarkItem> findByBenchmark(Benchmark benchmark) {
		return benchmarkItemRepository.findByBenchmark(benchmark);
	}

	@Override
	public List<BenchmarkItem> findByIdIn(Set<Integer> setOfBenchmarkItemIds) {
		return benchmarkItemRepository.findByIdIn(setOfBenchmarkItemIds);
	}
	@Override
	public Benchmark saveUpdateBenchmark(Benchmark benchmark) {
		Benchmark benchmarkDB = new Benchmark();
		if(benchmark.getId() != null ){
			benchmarkDB =  benchmarkRepository.findOne(benchmark.getId());
			benchmarkDB.setName(benchmark.getName());
		}else{
			benchmarkDB.setWorkspace(benchmark.getWorkspace());
			benchmarkDB.setName(benchmark.getName());
			benchmarkDB.setUserByCreatedBy(customUserDetailsService.getCurrentUser());		
			benchmarkDB.setCreatedDate(new Date());
		}
		benchmarkDB.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		benchmarkDB.setUpdatedDate(new Date());
		return benchmarkRepository.save(benchmarkDB);
	}
	@Override 
	public  Benchmark save(Benchmark benchmark) {	
		return benchmarkRepository.save(benchmark);
	
    }

	@Override
	public List<Benchmark> findByWorkspaceIsNullOrWorkspaceIn(Set<Workspace> listOfWorkspaces) {	
		return benchmarkRepository.findByWorkspaceIsNullOrWorkspaceIn(listOfWorkspaces);
	}

	@Override
	public Benchmark findfullyLoadedBenchmark(Integer idOfBenchMark) {
		Benchmark benchmark = benchmarkRepository.findOne(idOfBenchMark);		
		if(!benchmark.getBenchmarkItems().isEmpty()){
			for(BenchmarkItem oneBI :benchmark.getBenchmarkItems()){				
				oneBI.getId();
				oneBI.getBaseName();
				oneBI.getVersion();
				oneBI.getDisplayName();
				oneBI.getBenchmark();
			}
		}
		return benchmark;
	}

	@Override
	public void deleteBenchmark(Integer idOfBenchmark) {
	     Benchmark benchmark = benchmarkRepository.findOne(idOfBenchmark);
	     if(!benchmark.getBenchmarkItems().isEmpty()){
	    	 
	     }
	}

	@Override
	public List<BenchmarkItemScore> findByBenchmarkItemAndCurrentDate(BenchmarkItem benchmarkItem, Date currentDate) {
		return benchmarkItemScoreRepository.findByBenchmarkItemAndCurrentDate(benchmarkItem, currentDate);
	}

	@Override
	public List<Benchmark> findByWorkspaceIsNull() {
		return benchmarkRepository.findByWorkspaceIsNull();
	}
}

package org.birlasoft.thirdeye.beans.php;

import java.math.BigDecimal;

/**
 * 
 * PHPValueWrapper bean
 *
 */
public class PHPValueWrapper {

	private String rowDesc;
	private String colDesc;
	private BigDecimal value;
	private String color;
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getRowDesc() {
		return rowDesc;
	}
	
	public void setRowDesc(String rowDesc) {
		this.rowDesc = rowDesc;
	}
	
	public String getColDesc() {
		return colDesc;
	}
	
	public void setColDesc(String colDesc) {
		this.colDesc = colDesc;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
}

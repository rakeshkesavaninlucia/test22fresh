package org.birlasoft.thirdeye.colorscheme.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.colorscheme.bean.CSSBean;

/**
 * Service {@code interface} for colorizer
 * @author shaishav.dixit
 *
 */
public interface Colorizer {

	/**
	 * Color any bean which is extended by {@link CSSBean} based on the parameter evaluation value 
	 * @param param
	 * @param mapOfBeanAndValue
	 * @return
	 */
	public List<? extends CSSBean> getParameterColor(ParameterBean param, Map<? extends CSSBean, BigDecimal> mapOfBeanAndValue);
	
	/**
	 * Colors an asset based on the {@link QualityGate} selected. It will evaluate the parameters which are defined in 
	 * the quality gate and then calculates the weighted average to color the asset.
	 * @param qualityGate
	 * @param listOfBeans
	 * @return
	 */
	public List<? extends CSSBean> getQualityGateColor(QualityGate qualityGate, List<AssetBean> listOfBeans);
	
	
	/**
	 * This method is used to get the color for a parameter based on its aggregate value.
	 * 
	 * @author dhruv.sood
	 *  
	 * @param parmeterId
	 * @param aggregateValue
	 * @return
	 */
	public String getHomePageParameterColor(Integer parmeterId, BigDecimal aggregateValue);
}

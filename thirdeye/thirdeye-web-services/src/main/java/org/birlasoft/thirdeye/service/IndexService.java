package org.birlasoft.thirdeye.service;


/**
 * Service interface for Index.
 * @author sunil1.gupta
 */
public interface IndexService {

	public String buildIndexForTenant();
	
	
}

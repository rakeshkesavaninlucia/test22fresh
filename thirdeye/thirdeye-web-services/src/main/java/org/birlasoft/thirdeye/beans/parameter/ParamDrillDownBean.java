package org.birlasoft.thirdeye.beans.parameter;

import java.math.BigDecimal;
import java.util.List;


public class ParamDrillDownBean {
	private String title;
	private String subtitle;
	private List<Integer>ranges;
	private List<BigDecimal> measures;
	private List<BigDecimal> markers;
	private Integer parameterId;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public List<Integer> getRanges() {
		return ranges;
	}
	public void setRanges(List<Integer> ranges) {
		this.ranges = ranges;
	}
	public List<BigDecimal> getMeasures() {
		return measures;
	}
	public void setMeasures(List<BigDecimal> measures) {
		this.measures = measures;
	}
	public List<BigDecimal> getMarkers() {
		return markers;
	}
	public void setMarkers(List<BigDecimal> markers) {
		this.markers = markers;
	}
	public Integer getParameterId() {
		return parameterId;
	}
	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}	
}

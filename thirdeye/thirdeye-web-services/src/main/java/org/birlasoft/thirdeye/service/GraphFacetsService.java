package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.birlasoft.thirdeye.beans.widgets.BarChartConfig;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;


/**
 * Service for graph facets (Pie or Bar chart).
 * @author samar.gupta
 *
 */
public interface GraphFacetsService {
	/**
	 * Get map for plotting pie or Bar chart.
	 * @param idOfQuestionnaireQuestionId
	 * @return {@code Map<String, Integer>}
	 */
	public Map<String, Integer> getMapForPlotPieOrBarChart(Questionnaire qe, Question q, Optional<BarChartConfig> optionalConfig,Map<String,List<String>> filterMap);
}

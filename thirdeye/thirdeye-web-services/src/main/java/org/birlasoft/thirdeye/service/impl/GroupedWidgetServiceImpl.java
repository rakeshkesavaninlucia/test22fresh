package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.beans.widgets.GroupedDiscreteBarWrapper;
import org.birlasoft.thirdeye.beans.widgets.GroupedWidgetJSONConfig;
import org.birlasoft.thirdeye.beans.widgets.WidgetHelper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.AssetQuestionBean;
import org.birlasoft.thirdeye.search.api.beans.AssetQuestionWrapper;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.GroupedWidgetService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.QuestionSearchService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class GroupedWidgetServiceImpl implements GroupedWidgetService {
	
	@Autowired
	private ParameterSearchService parameterSearchService;
	@Autowired
	private QuestionSearchService questionSearchService;
	@Autowired
	private DashboardService dashboardService;
	
	@Override
	public void saveGroupWidget(GroupedWidgetJSONConfig widgetJSONConfig) {
		Integer widgetId = widgetJSONConfig.getId();
		if (widgetId != null && widgetId > 0){
			Widget w = dashboardService.findOneWidget(widgetId, false);
			WidgetHelper wh = new WidgetHelper();
			GroupedWidgetJSONConfig currentConfig = wh.getWidgetBeanFromWidget(w);
			currentConfig.setQuestionnaireId(widgetJSONConfig.getQuestionnaireId());
			currentConfig.setFieldOne(widgetJSONConfig.getFieldOne());
			currentConfig.setFieldTwo(widgetJSONConfig.getFieldTwo());
			currentConfig.setFieldOneId(widgetJSONConfig.getFieldOneId());
			currentConfig.setFieldTwoId(widgetJSONConfig.getFieldTwoId());
			w.setWidgetConfig(Utility.convertObjectToJSONString(currentConfig));
			dashboardService.saveWidget(w);
		}
	}

	@Override
	public List<GroupedDiscreteBarWrapper> getDataForGroupedGraph(GroupedWidgetJSONConfig widgetJSONConfig) {
		
		Map<String, List<Integer>> bucketOne = prepareBucketForField(widgetJSONConfig.getFieldOne(), widgetJSONConfig.getFieldOneId(), widgetJSONConfig.getQuestionnaireId());
		
		Map<String, List<Integer>> bucketTwo = prepareBucketForField(widgetJSONConfig.getFieldTwo(), widgetJSONConfig.getFieldTwoId(), widgetJSONConfig.getQuestionnaireId());
		
		Map<String, Map<String, List<Integer>>> requiredMap = new HashMap<>();
		if(!bucketOne.isEmpty() && !bucketTwo.isEmpty()) {			
			for (Entry<String, List<Integer>> entryOuter : bucketTwo.entrySet()) {
				Map<String, List<Integer>> map = new HashMap<>();
				for (Entry<String, List<Integer>> entryInner : bucketOne.entrySet()) {
					List<Integer> intersect = entryInner.getValue().stream()
							.filter(entryOuter.getValue()::contains)
							.collect(Collectors.toList());
					map.put(entryInner.getKey(), intersect);
				}
				requiredMap.put(entryOuter.getKey(), map);
			}
		} else if(bucketOne.isEmpty() || bucketTwo.isEmpty()) {
			return new ArrayList<>();
		}
		
		return prepareDiscreteBarWrapper(requiredMap);
	}

	private Map<String, List<Integer>> prepareBucketForField(String field, Integer fieldId, Integer questionnaireId) {
		AssetQuestionWrapper assetQuestionWrapper;
		AssetParameterWrapper assetParameterWrapper;
		Map<String, List<Integer>> bucket = new HashMap<>();
		if(field != null && fieldId != null && field.equals("question")) {
			assetQuestionWrapper = questionSearchService.getQuestionResponseFromElasticsearch(fieldId, questionnaireId, new HashSet<>(), null);
			bucket = createBucketFromQuestion(assetQuestionWrapper);
		} else if(field != null && fieldId != null && field.equals("parameter")) {
			assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(fieldId, questionnaireId, new HashSet<>(), null);
			bucket = createBucketFromParameter(assetParameterWrapper);
		}
		return bucket;
	}

	private List<GroupedDiscreteBarWrapper> prepareDiscreteBarWrapper(Map<String, Map<String, List<Integer>>> requiredMap) {
		List<GroupedDiscreteBarWrapper> discreteBarWrappers = new ArrayList<>();
		for (Entry<String, Map<String, List<Integer>>> entry : requiredMap.entrySet()) {
			GroupedDiscreteBarWrapper barWrapper = new GroupedDiscreteBarWrapper();
			barWrapper.setLabel(entry.getKey());
			for (Entry<String, List<Integer>> oneEntry : entry.getValue().entrySet()) {
				barWrapper.put(oneEntry.getKey(), oneEntry.getValue().size());
			}
			discreteBarWrappers.add(barWrapper);
		}
		return discreteBarWrappers;
		
	}

	private Map<String, List<Integer>> createBucketFromParameter(AssetParameterWrapper assetParameterWrapper) {
		return assetParameterWrapper.getValues().stream()
					.collect(Collectors.groupingBy(AssetParameterBean::getDescription, Collectors.mapping(AssetParameterBean::getAssetId, Collectors.toList())));
	}

	private Map<String, List<Integer>> createBucketFromQuestion(AssetQuestionWrapper assetQuestionWrapper) {
		return assetQuestionWrapper.getResponse().stream()
					.collect(Collectors.groupingBy(AssetQuestionBean::getResponse, Collectors.mapping(AssetQuestionBean::getAssetId, Collectors.toList())));
	}

}

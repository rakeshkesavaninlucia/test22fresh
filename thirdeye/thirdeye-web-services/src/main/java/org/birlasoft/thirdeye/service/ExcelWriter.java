package org.birlasoft.thirdeye.service;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * {@code Interface} for excel write
 * @author shaishav.dixit
 *
 */
@FunctionalInterface
public interface ExcelWriter {

	/**
	 * Write data on excel sheet and download on browser
	 * @param response
	 * @param workbook
	 * @param fileName
	 */
	public void writeExcel(HttpServletResponse response, XSSFWorkbook workbook, String fileName);

}

package org.birlasoft.thirdeye.service;

import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.security.UserAuthenticationDetailsBean;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Service class for load user by user name and get current user. 
 */

public interface CustomUserDetailsService extends UserDetailsService {
	
	/**
	 * Get current user.
	 * @return {@code User}
	 */
	public User getCurrentUser();
	
	/**
	 * Returns the {@link UserAuthenticationDetailsBean} for the current authenticated user session.
	 * @return
	 */
	public UserAuthenticationDetailsBean getCurrentAuthenticationDetails();
	
	/**
	 * Returns the active workspace for the user.
	 * 
	 * @return
	 */
	public Workspace getActiveWorkSpaceForUser();
	
}

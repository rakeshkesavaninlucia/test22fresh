package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.search.api.beans.FacetBean;

/**Service class for Filter
 * @author dhruv.sood
 *
 */
public interface FilterService {

	/**Method to a list of facets for Filter
	 * @author dhruv.sood
	 * @param tenantId
	 * @param activeWorkspaceId
	 * @param filterMap
	 * @return
	 */
	public List<FacetBean> getFacets(String tenantId, Integer activeWorkspaceId, Map<String,List<String>> filterMap);
}

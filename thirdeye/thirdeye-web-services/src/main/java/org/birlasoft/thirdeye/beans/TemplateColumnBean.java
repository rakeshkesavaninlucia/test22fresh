package org.birlasoft.thirdeye.beans;

import org.birlasoft.thirdeye.comparator.Sequenced;

/**
 * Bean for asset template column.
 * @author samar.gupta
 *
 */
public class TemplateColumnBean implements Sequenced {
	
	private String name;
	private String data;
	private int sequenceNumber;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Override
	public String toString() {
		return name + " = " + data ;
	}
	
	
}

package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.beans.report.ReportConfigBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.ReportRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ReportService;
import org.birlasoft.thirdeye.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sunil1.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ReportServiceImpl implements ReportService {
	
	@Autowired
	private ReportRepository reportRepository;
	
	
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	public ReportServiceImpl() {
		//Default constructor
	}
	
	@Override
	public Report saveOrUpdateReport(ReportConfigBean reportConfigBean) {
		return reportRepository.save(prepareReportBean(reportConfigBean));
	}
	
	
	
	private  Report prepareReportBean(ReportConfigBean reportConfigBean) {
		Report report = new Report();
		if(reportConfigBean.getId() != null)
		  report.setId(reportConfigBean.getId());
		report.setReportName(reportConfigBean.getReportName());
		report.setDescription(reportConfigBean.getDescription());
		report.setReportType(reportConfigBean.getReportType());
		report.setReportConfig(reportConfigBean.getReportConfig());
		report.setDeletedStatus(reportConfigBean.isDeletedStatus());
		report.setUserByCreatedBy(customUserDetailsService.getCurrentUser());
		report.setCreatedDate(new Date());
		report.setUserByUpdatedBy(customUserDetailsService.getCurrentUser());
		report.setUpdatedDate(new Date());
		report.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		
		return report;
	}
	
	@Override
	public void deleteReport(Integer reportId) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<ReportConfigBean> findAllMySavedAndAssingedReport(Workspace activeWorkSpaceForUser, User currentUser) {
		List<Report> reports = reportRepository.findByWorkspaceAndUserByCreatedBy(activeWorkSpaceForUser,currentUser);
		List<ReportConfigBean> listOfRCB =  reports.stream().map(ReportConfigBean :: new).collect(Collectors.toList());
		boolean status  = !reports.stream().anyMatch(r -> r.isStatus());
		listOfRCB.add(getDefaultReport(status));
		return listOfRCB;
	}

	private ReportConfigBean getDefaultReport(boolean status) {
		ReportConfigBean rcb = new ReportConfigBean();
		rcb.setId(-1);
		rcb.setReportName("Portfolio Snapshot");
		rcb.setReportType("Homepage");
		rcb.setDescription("It is homepage");
		rcb.setReportConfig("");
		rcb.setDeletedStatus(false);
		rcb.setStatus(status);
		return rcb;
	}

	@Override
	public Report findReportById(Integer reportId) {
		return reportRepository.findOne(reportId);
	}	
	
	@Override
	public List<Report> findByWorkspaceIn(Set<Workspace> workspaces) {

		return reportRepository.findByWorkspaceIn(workspaces);
	}

	@Override 
	public List<Report> saveAll(List<Report> reports) {
		return reportRepository.save(reports);
	}

	@Override
	public void activateReportAsHomepage(Integer id) {
		List<Report> reports = new ArrayList<>();
		Report r1 = reportRepository.findByWorkspaceAndStatus(customUserDetailsService.getActiveWorkSpaceForUser(),true);
		 
		if(r1 != null){
			r1.setStatus(false);
			reports.add(r1);
		}
		if(id != -1){
		  Report r2 = reportRepository.findOne(id);
		  r2.setStatus(true);
		  reports.add(r2);
		}
		reportRepository.save(reports);
	}
	@Override
	public Report findByReportName(String reportName) {
		return reportRepository.findByreportName(reportName);
	}
	
}

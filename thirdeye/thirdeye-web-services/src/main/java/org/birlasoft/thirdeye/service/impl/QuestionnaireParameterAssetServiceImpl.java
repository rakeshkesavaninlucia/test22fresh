/**
 * 
 */
package org.birlasoft.thirdeye.service.impl;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameterAsset;
import org.birlasoft.thirdeye.repositories.QuestionnaireParameterAssetRepository;
import org.birlasoft.thirdeye.service.QuestionnaireParameterAssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author dhruv.sood
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionnaireParameterAssetServiceImpl implements QuestionnaireParameterAssetService {

	
	@Autowired
	private QuestionnaireParameterAssetRepository questionnaireParameterAssetRepository;
	
	
	@Override
	public QuestionnaireParameterAsset findByQuestionnaireAndParameterAndAsset(Questionnaire questionnaire, Parameter parameter, Asset asset) {
		return questionnaireParameterAssetRepository.findByQuestionnaireAndParameterAndAsset(questionnaire,parameter,asset);
	}

	@Override
	public QuestionnaireParameterAsset save(QuestionnaireParameterAsset questionnaireParameterAsset) {
		return questionnaireParameterAssetRepository.save(questionnaireParameterAsset);
	}
	
}

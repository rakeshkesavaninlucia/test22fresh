package org.birlasoft.thirdeye.beans;

/**
 * Bean for mandatory questions to show on answered/total.
 * @author samar.gupta
 */
public class MandatoryQuestionBean {
	
	private Integer answered;
	private Integer total;
	
	public Integer getAnswered() {
		return answered;
	}
	public void setAnswered(Integer answered) {
		this.answered = answered;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
}

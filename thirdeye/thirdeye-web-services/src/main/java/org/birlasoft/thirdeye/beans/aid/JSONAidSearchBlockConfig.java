package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.List;

/**
 * @author samar.gupta
 *
 */
public class JSONAidSearchBlockConfig {//extends JSONAIDBlockConfig {
	private int id;
	private int aidId;
	private int sequenceNumber;
//	private AIDBlockType aidBlock;
	private String aidBlockType;
	private String blockTitle;
	private List<JSONAidSearchSubBlockConfig> subBlocks = new ArrayList<>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAidId() {
		return aidId;
	}
	public void setAidId(int aidId) {
		this.aidId = aidId;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	/*public AIDBlockType getAidBlock() {
		return aidBlock;
	}
	public void setAidBlock(AIDBlockType aidBlockType) {
		this.aidBlock = aidBlockType;
	}*/
	public String getBlockTitle() {
		return blockTitle;
	}
	public void setBlockTitle(String blockTitle) {
		this.blockTitle = blockTitle;
	}
	public List<JSONAidSearchSubBlockConfig> getSubBlocks() {
		return subBlocks;
	}
	public void setSubBlocks(List<JSONAidSearchSubBlockConfig> subBlocks) {
		this.subBlocks = subBlocks;
	}
	public String getAidBlockType() {
		return aidBlockType;
	}
	public void setAidBlockType(String aidBlockType) {
		this.aidBlockType = aidBlockType;
	}
}

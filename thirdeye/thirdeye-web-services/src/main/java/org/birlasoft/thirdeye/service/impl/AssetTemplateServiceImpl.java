package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetTemplateBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.RelationshipTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.AIDRepository;
import org.birlasoft.thirdeye.repositories.AssetTemplateRepository;
import org.birlasoft.thirdeye.repositories.FunctionalMapRepository;
import org.birlasoft.thirdeye.repositories.GraphAssetTemplateRepository;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateColumnService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.RelationshipTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} for asset template. Save, update find, delete of asset
 * template and asset
 * 
 * @author shaishav.dixit
 *
 */
@Service("assetTemplateService")
@Transactional(value = TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetTemplateServiceImpl implements AssetTemplateService {

	@Autowired
	private AssetTemplateRepository assetTemplateRepository;
	@Autowired
	private AssetTypeService assetTypeService;
	@Autowired
	private CustomUserDetailsService userDetailsService;
	@Autowired
	private AssetTemplateColumnService assetTemplateColumnService;
	@Autowired
	private RelationshipTemplateService relationshipTemplateService;
	@Autowired
	private AssetService assetService;
	@Autowired
	private AIDRepository aidRepository;
	@Autowired
	private FunctionalMapRepository functionalMapRepository;
	@Autowired
	private GraphAssetTemplateRepository graphAssetTemplateRepository;
	

	/**
	 * Constructor autowiring of dependencies
	 * 
	 * @param assetTemplateRepository
	 * @param assetTypeService
	 * @param userDetailsService
	 * @param assetTemplateColumnService
	 * @param assetService
	 * @param relationshipTemplateService
	 */
	/*
	 * @Autowired public AssetTemplateServiceImpl(AssetTemplateRepository
	 * assetTemplateRepository, AssetTypeService assetTypeService,
	 * CustomUserDetailsService userDetailsService, AssetTemplateColumnService
	 * assetTemplateColumnService, RelationshipTemplateService
	 * relationshipTemplateService) { this.assetTemplateRepository =
	 * assetTemplateRepository; this.assetTypeService = assetTypeService;
	 * this.userDetailsService = userDetailsService;
	 * this.assetTemplateColumnService=assetTemplateColumnService;
	 * this.relationshipTemplateService = relationshipTemplateService; }
	 */

	@Override
	public AssetTemplate save(AssetTemplate assetTemplate) {
		return assetTemplateRepository.save(assetTemplate);
	}

	@Override
	public AssetTemplate findOne(Integer id) {
		return assetTemplateRepository.findOne(id);
	}

	@Override
	public List<AssetTemplate> findAll() {
		return assetTemplateRepository.findAll();
	}

	@Override
	public AssetTemplate findFullyLoadedAssetTemplate(Integer id) {
		AssetTemplate fullyLoadedTemplate = assetTemplateRepository.findOne(id);
		fullyLoadedTemplate.getAssets().size();

		return fullyLoadedTemplate;
	}

	@Override
	public AssetTemplate createNewAssetTemplateObject(AssetTemplateBean assetTemplateBean, User currentUser) {
		AssetTemplate assetTemplate = new AssetTemplate();
		assetTemplate.setAssetTemplateName(assetTemplateBean.getAssetTemplateName());
		assetTemplate.setDescription(assetTemplateBean.getDescription());
		assetTemplate.setCreatedDate(new Date());
		assetTemplate.setUpdatedDate(new Date());
		assetTemplate.setUserByCreatedBy(currentUser);
		assetTemplate.setUserByUpdatedBy(currentUser);
		assetTemplate.setCreatedDate(new Date());
		assetTemplate.setUpdatedDate(new Date());

		Set<AssetTemplateColumn> assetTemplateColumns = new HashSet<>();
		if (!assetTypeService.findOne(assetTemplateBean.getAssetTypeId()).getAssetTypeName()
				.equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())) {
			assetTemplateColumns
					.add(assetTemplateColumnService.createDefaultAssetTemplateColumn(assetTemplate, currentUser));
		}

		assetTemplate.setAssetTemplateColumns(assetTemplateColumns);
		assetTemplate.setAssetType(assetTypeService.findOne(assetTemplateBean.getAssetTypeId()));
		assetTemplate.setWorkspace(userDetailsService.getActiveWorkSpaceForUser());
		return assetTemplate;
	}

	@Override
	public List<AssetTemplate> findByAssetTypeAndUserByCreatedBy(AssetType assetType, User currentuser) {
		return assetTemplateRepository.findByAssetTypeAndUserByCreatedBy(assetType, currentuser);
	}

	@Override
	public List<AssetTemplate> findByWorkspaceIn(List<Workspace> listOfWorkspaces) {
		return assetTemplateRepository.findByWorkspaceIn(listOfWorkspaces);
	}

	@Override
	public List<AssetTemplate> findByIdIn(Set<Integer> idsOfAssetTemplate) {
		return assetTemplateRepository.findByIdIn(idsOfAssetTemplate);
	}

	@Override
	public List<AssetTemplate> listTemplateForActiveWorkspace() {
		Workspace activeWorkspace = userDetailsService.getActiveWorkSpaceForUser();
		List<Workspace> workspaces = new ArrayList<>();
		workspaces.add(activeWorkspace);
		AssetTypes[] assetTypes = new AssetTypes[] { AssetTypes.RELATIONSHIP };
		List<AssetType> listOfAssetTypes = assetTypeService.extractAssetTypes(assetTypes);
		return assetTemplateRepository.findByAssetTypeInAndWorkspaceIn(listOfAssetTypes, workspaces);
	}

	@Override
	public AssetTemplate updateAssetTemplateObject(AssetTemplateBean assetTemplateBean, User currentUser) {
		AssetTemplate templateToUpdate = findOne(assetTemplateBean.getId());
		templateToUpdate.setAssetTemplateName(assetTemplateBean.getAssetTemplateName());
		templateToUpdate.setDescription(assetTemplateBean.getDescription());
		templateToUpdate.setUserByUpdatedBy(currentUser);
		templateToUpdate.setUpdatedDate(new Date());
		return templateToUpdate;
	}

	@Override
	public List<AssetTemplate> findByAssetTemplateNameAndAssetTypeAndUserByCreatedBy(String assetTemplateName,
			AssetType assetType, User currentuser) {
		return assetTemplateRepository.findByAssetTemplateNameAndAssetTypeAndUserByCreatedBy(assetTemplateName,
				assetType, currentuser);
	}

	@Override
	public Map<AssetType, List<AssetTemplate>> getMapOfTemplateByTypeForActiveWorkspace() {
		Map<AssetType, List<AssetTemplate>> mapToReturn = new HashMap<>();
		List<AssetTemplate> listOfAssetTemplate = listTemplateForActiveWorkspace();
		Set<AssetType> setOfAssetType = new HashSet<>();
		for (AssetTemplate assetTemplate : listOfAssetTemplate) {
			setOfAssetType.add(assetTemplate.getAssetType());
		}
		for (AssetType assetType : setOfAssetType) {
			List<AssetTemplate> listToBeAdded = new ArrayList<>();
			for (AssetTemplate assetTemplate : listOfAssetTemplate) {
				if (!assetTemplate.getAssets().isEmpty())
					if (assetTemplate.getAssetType().equals(assetType)) {
						listToBeAdded.add(assetTemplate);
					}
			}
			mapToReturn.put(assetType, listToBeAdded);
		}
		return mapToReturn;
	}

	@Override
	public List<AssetTemplate> findByWorkspaceIn(List<Workspace> listOfWorkspaces, boolean loaded) {
		List<AssetTemplate> listOfAssetTemplates = assetTemplateRepository.findByWorkspaceIn(listOfWorkspaces);

		if (loaded) {
			for (AssetTemplate assetTemplate : listOfAssetTemplates) {
				if (assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())) {
					assetTemplate.getRelationshipTemplates().size();
				}
			}
		}

		return listOfAssetTemplates;
	}

	@Override
	public void checkForViewTemplateColumn(AssetTemplate oneTemplateForViewing) {
		if (oneTemplateForViewing.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())) {
			List<RelationshipTemplate> listOfRelationshipTemplates = relationshipTemplateService
					.findByAssetTemplate(oneTemplateForViewing);
			if (listOfRelationshipTemplates.size() == 1) {
				RelationshipTemplate relationshipTemplate = listOfRelationshipTemplates.get(0);
				if (!relationshipTemplate.isHasAttribute()) {
					throw new AccessDeniedException("User is not allowed to access this template");
				}
			} else {
				throw new AccessDeniedException("User is not allowed to access this template");
			}
		}
	}

	@Override
	public List<AssetTemplateBean> getListOfTemplateBeanWithHasColumn() {
		List<AssetTemplateBean> listOfAssetTemplateBeans = new ArrayList<>();
		List<Workspace> listOfWorkspaces = new ArrayList<>();
		listOfWorkspaces.add(userDetailsService.getActiveWorkSpaceForUser());
		List<AssetTemplate> assetTemplates = this.findByWorkspaceIn(listOfWorkspaces);
		List<RelationshipTemplate> listOfRelationshipTemplates = relationshipTemplateService
				.findByAssetTemplateIn(new HashSet<AssetTemplate>(assetTemplates));
		for (AssetTemplate assetTemplate : assetTemplates) {
			AssetTemplateBean assetTemplateBean = new AssetTemplateBean(assetTemplate);
			if (assetTemplate.getAssetType().getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())) {
				for (RelationshipTemplate relationshipTemplate : listOfRelationshipTemplates) {
					if (relationshipTemplate.getAssetTemplate().getId().equals(assetTemplate.getId())) {
						Set<RelationshipTemplate> relationshipTemplateToAdd = new HashSet<>();
						relationshipTemplateToAdd.add(relationshipTemplate);
						assetTemplate.setRelationshipTemplates(relationshipTemplateToAdd);

						assetTemplateBean.setRelationshipTemplateId(relationshipTemplate.getId());
						assetTemplateBean.setHasTemplateColumn(relationshipTemplate.isHasAttribute());

						break;
					}
				}
			} else {
				assetTemplateBean.setHasTemplateColumn(true);
			}
			listOfAssetTemplateBeans.add(assetTemplateBean);
		}
		return listOfAssetTemplateBeans;
	}

	@Override
	public List<AssetTemplate> findByAssetTypeAndWorkspaceIn(AssetType assetType, List<Workspace> listOfWorkspaces) {
		return assetTemplateRepository.findByAssetTypeAndWorkspaceIn(assetType, listOfWorkspaces);
	}

	@Override
	public boolean isTemplateExpandable(AssetTemplate templateToDelete) {
		if (!assetService.findByAssetTemplate(templateToDelete).isEmpty()) {
			return true;
		}
		if (!aidRepository.findByAssetTemplate(templateToDelete).isEmpty()) {
			return true;
		}
		if (!functionalMapRepository.findByAssetTemplate(templateToDelete).isEmpty()) {
			return true;
		}
		if (!graphAssetTemplateRepository.findByAssetTemplate(templateToDelete).isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public AssetTemplate deleteTemplate(AssetTemplate templateToDelete) {
		templateToDelete.setAssetTemplateColumns(null);
		assetTemplateRepository.delete(templateToDelete);
		return templateToDelete;
	}

	@Override
	public List<AssetTemplate> listRelationshipTemplateForActiveWorkspace() {
		Workspace activeWorkspace = userDetailsService.getActiveWorkSpaceForUser();
		List<Workspace> workspaces = new ArrayList<>();
		workspaces.add(activeWorkspace);
		return findByAssetTypeAndWorkspaceIn(assetTypeService.findByAssetTypeName(AssetTypes.RELATIONSHIP.getValue()),
				workspaces);
	}

	@Override
	public void deleteByAssetTemplateId(AssetTemplate templateToDelete) {
		templateToDelete.setAssetTemplateColumns(null);
		assetTemplateRepository.deleteByAssetTemplateId(templateToDelete.getId());
	}

	@Override
	public List<AssetTemplate> findAssetTemplateByAssetTypeId(Integer assetTypeId) {
		return assetTemplateRepository.findByAssetTypeId(assetTypeId);
	}
}

package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.interfaces.ParameterEvaluator;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.ResponseDataService;
import org.birlasoft.thirdeye.util.Utility;

/**
 * This class for evaluation of TCO.
 * @author samar.gupta
 *
 */
public class TcoParameterEvaluator implements ParameterEvaluator {

	private final Questionnaire questionnaire;
	
	private ParameterService parameterService;
	
	private QuestionnaireQuestionService questionnaireQuestionService;
	
	private QuestionnaireParameterService questionnaireParameterService;
	
	private QuestionnaireAssetService questionnaireAssetService;
	
	private ResponseDataService responseDataService; 
	
	/**
	 * @param questionnaireQuestionService
	 * @param questionnaire
	 * @param questionnaireParameterService
	 * @param questionnaireAssetService
	 * @param responseDataService
	 * @param parameterService
	 */
	public TcoParameterEvaluator(QuestionnaireQuestionService questionnaireQuestionService,
			Questionnaire questionnaire,
			QuestionnaireParameterService questionnaireParameterService,
			QuestionnaireAssetService questionnaireAssetService,
			ResponseDataService responseDataService,
			ParameterService parameterService) {
		this.questionnaire = questionnaire;
		this.questionnaireQuestionService = questionnaireQuestionService;
		this.questionnaireParameterService = questionnaireParameterService;
		this.questionnaireAssetService = questionnaireAssetService;
		this.responseDataService = responseDataService;
		this.parameterService = parameterService;
	}

	@Override
	public List<ParameterEvaluationResponse> evaluateParameter(List<AssetBean> assets) {
		List<ParameterEvaluationResponse> listOfParameterResponses = new ArrayList<>();
		if(!assets.isEmpty()){
		
			// Create map for listOfQp, key is parameterId and value is List<QP>
			Map<Integer, List<QuestionnaireParameter>> mapOfQp = createMapOfQP();
			
			// fetch all questionnaire assets by questionnaire and assets
			Set<QuestionnaireAsset> listOfQa = createSetOfQA(assets);
			
			// fetch QPs by Qe and parent parameter is null (Selected parameter from modal pop-up in QE-Parameter page)
			List<QuestionnaireParameter> listOfSelectedQps = questionnaireParameterService.findByQuestionnaire(questionnaire, true);
			
			for (QuestionnaireParameter oneQp : listOfSelectedQps) {
					ParameterEvaluationResponse parameterEvaluationResponse = new ParameterEvaluationResponse();
					parameterEvaluationResponse.setParameterBean(new ParameterBean(oneQp.getParameterByParameterId()));
					
					Map<QuestionnaireAsset, Set<QuestionnaireQuestion>> mapOfQA = createMapOfQA(mapOfQp, listOfQa, oneQp);
					
					// Create Set for AssetBean
					Map<Integer, AssetBean> mapOfAssetBeansId = createMapOfAssetBean(assets);
					// Calculate response for asset
					for (QuestionnaireAsset qa : listOfQa) {
						evaluateAssetResponseValue(parameterEvaluationResponse, mapOfQA, mapOfAssetBeansId, qa);
					}
					
					// add evaluation response in list
					listOfParameterResponses.add(parameterEvaluationResponse);
			}
		}
		
		return listOfParameterResponses;
	}

	private void evaluateAssetResponseValue(
			ParameterEvaluationResponse parameterEvaluationResponse,
			Map<QuestionnaireAsset, Set<QuestionnaireQuestion>> mapOfQA,
			Map<Integer, AssetBean> mapOfAssetBeansId, QuestionnaireAsset qa) {
		if(mapOfQA.containsKey(qa)){
			Set<QuestionnaireQuestion> setOfQq = mapOfQA.get(qa);
			BigDecimal response = calculateResponse(setOfQq);
			// Add Asset value in Map
			if(mapOfAssetBeansId.containsKey(qa.getAsset().getId()))
				parameterEvaluationResponse.addAssetEvaluation(mapOfAssetBeansId.get(qa.getAsset().getId()), response);
		}
	}

	private Map<Integer, AssetBean> createMapOfAssetBean(List<AssetBean> assets) {
		Map<Integer, AssetBean> mapOfAssetBeansId = new HashMap<>();
		for (AssetBean ab : assets) {
			mapOfAssetBeansId.put(ab.getId(), ab);
		}
		return mapOfAssetBeansId;
	}

	private BigDecimal calculateResponse(Set<QuestionnaireQuestion> setOfQq) {
		BigDecimal response = new BigDecimal(0);
		if(!setOfQq.isEmpty()){
			// fetch all responses by QQs.
			List<ResponseData> listOfResponseDatas = responseDataService.findByQuestionnaireQuestionIn(setOfQq);
			for (ResponseData responseData : listOfResponseDatas) {
				JSONNumberResponseMapper numberResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONNumberResponseMapper.class);
				if(null != numberResponseMapper && null != numberResponseMapper.getResponseNumber())
					response = response.add(BigDecimal.valueOf(numberResponseMapper.getResponseNumber()));
			}
		}
		return response;
	}

	private Map<QuestionnaireAsset, Set<QuestionnaireQuestion>> createMapOfQA(Map<Integer, List<QuestionnaireParameter>> mapOfQp,
			Set<QuestionnaireAsset> listOfQa, QuestionnaireParameter oneQp) {
		
		Set<QuestionnaireQuestion> listOfQqs = createSetOfQQ(mapOfQp, oneQp, listOfQa);				
		
		// Create Map key as QA and value as List of QQ.
		Map<QuestionnaireAsset, Set<QuestionnaireQuestion>> mapOfQA = new HashMap<>();
		for (QuestionnaireQuestion qq : listOfQqs) {
			if(mapOfQA.containsKey(qq.getQuestionnaireAsset())){
				Set<QuestionnaireQuestion> qqs = mapOfQA.get(qq.getQuestionnaireAsset());
				qqs.add(qq);
				mapOfQA.put(qq.getQuestionnaireAsset(), qqs);
			} else {
				Set<QuestionnaireQuestion> qqs = new HashSet<>();
				qqs.add(qq);
				mapOfQA.put(qq.getQuestionnaireAsset(), qqs);
			}
		}
		return mapOfQA;
	}

	private Set<QuestionnaireQuestion> createSetOfQQ(Map<Integer, List<QuestionnaireParameter>> mapOfQp, QuestionnaireParameter oneQp, Set<QuestionnaireAsset> listOfQa) {
		// Get map for parameter tree
		Map<ParameterType, List<ParameterBean>> mapOfParamTree = parameterService.getAllParametersInTree(parameterService.findFullyLoaded(oneQp.getParameterByParameterId().getId()));
		
		// Get list of parameter beans by TCOL parameter type from map
		List<ParameterBean> tcolParams = mapOfParamTree.get(ParameterType.TCOL);
		
		List<QuestionnaireParameter> listOfQpInMap = new ArrayList<>();
		
		// Get QPs for TCOL type 
		for (ParameterBean pb : tcolParams) {
			if(mapOfQp.containsKey(pb.getId()))
				listOfQpInMap.addAll(mapOfQp.get(pb.getId())); 
		}
		
		// fetch all QQ by QPs and QAs.
		Set<QuestionnaireQuestion> listOfQqs = new HashSet<>();
		if(!listOfQpInMap.isEmpty() && !listOfQa.isEmpty())
			listOfQqs = questionnaireQuestionService.findByQuestionnaireParameterInAndQuestionnaireAssetIn(listOfQpInMap, listOfQa);
		return listOfQqs;
	}

	private Set<QuestionnaireAsset> createSetOfQA(List<AssetBean> assets) {
		// Create set of Asset from list of AssetBean
		Set<Asset> setOfAsset = new HashSet<>();
		for (AssetBean assetBean : assets) {
			Asset asset = new Asset();
			asset.setId(assetBean.getId());
			setOfAsset.add(asset);
		}
		
		Set<QuestionnaireAsset> setOfQa = new HashSet<>();
		if(!setOfAsset.isEmpty())
			setOfQa = questionnaireAssetService.findByQuestionnaireAndAssetIn(questionnaire, setOfAsset);
		
		return setOfQa;
	}

	private Map<Integer, List<QuestionnaireParameter>> createMapOfQP() {
		// fetch all questionnaire parameters for questionnaire
		List<QuestionnaireParameter> listOfQp = questionnaireParameterService.findByQuestionnaire(questionnaire);
		
		Map<Integer, List<QuestionnaireParameter>> mapOfQp = new HashMap<>();
		
		for (QuestionnaireParameter qp : listOfQp) {
			if(mapOfQp.containsKey(qp.getParameterByParameterId().getId())){
				List<QuestionnaireParameter> qps = mapOfQp.get(qp.getParameterByParameterId().getId());
				qps.add(qp);
				mapOfQp.put(qp.getParameterByParameterId().getId(), qps);
			} else {
				List<QuestionnaireParameter> qps = new ArrayList<>();
				qps.add(qp);
				mapOfQp.put(qp.getParameterByParameterId().getId(), qps);
			}
			
		}
		return mapOfQp;
	}
}

package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.report.ReportConfigBean;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * 
 * @author sunil1.gupta
 *
 */

public interface ReportService {

	/**
	 * method to save the reports
	 * @param reportConfigBean
	 * @return {@report}
	 */
	public Report saveOrUpdateReport(ReportConfigBean reportConfigBean);
	
	/**
	 * method to delete report 
	 * @param reportId
	 */
	public void deleteReport(Integer reportId);

	/**
	 * method to find All saved and assigned reports for an user
	 * @param activeWorkSpaceForUser
	 * @param currentUser
	 * @return {@List<ReportConfigBean>}
	 */
	public List<ReportConfigBean> findAllMySavedAndAssingedReport(Workspace activeWorkSpaceForUser, User currentUser);
	
	/**
	 * method to find report by id
	 * @param idOfColumn
	 * @return {@report}}
	 */
	public Report findReportById(Integer idOfColumn);
	
		
	/**
	 * @param workspaces
	 * @return
	 */
	public List<Report> findByWorkspaceIn(Set<Workspace> workspaces);
	

	/**
	 * @param reports
	 * @return
	 */
	public List<Report> saveAll(List<Report> reports);

	/**
	 * @param id
	 */
	public void activateReportAsHomepage(Integer id);
	/**
	 * method to find report by reportName
	 * @param reportName
	 * @return
	 */
	public Report findByReportName(String reportName);

}

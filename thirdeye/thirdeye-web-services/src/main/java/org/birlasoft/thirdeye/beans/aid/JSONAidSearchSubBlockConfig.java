package org.birlasoft.thirdeye.beans.aid;


/**
 * @author samar.gupta
 *
 */
public class JSONAidSearchSubBlockConfig {
	
	private String subBlockName;
	private String subBlockTitle;
	private String displayString;
	
	public String getSubBlockName() {
		return subBlockName;
	}
	public void setSubBlockName(String subBlockName) {
		this.subBlockName = subBlockName;
	}
	public String getSubBlockTitle() {
		return subBlockTitle;
	}
	public void setSubBlockTitle(String subBlockTitle) {
		this.subBlockTitle = subBlockTitle;
	}
	public String getDisplayString() {
		return displayString;
	}
	public void setDisplayString(String displayString) {
		this.displayString = displayString;
	}
}

package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.BcmLevelBean;
import org.birlasoft.thirdeye.beans.ExcelCellBean;
import org.birlasoft.thirdeye.beans.JSONBenchmarkMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONBenchmarkQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceQuestionMapper;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.QuestionnaireQuestionBean;
import org.birlasoft.thirdeye.beans.bcm.BcmResponseBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapDataBean;
import org.birlasoft.thirdeye.beans.fr.AssetValueWrapper;
import org.birlasoft.thirdeye.beans.fr.BcmChildLevelWrapper;
import org.birlasoft.thirdeye.beans.fr.BcmParentLevelWrapper;
import org.birlasoft.thirdeye.beans.fr.FunctionalRedundancyWrapper;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterConfigBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterEvaluator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.DataType;
import org.birlasoft.thirdeye.constant.ExportPurpose;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.ExportLog;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.FunctionalMapData;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.FunctionalMapDataRepository;
import org.birlasoft.thirdeye.repositories.FunctionalMapRepository;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMAssetValueBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMLevelBean;
import org.birlasoft.thirdeye.search.api.beans.IndexBCMParameterBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMReportService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ExcelWriter;
import org.birlasoft.thirdeye.service.ExportLogService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.util.ExcelUtility;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.validators.CellTypeValidator;
import org.birlasoft.thirdeye.validators.ExcelCellValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Service implementation class for Functional Maps
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class FunctionalMapServiceImpl implements FunctionalMapService {
	
	private static Logger logger = LoggerFactory.getLogger(FunctionalMapServiceImpl.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	
	private static final int ROWNUM_LEVEL_START = 2;
	private static final String SHEET_MAP = "Map";
	private static final String CELL_TYPE_MODIFIED = "Cell Type has been modified.";
	private static final String HIDDEN_FORMAT=";;;";
	
	@Autowired
	private AssetTemplateService assetTemplateService;
	private QuestionService questionService;
	private BCMService bcmService;
	private FunctionalMapRepository functionalMapRepository;
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private ExportLogService exportLogService;
	@Autowired
	private ExcelWriter excelWriter;
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private BCMLevelService bcmLevelService;
	@Autowired
	private AssetService assetService;
	@Autowired
	private	FunctionalMapDataRepository functionalMapDataRepository;
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private QuestionnaireService  questionnaireService;
	@Autowired
	private BCMReportService bcmReportService;
	
	/**
	 * Parameterized Constructor
	 * @param questionService
	 * @param bcmService
	 * @param functionalMapRepository
	 * @param customUserDetailsService
	 */
	@Autowired
	public FunctionalMapServiceImpl(QuestionService questionService, BCMService bcmService,
			FunctionalMapRepository functionalMapRepository,
			CustomUserDetailsService customUserDetailsService) {		
		this.questionService = questionService;
		this.bcmService = bcmService;
		this.functionalMapRepository = functionalMapRepository;
		this.customUserDetailsService = customUserDetailsService;
	}

	@Override
	public FunctionalMap createFunctionalMapObject(FunctionalMapBean functionalMapBean, User currentUser) {
		FunctionalMap functionalMap = new FunctionalMap();
		setFunctionalMapDetails(functionalMapBean, functionalMap);
		functionalMap.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		functionalMap.setUserByCreatedBy(currentUser);
		functionalMap.setUserByUpdatedBy(currentUser);
		functionalMap.setCreatedDate(new Date());
		functionalMap.setUpdatedDate(new Date());
		return functionalMap;
	}

	private void setFunctionalMapDetails(FunctionalMapBean functionalMapBean, FunctionalMap functionalMap) {
		functionalMap.setName(functionalMapBean.getName());
		functionalMap.setAssetTemplate(assetTemplateService.findOne(functionalMapBean.getAssetTemplateId()));
		functionalMap.setBcm(bcmService.findOne(functionalMapBean.getBcmId()));
		functionalMap.setType(functionalMapBean.getType());
		functionalMap.setQuestion(questionService.findOne(functionalMapBean.getQuestionId()));
	}

	@Override
	public FunctionalMap save(FunctionalMap fm) {
		return functionalMapRepository.save(fm);
	}

	@Override
	public List<FunctionalMap> findByWorkspace(Workspace workspace, boolean loaded) {
		List<FunctionalMap> listOfFm = functionalMapRepository.findByWorkspace(workspace);
		
		if(loaded){
			for (FunctionalMap functionalMap : listOfFm) {
				functionalMap.getAssetTemplate().getAssetTemplateName();
				functionalMap.getBcm().getBcmName();
				functionalMap.getQuestion().getTitle();
			}
		}
		
		return listOfFm;
	}

	@Override
	public FunctionalMap findOne(Integer idOfFunctionalMap, boolean loaded) {
		FunctionalMap fm = functionalMapRepository.findOne(idOfFunctionalMap);
		
		if(loaded){
			fm.getAssetTemplate().getAssetTemplateName();
			fm.getBcm().getBcmName();
			fm.getQuestion().getTitle();
			fm.getWorkspace().getWorkspaceName();
		}
		return fm;
	}

	@Override
	public FunctionalMap updateFunctionalMapObject(FunctionalMapBean functionalMapBean, User currentUser) {
		FunctionalMap functionalMap = findOne(functionalMapBean.getId(), false);
		setFunctionalMapDetails(functionalMapBean, functionalMap);
		functionalMap.setUserByUpdatedBy(currentUser);
		functionalMap.setUpdatedDate(new Date());
		return functionalMap;
	}
	

	@Override
	public void exportFmTemplate(HttpServletResponse response, FunctionalMap fm) {
		String fileName = fm.getName().replaceAll("\\s+","_");
		String hash = Utility.getUniqueHash();
		XSSFWorkbook workbook = exportFmLevelsExcel(fm,hash);		
		if(workbook != null){
			exportLogService.saveExportLog(fm.getId(),hash,ExportPurpose.FM.toString());
		}
		excelWriter.writeExcel(response, workbook, fileName);		
	}
	
	private XSSFWorkbook exportFmLevelsExcel(FunctionalMap fm, String hash) {
		
		// Find out the depth to which you need to go
		Set<Integer> levelNumbers = getLevelNumbers(fm);
		
		// See what is available in the database for the depth that is required
		List<BcmLevel> listOfBcmLevel = bcmLevelService.findByBcmAndLevelNumberIn(fm.getBcm(), levelNumbers, false);
		
		// The roots of the BCM tree
		List<BcmLevelBean> rootBcmLevels = bcmLevelService.generateLevelTreeFromBcmLevels(listOfBcmLevel);
		
		// Load the asset template from which we will be pulling the 
		// asset details
		AssetTemplate assetTemplate = assetTemplateService.findOne(fm.getAssetTemplate().getId());
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);		

		// Pull the assets that need to be shown and convert them into the beans
		Set<Asset> assets = assetService.findByAssetTemplate(assetTemplate);
		List<AssetBean> listOfAssets = assetService.getListOfAssetBean(assets);	
		
		// Which question will give the drop down
		Question question = questionService.findOne(fm.getQuestion().getId());
		workspaceSecurityService.authorizeQuestionAccess(question);

		
		QuestionnaireQuestionBean qqBean = questionService.createQQBeanForViewQuestion(question, "[Name of your Asset]", 0);
		return prepareFMWorKBook(rootBcmLevels, listOfAssets, qqBean, hash, levelNumbers);
	}
	
	/**
	 * Creates the workbook for the download of the BCM level mapping
	 * 
	 * @param rootBcmLevels
	 * @param levelNumbers 
	 */
	private XSSFWorkbook prepareFMWorKBook(List<BcmLevelBean> rootBcmLevels, List<AssetBean> assetBean,
								QuestionnaireQuestionBean qqBean,String hash, Set<Integer> levelNumbers) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet1 = workbook.createSheet(SHEET_MAP);
		
			
		// Setup the config sheet
		ExcelUtility.setupConfigSheet(hash, workbook);

		// add 2 row to the sheet1
		Row row0 = sheet1.createRow(0);
		Row row1 = sheet1.createRow(1);
				
		
		// Styling Cells
		CellStyle cellStyle = ExcelUtility.getCellWithColor(workbook, HSSFColor.LIGHT_BLUE.index);
		Font font = workbook.createFont();
		font.setFontHeightInPoints((short) 15);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		cellStyle.setFont(font);
		
		CellStyle cellStyleL2 = ExcelUtility.getCellWithColor(workbook, HSSFColor.GOLD.index);	
		
		Cell rowcol;	
			
		// Populating BCM Levels	
		int rowIndex = ROWNUM_LEVEL_START;
		
		// Fetch all the leaf nodes
		
		
		Set<BcmLevelBean> allTheLeaves = new HashSet<>();
		
		// Ordering of the hashset is going to be a problem. The L1 / L2 are sequenced!
		// Work your way up to the parents
		int levelCount=0;
		for (BcmLevelBean oneRootLevel : rootBcmLevels){
			// If you are dealing with bad data
			// i.e. you ask for level 3 but data only till level 2 then
			// the set below will be empty then you can run recursion to reduce target
			// level until >=1 .
			for(int i=Collections.max(levelNumbers);i>0;i--){
				Set<BcmLevelBean> leafNodesAtLevel = oneRootLevel.getLeavesOfTree(i);
				if (!leafNodesAtLevel.isEmpty()){
					allTheLeaves.addAll(leafNodesAtLevel);
					if(levelCount<i)
					   levelCount = i;
					break;
				}
		    }
		}
		for(int i =1;i<=levelCount;i++){
			 rowcol = row1.createCell(i);
			 rowcol.setCellValue("L"+i+" Process");
			 rowcol.setCellStyle(cellStyle);
		}
		
		for (BcmLevelBean oneLeaf : allTheLeaves){
			processRow(oneLeaf, sheet1, rowIndex, 0);
			rowIndex ++;
		}
		
		// Populating the assets
		assetBean.sort((obj1, obj2) -> obj1.getShortName().compareTo(obj2.getShortName()));
		int cellIndex = levelCount+1;
		for (AssetBean ab : assetBean) {

			Cell assetHiddenCell = row0.createCell(cellIndex);
			Cell assetCell = row1.createCell(cellIndex);

			assetCell.setCellValue(ab.getShortName());
			assetHiddenCell.setCellValue(ab.getId());
			assetHiddenCell.setCellStyle(ExcelUtility.getCustomCellStyle(workbook,HIDDEN_FORMAT));

			assetCell.setCellStyle(cellStyleL2);
			sheet1.autoSizeColumn(cellIndex);
			cellIndex = cellIndex + 1;
		}

		// Retrieving the Question's options for dropdown
		List<JSONQuestionOptionMapper> options = qqBean.getQuestion()
				.getJsonMultiChoiceQuestionMapper().getOptions();
		String[] optionsArray = new String[options.size()];
		int optionsArrayIndex = 0;
		for (JSONQuestionOptionMapper op : options) {

			optionsArray[optionsArrayIndex] = op.getText();
			optionsArrayIndex++;
		}
		

		// Populating the dropdowns
		int totalRows = allTheLeaves.size() + 1;
		int totalColumns = assetBean.size() + levelCount ;
		
		//Generating Dropdown
		//sheet1.setDefaultColumnStyle(levelCount+1, ExcelUtility.getCustomCellStyle(sheet1.getWorkbook(),Constants.TEXT_FORMAT));
		ExcelUtility.generateDropDown(sheet1, 2, totalRows, levelCount+1, totalColumns, optionsArray);
		
		
		sheet1.createFreezePane(levelCount+1, 2);
		for(int i =0;i<=levelCount;i++){
			sheet1.autoSizeColumn(i);
		}
		row0.setZeroHeight(true);
		sheet1.setColumnHidden(0, true);	
		sheet1.setZoom(70);
		return workbook;
	}

	private void processRow(BcmLevelBean oneLeaf, XSSFSheet sheet1, int rowIndex, int startCol) {
		Row oneRowBeingWritten = sheet1.createRow(rowIndex);
		writeOneLevel(oneLeaf, oneRowBeingWritten, startCol,sheet1);
	}

	private void writeOneLevel(BcmLevelBean oneLeaf, Row oneRowBeingWritten, int startCol,XSSFSheet sheet1) { 
		
		if (oneLeaf.getParent() != null){
			writeOneLevel (oneLeaf.getParent(), oneRowBeingWritten, startCol,sheet1);
			Cell oneCell = oneRowBeingWritten.createCell(startCol + oneLeaf.getLevelNumber());
			oneCell.setCellValue(oneLeaf.getBcmLevelName());
			oneCell.setCellStyle(ExcelUtility.getCellWithColor(sheet1.getWorkbook(), HSSFColor.GREEN.index));
			
			//BCM leaf id saved to column zero of same row
			Cell oneCellId = oneRowBeingWritten.createCell(0);
			oneCellId.setCellValue(oneLeaf.getId());
			oneCellId.setCellStyle(ExcelUtility.getCustomCellStyle(sheet1.getWorkbook(),HIDDEN_FORMAT));
		} else {
			Cell oneCell = oneRowBeingWritten.createCell(startCol + oneLeaf.getLevelNumber());
			oneCell.setCellValue(oneLeaf.getBcmLevelName());
			oneCell.setCellStyle(ExcelUtility.getCellWithColor(sheet1.getWorkbook(), HSSFColor.GOLD.index));
			
			//BCM leaf id saved to column zero(HIDDEN) of same row
			Cell oneCellId = oneRowBeingWritten.createCell(0);
			oneCellId.setCellValue(oneLeaf.getId());
			oneCellId.setCellStyle(ExcelUtility.getCustomCellStyle(sheet1.getWorkbook(),HIDDEN_FORMAT));
		}
		
	}
		
	/**
	 * This method returns the depth to which you would 
	 * like to traverse the BCM tree
	 * @author sunil1.gupta
	 * @param type
	 * @return levelNumbers
	 */
	private Set<Integer> getLevelNumbers(FunctionalMap fm) {
		Set<Integer> levelNumbers = new HashSet<>();
		List<BcmLevel> listOfBcmLevel = bcmLevelService.findByBcm(fm.getBcm());
		Set<Integer> setOfBcmLevel = new HashSet<>();
		Map<Integer,String> mapOfBcmLevelNumber = new HashMap<>();
		listOfBcmLevel.forEach(bcmLevels->setOfBcmLevel.add(bcmLevels.getLevelNumber()));
		setOfBcmLevel.forEach(bcmLevelNumber-> mapOfBcmLevelNumber.put(bcmLevelNumber, "L"+ bcmLevelNumber));
		int targetDepth = 0;
		for (Map.Entry<Integer, String> entry : mapOfBcmLevelNumber.entrySet()) {
			if(fm.getType().equals(entry.getValue())){
		    targetDepth=entry.getKey();
		}
		}
		for(int i=1;i <= targetDepth;i++){
			levelNumbers.add(i);
		}		
		return levelNumbers;
	}
	
	/**
	 * method to import and validate functional
	 * map security token.
	 * @author sunil1.gupta
	 */
	@Override
	public void importFunctionalMapExcel(Workbook workbook, FunctionalMap fm,
			List<ExcelCellBean> listOfErrorCells, Integer questionnaireId) {
		
		List<FunctionalMapData> listOfFMData = new ArrayList<>();		
		//get exportLog by  hashValue getting from workbook 
		ExportLog exportLog = getExportLogByHashValue(workbook);
		// first validate Functional map  import excel sheet 2 for hash security
		boolean isValidate = validateFMSheetSecurity(exportLog, fm, listOfErrorCells);
		//check excel hash security is valid or not then read fm excel
		if (isValidate) {			
			listOfFMData =  readFmExcel(workbook, fm, listOfErrorCells);			
		}
		
	    // save Functional Map data  into Functional_Map_Data table and update export log update timeup
		if(listOfErrorCells.isEmpty()){
			if(!listOfFMData.isEmpty()){
			   List<FunctionalMapData> listOfUpdatedFMData = prepareUpdatedFMDataList(fm,listOfFMData,questionnaireId);
			   functionalMapDataRepository.save(listOfUpdatedFMData);
			   exportLogService.updateExportLog(exportLog);
			}else{
				  List<String> errors = new ArrayList<>();
			      errors.add("Uploaded Excel has no data.");
				  setExcelErrors(listOfErrorCells, errors, null);
		    }
		}		
	}	
	
	/**
	 * @author sunil1.gupta
	 * @param fm
	 * @param listOfFMData
	 * @param questionnaireId
	 * @return FunctionalMapData
	 */
	private List<FunctionalMapData> prepareUpdatedFMDataList(FunctionalMap fm,List<FunctionalMapData> listOfExcelFMData, Integer questionnaireId) {
	    
	    List<FunctionalMapData> fmDataList = new ArrayList<>();
	    Questionnaire questionnaire = questionnaireService.findOne(questionnaireId);
	    List<FunctionalMapData> orignalFmDataList = functionalMapDataRepository.findByFunctionalMapAndQuestionnaire(fm, questionnaire);
	    Iterator<FunctionalMapData> itExcelFmData = listOfExcelFMData.iterator();
	    while(itExcelFmData.hasNext()){
	    	FunctionalMapData oneFMData = itExcelFmData.next();                      

	        Integer assetId = oneFMData.getAsset().getId();
	        Integer bcmLevelId = oneFMData.getBcmLevel().getId(); 
	        Integer fmId = oneFMData.getFunctionalMap().getId();
	        
	        oneFMData.setQuestionnaire(questionnaire);
	        for(FunctionalMapData oneMapData : orignalFmDataList)             
	        {                       
	            if(assetId == oneMapData.getAsset().getId() 
	                    && bcmLevelId == oneMapData.getBcmLevel().getId()
	                    && fmId == oneMapData.getFunctionalMap().getId())
	            {
	            	oneFMData.setId(oneMapData.getId());
	            }	            
	        }
	        fmDataList.add(oneFMData);
	    }
		return fmDataList;
	}

	/**
	 * method to read functional map excel data and validate.
	 * @author sunil1.gupta
	 * @param workbook
	 * @param fm
	 * @param listOfErrorCells
	 */
	private List<FunctionalMapData> readFmExcel(Workbook workbook, FunctionalMap fm,List<ExcelCellBean> listOfErrorCells) {
		 
		   Sheet firstSheet = workbook.getSheetAt(0);	
		   // validate FM excel structure and prepare list of finctional map data
		   return validateFmExcelStructureAndPrepareData(firstSheet ,fm , listOfErrorCells);	
	}
	
	/**
	 * method to validate uploaded Functional map excel
	 * structure,and prepare functional map data list to
	 * store in Functional_Map_Data table.
	 * @author sunil1.gupta
	 * @param firstSheet
	 * @param fm
	 * @param listOfErrorCells
	 * @return listFunctionalMapDatas
	 */

	private List<FunctionalMapData> validateFmExcelStructureAndPrepareData(Sheet firstSheet , FunctionalMap fm, List<ExcelCellBean> listOfErrorCells) {
		
		Set<Integer> levelNumbers = getLevelNumbers(fm);		
		// See what is available in the database for the depth that is required
		List<BcmLevel> listOfBcmLevel = bcmLevelService.findByBcmAndLevelNumberIn(fm.getBcm(), levelNumbers, false);		
		// The roots of the BCM tree
		List<BcmLevelBean> rootBcmLevels = bcmLevelService.generateLevelTreeFromBcmLevels(listOfBcmLevel);		
		// Load the asset template from which we will be pulling the 
		// asset details
		AssetTemplate assetTemplate = assetTemplateService.findOne(fm.getAssetTemplate().getId());
		// Check if the user can access this template
		workspaceSecurityService.authorizeTemplateAccess(assetTemplate);	
		// Pull the assets that need to be shown and convert them into the beans
		Set<Asset> assets = assetService.findByAssetTemplate(assetTemplate);
		List<AssetBean> listOfAssetBeans = assetService.getListOfAssetBean(assets);	
		Set<BcmLevelBean> allTheLeaves = new HashSet<>();
		
		// Ordering of the hashset is going to be a problem. The L1 / L2 are sequenced!
		// Work your way up to the parents
		int levelCount=0;
		for (BcmLevelBean oneRootLevel : rootBcmLevels){
			// If you are dealing with bad data
			// i.e. you ask for level 3 but data only till level 2 then
			// the set below will be empty then you can run recursion to reduce target
			// level until >=1 .
			for(int i=Collections.max(levelNumbers);i>0;i--){
				Set<BcmLevelBean> leafNodesAtLevel = oneRootLevel.getLeavesOfTree(i);
				if (!leafNodesAtLevel.isEmpty()){
					allTheLeaves.addAll(leafNodesAtLevel);
					if(levelCount<i)
					   levelCount = i;
					break;
				}
		    }
		  }		
			
		  //validate BCM Level on Functional map uploaded sheet		
		   Map<Integer,BcmLevel> mapOfBCMlevel  = validateFunctionalMapBCMLevels(firstSheet,allTheLeaves,listOfBcmLevel,levelCount,listOfErrorCells);	
		  //validate Asset on Functional map uploaded sheet	
		   Map<Integer,Asset> mapOfAssets = validateAssets(firstSheet,listOfAssetBeans,assets,levelCount,listOfErrorCells);	
		   //validate excel sheet data for upload and prepare functional map data	
		   return validateAndPrepareFMData(firstSheet, fm, mapOfBCMlevel, mapOfAssets,levelCount, listOfErrorCells);
	  }
	
	/**
	 * method to validate the excel data for upload in 
	 * Functional_Map_Data table
	 * @author sunil1.gupta
	 * @param firstSheet
	 * @param fm
	 * @param mapOfBCMlevel
	 * @param mapOfAssets
	 * @param levelCount
	 * @param listOfErrorCells
	 * @return listFunctionalMapDatas
	 */

	private List<FunctionalMapData> validateAndPrepareFMData(Sheet firstSheet,FunctionalMap fm, Map<Integer, BcmLevel> mapOfBCMlevel,Map<Integer, Asset> mapOfAssets,Integer levelCount,List<ExcelCellBean> listOfErrorCells) {
		
		Question question = questionService.findOne(fm.getQuestion().getId());
		List<FunctionalMapData> listFunctionalMapDatas = new ArrayList<>();
		Iterator<Row> firstSheetItr = firstSheet.iterator();		
		while (firstSheetItr.hasNext()){
			Row nextRow = firstSheetItr.next();
			if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1){
				continue;
			}			
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			int columIndex;
			while (cellIterator.hasNext()) {
				Cell nextCell = cellIterator.next();
				columIndex = nextCell.getColumnIndex();
				if(columIndex <= levelCount){
					continue;
				}				
				ExcelCellBean cellBean = new ExcelCellBean();
				cellBean.setValue(question.getQueTypeText());
				validateCellData(cellBean, nextCell);				
				if(cellBean.getErrors().isEmpty()){
					FunctionalMapData functionalMapData = new FunctionalMapData();
					functionalMapData.setAsset(mapOfAssets.get(nextCell.getColumnIndex()));
					functionalMapData.setBcmLevel(mapOfBCMlevel.get(nextCell.getRowIndex()));
					functionalMapData.setFunctionalMap(fm);
					functionalMapData.setUser(customUserDetailsService.getCurrentUser());
					functionalMapData.setData(getResponseData(question.getQueTypeText(),cellBean.getValue()));
					functionalMapData.setUpdatedDate(new Date());
					listFunctionalMapDatas.add(functionalMapData);
				} else {
					listOfErrorCells.add(cellBean);
				}
			
			 }			
		}		
		return listFunctionalMapDatas;
	}
	
	/**
	 * method to validate Functional map BCM Levels
	 * for uploaded excel and prepare map for BCM level
	 * to that row .
	 * @author sunil1.gupta
	 * @param firstSheet
	 * @param allBCMLeaves
	 * @param listOfBcmLevel
	 * @param levelCount
	 * @param listOfErrorCells
	 * @return mapOfRowAndBCMlevel
	 */

	private Map<Integer,BcmLevel> validateFunctionalMapBCMLevels(Sheet firstSheet,Set<BcmLevelBean> listOfBCMLeaves,List<BcmLevel> listOfBcmLevel,Integer levelCount, List<ExcelCellBean> listOfErrorCells) {
		
		Map<Integer,BcmLevel> mapOfRowAndBCMlevel = new HashMap<>();
		Map<Integer,BcmLevel> mapOfBCMlevel = new HashMap<>();
		Map<Integer,Integer> mapOfColumAndBcmLevelNumber = new HashMap<>();
		Map<Integer,String> mapOfColumAndBcmLevelName = new HashMap<>();
		Map<Integer,BcmLevelBean> mapOfBCMLeaves = new HashMap<>();
		
		for(BcmLevel oneBLevel :listOfBcmLevel){
			mapOfBCMlevel.put(oneBLevel.getId(), oneBLevel);
		}
		for(BcmLevelBean oneBLevelBean :listOfBCMLeaves){
			mapOfBCMLeaves.put(oneBLevelBean.getId(), oneBLevelBean);
		}	
		for(int i =1;i<=levelCount;i++){
			mapOfColumAndBcmLevelNumber.put(i, i);
			mapOfColumAndBcmLevelName.put(i, "L"+i+" Process");
		}	
		Iterator<Row> firstSheetItr = firstSheet.iterator();
		while (firstSheetItr.hasNext()){
			Row nextRow = firstSheetItr.next();
			if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1){
				if(nextRow.getRowNum() == 1){
					validateBCMHeaders(nextRow,mapOfColumAndBcmLevelName,listOfErrorCells);					
				}
				continue;
			}
			BcmLevelBean bcmLevelBean = new BcmLevelBean();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			int columIndex = 0;
			while (cellIterator.hasNext() && columIndex <= levelCount) {
				List<String> errors = new ArrayList<>();
				Cell nextCell = cellIterator.next();
				if(nextCell.getColumnIndex()==0){
					if(nextCell.getCellType() == Cell.CELL_TYPE_NUMERIC && mapOfBCMLeaves.get(new Double(nextCell.getNumericCellValue()).intValue()) !=null){
						 bcmLevelBean = mapOfBCMLeaves.get(new Double(nextCell.getNumericCellValue()).intValue());
				         mapOfRowAndBCMlevel.put(nextRow.getRowNum(), mapOfBCMlevel.get(bcmLevelBean.getId()));				       
					}else{						
						errors.add(CELL_TYPE_MODIFIED);
						setExcelErrors(listOfErrorCells, errors,nextCell);
					}
				}else{					
					if(nextCell.getCellType() == Cell.CELL_TYPE_STRING){						 
						 int levelNumber = mapOfColumAndBcmLevelNumber.get(nextCell.getColumnIndex()) == null ? -1 : mapOfColumAndBcmLevelNumber.get(nextCell.getColumnIndex());
						 if(bcmLevelBean!=null && levelNumber!=-1)
						  validateBCMLevel(nextCell,levelNumber,bcmLevelBean,listOfErrorCells);
					}else{
						errors.add(CELL_TYPE_MODIFIED);
						setExcelErrors(listOfErrorCells, errors,nextCell);
					}					
				}
				columIndex++;
			}
		}
		return mapOfRowAndBCMlevel;
	}
	
	/**
	 * method to validate BCM header text
	 * @author sunil1.gupta
	 * @param nextRow
	 * @param mapOfColumAndBcmLevelName
	 * @param listOfErrorCells
	 */
	
	private void validateBCMHeaders(Row nextRow,Map<Integer, String> mapOfColumAndBcmLevelName,List<ExcelCellBean> listOfErrorCells) {
		List<String> errors = new ArrayList<>();		
		for(Cell oneCell : nextRow){
			if(oneCell.getColumnIndex() <= mapOfColumAndBcmLevelName.size() && ((oneCell.getCellType() != Cell.CELL_TYPE_STRING) || !oneCell.getStringCellValue().equalsIgnoreCase(mapOfColumAndBcmLevelName.get(oneCell.getColumnIndex())))){
				errors.add("BCM Header Cell Type has been modified.");
				setExcelErrors(listOfErrorCells, errors,oneCell);				 
		     }
	     }
    }

	/**
	 * method to validate BCM level
	 * @author sunil1.gupta
	 * @param cellValue
	 * @param levelNumber
	 * @param bcmLevelBean
	 * @param listOfErrorCells
	 */

	private void validateBCMLevel(Cell cell, int levelNumber,BcmLevelBean bcmLevelBean,List<ExcelCellBean> listOfErrorCells) {
		List<String> errors = new ArrayList<>();		
	   	if(!bcmLevelBean.getBcmLevelName().equalsIgnoreCase(cell.getStringCellValue()) && bcmLevelBean.getLevelNumber() !=levelNumber){
	     	if(bcmLevelBean.getParent() !=null){
	     		validateBCMLevel(cell,levelNumber,bcmLevelBean.getParent(),listOfErrorCells);
	     	}else{
		      errors.add("BCM Levels have been modified");
		      setExcelErrors(listOfErrorCells, errors,cell);
	     	}
	    }else{
	    	if(!bcmLevelBean.getBcmLevelName().equalsIgnoreCase(cell.getStringCellValue())){
	    		  errors.add("BCM Levels have been modified.");
			      setExcelErrors(listOfErrorCells, errors,cell);
	    	}	    	
	    }
    }
	
    /**
     * method to validate uploaded functional map sheet asset
     * and return map of column and Assets.
     * @param firstSheet
     * @param listOfAssetBeans
     * @param assets
     * @param levelCount
     * @param listOfErrorCells
     * @return mapOfColumnAndAssets
     */
	
	private Map<Integer,Asset> validateAssets(Sheet firstSheet,List<AssetBean> listOfAssetBeans, Set<Asset> assets,Integer levelCount,List<ExcelCellBean> listOfErrorCells) {

		Row firstRow = firstSheet.getRow(firstSheet.getFirstRowNum());
		Row secondRow = firstSheet.getRow(firstSheet.getFirstRowNum()+1);
		
		Map<Integer,Asset> mapOfAssets = new HashMap<>();
		Map<Integer,AssetBean> mapOfAssetBeans = new HashMap<>();		
		Map<Integer,Asset> mapOfColumnAndAssets = new HashMap<>();
		List<String> errors = new ArrayList<>();
		
		for(Asset oneAsset :assets){
			mapOfAssets.put(oneAsset.getId(), oneAsset);
		}
		for(AssetBean oneAssetBean :listOfAssetBeans){
			mapOfAssetBeans.put(oneAssetBean.getId(), oneAssetBean);
		}		
		for (Cell oneCell : firstRow) {
			if(oneCell.getColumnIndex() > levelCount){
				if(oneCell.getCellType() == Cell.CELL_TYPE_NUMERIC &&  secondRow.getCell(oneCell.getColumnIndex()).getCellType() == Cell.CELL_TYPE_STRING){
					if(mapOfAssetBeans.get(new Double(oneCell.getNumericCellValue()).intValue()).getShortName().equalsIgnoreCase(secondRow.getCell(oneCell.getColumnIndex()).getStringCellValue())){
						mapOfColumnAndAssets.put(oneCell.getColumnIndex(), mapOfAssets.get(new Double(oneCell.getNumericCellValue()).intValue()));
					}else{
						errors.add("Asset Column headers have been modified.");
						setExcelErrors(listOfErrorCells, errors,oneCell);
					}				
				}else{
		        	errors.add(CELL_TYPE_MODIFIED);
					setExcelErrors(listOfErrorCells, errors,oneCell);
		         }
			}
		}	
		
		return mapOfColumnAndAssets;
	}

	/**
	 * method to set excel error.
	 * @param listOfErrorCells
	 * @param errors
	 */
	@Override
	public void setExcelErrors(List<ExcelCellBean> listOfErrorCells,List<String> errors,Cell cell) {
		 ExcelCellBean excelCellBean = new ExcelCellBean();
		 if(cell !=null){
			 excelCellBean.setRow(cell.getRowIndex());
			 excelCellBean.setColumn(cell.getColumnIndex());
		 }
		 excelCellBean.setErrors(errors);
		 listOfErrorCells.add(excelCellBean);
	}

	/**
	 * method will check the hash code security on uploaded workbook
	 * for sheet 2
	 * @author sunil1.gupta
	 * @param workbook
	 * @param functionalMap
	 * @param listOfErrorCells
	 */
	private boolean validateFMSheetSecurity(ExportLog exportLog,FunctionalMap functionalMap, List<ExcelCellBean> listOfErrorCells) {
		boolean isValidate;		
		if(exportLog !=null && (exportLog.getRefId() == functionalMap.getId()) 
				&& (exportLog.getUser().getId() == customUserDetailsService.getCurrentUser().getId())
				&& !validateDate(exportLog.getTimeDown(),functionalMap.getUpdatedDate())){
			
			isValidate =true;
		}else{			
			ExcelCellBean excelCellBean = new ExcelCellBean();
			List<String>errors = new ArrayList<>();		
			errors.add("Uploaded file is not valid.");
			excelCellBean.setErrors(errors);
			listOfErrorCells.add(excelCellBean);
			isValidate =false;
		}
		return isValidate;
	}
	
	/**
	 * method to get exportlog by getting hashvalue from workbook 
	 * @author sunil1.gupta
	 * @param workbook
	 * @return ExportLog
	 */
	@Override
	public ExportLog getExportLogByHashValue(Workbook workbook) {
		Sheet configSheet  = workbook.getSheetAt(1);		 
		Row firstRow = configSheet.getRow(configSheet.getFirstRowNum());
		Cell firstCell = firstRow.getCell(firstRow.getFirstCellNum());
		String hashFromUploadedWorkbook =firstCell.getStringCellValue();
		return exportLogService.findByHash(hashFromUploadedWorkbook);
	}
	/**
	 * method will we used to validate export excel download time
	 * and functional map updated time
	 * if exportLogDate < fmUpdateddate then return error
	 * @author sunil1.gupta
	 * @param exportLogDate
	 * @param fmUpdateddate
	 * @return
	 */
	@Override
	 public boolean validateDate(Date exportLogDate,Date fmUpdateddate)
	 {
		 boolean isDateValid = false;
	     int d = exportLogDate.compareTo(fmUpdateddate);
	     if(d < 0){
	    	 isDateValid = true;
	     }
	  return isDateValid;
	 }

	/**
	 * method to validate excel Cell data
	 * @author sunil1.gupta
	 * @param cellBean
	 * @param cell
	 */
	 private void validateCellData(ExcelCellBean cellBean, Cell cell) {
		 cellBean.setRow(cell.getRowIndex());
		 cellBean.setColumn(cell.getColumnIndex());
		 List<ExcelCellValidator> cellValidators = new ArrayList<>();		
		 switch(cell.getCellType()){
		 case Cell.CELL_TYPE_NUMERIC:                               
			 cellValidators.add(new CellTypeValidator(DataType.NUMBER.toString()));
			 break;

		 case Cell.CELL_TYPE_STRING:
			 cellValidators.add(new CellTypeValidator(DataType.TEXT.toString()));                           
			 break;                           

		 }
		 cellBean.setCellValidators(cellValidators);
		 cellBean.setErrors(new ArrayList<String>());
		 for (ExcelCellValidator oneValidator : cellBean.getCellValidators()) {
			 oneValidator.validate(cell, cellBean.getErrors());
		 }	
		 if(cellBean.getErrors().isEmpty()){
			 validateCellValue(cellBean, cell);	
		 }
	 }

	 /**
	  * method to validate excel cell value is 
	  * exist in Question Options.
	  * @author sunil1.gupta
	  * @param cellBean
	  * @param cell
	  */
	private void validateCellValue(ExcelCellBean cellBean, Cell cell) {
		boolean isValueExist = false;		
		String cellValue = null;
		String jSONStringOfQuestion = cellBean.getValue();
		JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(jSONStringOfQuestion, JSONMultiChoiceQuestionMapper.class);
		if (jsonMultiChoiceQuestionMapper != null){
			switch(cell.getCellType()){          
			case Cell.CELL_TYPE_NUMERIC:
				if(Math.floor(cell.getNumericCellValue()) == cell.getNumericCellValue()){                                      
					cellValue = String.valueOf((int)cell.getNumericCellValue());
				}else{
					cellValue = String.valueOf(cell.getNumericCellValue());
				}
				break;
			case Cell.CELL_TYPE_STRING:
				cellValue = cell.getStringCellValue();
				break;
			}

			cellBean.setValue(cellValue);  
			for (JSONQuestionOptionMapper oneOption : jsonMultiChoiceQuestionMapper.getOptions()){
				if (oneOption.getText().equals(cellValue)){
					isValueExist = true;
					break;
				}
			}
			if(!isValueExist){
				cellBean.getErrors().add("This cell has invalid option.");
			}
		}
	}
	 
	/**
	 * method to get response data as a 
	 * json format .
	 * @author sunil1.gupta
	 * @param questionTypetext
	 * @param cellValue
	 * @return jsonString
	 */
	@Override
	 public String getResponseData(String questionTypetext, String cellValue){			
			String jSONStringOfQuestion = questionTypetext;
			JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(jSONStringOfQuestion, JSONMultiChoiceQuestionMapper.class);
			JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = new JSONMultiChoiceResponseMapper();
			if (jsonMultiChoiceQuestionMapper != null){		
					// We need to see which options were selected
					for (JSONQuestionOptionMapper oneOption : jsonMultiChoiceQuestionMapper.getOptions()){
						if (oneOption.getText().equals(cellValue)){
							jsonMultiChoiceResponseMapper.addOption(oneOption);
						}
					}
			}
			return Utility.convertObjectToJSONString(jsonMultiChoiceResponseMapper);	
	}
	
	@Override
	 public String getResponseDataForBenchmarkMCQ(String questionTypetext, String cellValue){			
			String jSONStringOfQuestion = questionTypetext;
			JSONBenchmarkMultiChoiceQuestionMapper jsonBenchmarkMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(jSONStringOfQuestion, JSONBenchmarkMultiChoiceQuestionMapper.class);
			JSONMultiChoiceResponseMapper jsonBenchmarkMultiChoiceResponseMapper = new JSONMultiChoiceResponseMapper();
			if (jsonBenchmarkMultiChoiceQuestionMapper != null){		
					// We need to see which options were selected
					for (JSONBenchmarkQuestionOptionMapper oneOption : jsonBenchmarkMultiChoiceQuestionMapper.getOptions()){
						if (oneOption.getText().equals(cellValue)){
							jsonBenchmarkMultiChoiceResponseMapper.addOption(oneOption);
						}
					}
			}
			return Utility.convertObjectToJSONString(jsonBenchmarkMultiChoiceResponseMapper);	
	}
	 
	@Override
	public Map<Integer, String> getMapOfLevelTypes(FunctionalMap functionalMap) {
		Map<Integer, String> mapOfFunctionalMapLevels = new HashMap<>();
		Map<Integer, String> mapOfFunctionalMapLevelType = new HashMap<>();
		List<BcmLevel> listBcmLevel = bcmLevelService.findByBcm(functionalMap.getBcm());
	    listBcmLevel.forEach(bcmLevelNumber-> mapOfFunctionalMapLevelType.put(bcmLevelNumber.getLevelNumber(), "L"+ bcmLevelNumber.getLevelNumber()));
	    for(Map.Entry<Integer,String> mapOfFm: mapOfFunctionalMapLevelType.entrySet()){
	    	if(!mapOfFm.getValue().equals(functionalMap.getType())){
	    		mapOfFunctionalMapLevels.put(mapOfFm.getKey(), mapOfFm.getValue());
	    	}
	    	else{
	    		break;
	    }
	    }
		return mapOfFunctionalMapLevels;
	}

	@Override
	public List<FunctionalMapDataBean> fetchFunctionalMapDataByBcmLevel(BcmLevel bcmLevel) {	
		List<FunctionalMapDataBean> listOfFMDataBean = new ArrayList<>();
		List<FunctionalMapData> fmDataList = functionalMapDataRepository.findByBcmLevel(bcmLevel);
		if (!fmDataList.isEmpty()) {
			for (FunctionalMapData oneFMData : fmDataList) {
				listOfFMDataBean.add(new FunctionalMapDataBean(oneFMData));
			}
		}
		return listOfFMDataBean;
	}
	
	@Override
	public Map<Bcm, List<FunctionalMap>> getMapOfFunctionalMapsByBcmForActiveWorkspace(Workspace activeWorkspace) {
		Map<Bcm, List<FunctionalMap>> mapOfFunctionalMapsByBcm = new HashMap<>();
		
		List<FunctionalMap> functionalMaps = findByWorkspace(activeWorkspace, true);
		
		for (FunctionalMap functionalMap : functionalMaps) {
			if(mapOfFunctionalMapsByBcm.containsKey(functionalMap.getBcm())){
				List<FunctionalMap> listOfFm = mapOfFunctionalMapsByBcm.get(functionalMap.getBcm());
				listOfFm.add(functionalMap);
				mapOfFunctionalMapsByBcm.put(functionalMap.getBcm(), listOfFm);
			}else{
				List<FunctionalMap> listOfFm = new ArrayList<>();
				listOfFm.add(functionalMap);
				mapOfFunctionalMapsByBcm.put(functionalMap.getBcm(), listOfFm);
			}
		}
		
		return mapOfFunctionalMapsByBcm;
	}

	@Override
	public Set<AssetBean> createSetOfAssetBeans(List<FunctionalMapDataBean> functionalMapDataBeans) {
		Set<AssetBean> assetBeans = new HashSet<>();
		
		for (FunctionalMapDataBean fmdb : functionalMapDataBeans) {
			assetBeans.add(fmdb.getAssetBean());
		}
		
		return assetBeans;
	}

	@Override
	public String getLevelNumberValue(int levelNumber) {
		String levelNumberValue = "L" + levelNumber;
		return levelNumberValue;
	}

	/**
	 * Evaluate functional coverage and get list of bcm responses
	 * for functional redundancy map.
	 * @param rollUpLevel
	 * @param bcmLevel
	 * @param fcParameter
	 * @return {@code List<BcmResponseBean>}
	 */
	private List<BcmResponseBean> getBcmResponses(BcmLevel bcmLevel, Parameter fcParameter,Integer rollUpLevel, Questionnaire questionnaire) {
		List<BcmResponseBean> bcmResponseBeans = new ArrayList<>();
		if(fcParameter != null && fcParameter.getType().equals(ParameterType.FC.name())){
			List<ParameterConfig> parameterConfigs = parameterService.findParameterConfigListByParameter(fcParameter);
			if(parameterConfigs.size() == 1){
				FunctionalCoverageParameterConfigBean fcConfigBean = Utility.convertJSONStringToObject(parameterConfigs.get(0).getParameterConfig(), FunctionalCoverageParameterConfigBean.class);
				if(fcConfigBean != null){
					FunctionalMap fm = this.findOne(fcConfigBean.getFunctionalMapId(), true);
					
					// fetch functional map data by functional map 
					List<FunctionalMapDataBean> fmDataBeans = this.fetchFunctionalMapData(fm, questionnaire);
					
					// pull all distinct assets from list of functional map data 
					List<AssetBean> assetBeans = new ArrayList<>(this.createSetOfAssetBeans(fmDataBeans));
					
					// To modify roll-up level to FC evaluation, update functional map level type					
					if(rollUpLevel != null && rollUpLevel >0){
					  fcConfigBean.setFunctionalMapLevelType(this.getLevelNumberValue(rollUpLevel));
					}else{
					  fcConfigBean.setFunctionalMapLevelType(this.getLevelNumberValue(bcmLevel.getLevelNumber()+1));
					}					
					List<Parameter> listOfParameters = new ArrayList<>();
					listOfParameters.add(fcParameter);
					FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, this, bcmLevelService, fcConfigBean, questionnaire);
					bcmResponseBeans = fcParameterEvaluator.evaluateBcmLevel(assetBeans, fcParameter);
				}
			}
		}
		
		return bcmResponseBeans;
	}
	
	@Override
	public FunctionalRedundancyWrapper getFunctionalRedundancyWrapper(BcmLevel bcmLevel, Parameter fcParameter, Questionnaire questionnaire, String mode, Map<String, List<String>> filterMap){
		FunctionalRedundancyWrapper functionalRedundancyWrapper = new FunctionalRedundancyWrapper();
		
		Set<Integer> setOfAssetIds = getAssetIdsFromElasticSearch(getAssetTemplateId(fcParameter), filterMap);
		
		IndexBCMBean indexBCMBean = bcmReportService.getBcmFromElasticsearch(bcmLevel.getBcm().getId());
		Map<IndexBCMLevelBean, List<IndexBCMLevelBean>> mapOfIndexBCMLevelBean = createMapForIndexBCMLevelBean(bcmLevel, mode, indexBCMBean);
		
		List<BcmParentLevelWrapper> bcmDetails = new ArrayList<>();
		Set<AssetValueWrapper> assetDetails = new HashSet<>();
		for (Map.Entry<IndexBCMLevelBean, List<IndexBCMLevelBean>> entry : mapOfIndexBCMLevelBean.entrySet()) {
			IndexBCMLevelBean levelBean = entry.getKey();
			List<IndexBCMLevelBean> listOfIndexBCMLevelBeans = entry.getValue();
		
			BcmParentLevelWrapper bcmParentLevelWrapper = new BcmParentLevelWrapper();
			bcmParentLevelWrapper.setParentLevel(levelBean.getBcmLevelName());
			bcmParentLevelWrapper.setParentLevelId(levelBean.getBcmLevelId());
			List<BcmChildLevelWrapper> children = new ArrayList<>();
			for (IndexBCMLevelBean indexBCMLevelBean : listOfIndexBCMLevelBeans) {
				children.add(createBcmChildLevelWrapperObject(fcParameter, questionnaire, indexBCMBean, assetDetails, indexBCMLevelBean, setOfAssetIds));
			}
			bcmParentLevelWrapper.setChildren(children);
			bcmDetails.add(bcmParentLevelWrapper);
			functionalRedundancyWrapper.setBcmDetails(bcmDetails);
			List<AssetValueWrapper> listOfAssetValueWrappers = new ArrayList<>(assetDetails);
			listOfAssetValueWrappers.sort((obj1, obj2) -> obj1.getName().compareTo(obj2.getName()));
			functionalRedundancyWrapper.setAssetDetails(listOfAssetValueWrappers);
		}
		return functionalRedundancyWrapper;
	}
	
	private Integer getAssetTemplateId(Parameter fcParameter) {
		ParameterConfig parameterConfig = parameterService.findParameterConfigByParameter(fcParameter);
		FunctionalCoverageParameterConfigBean fcParameterConfigBean = Utility.convertJSONStringToObject(parameterConfig.getParameterConfig(), FunctionalCoverageParameterConfigBean.class);
		FunctionalMap functionalMap = this.findOne(fcParameterConfigBean.getFunctionalMapId(), false);
		return functionalMap.getAssetTemplate().getId();
	}

	private BcmChildLevelWrapper createBcmChildLevelWrapperObject(Parameter fcParameter, Questionnaire questionnaire,
			IndexBCMBean indexBCMBean, Set<AssetValueWrapper> assetDetails,
			IndexBCMLevelBean indexBCMLevelBean, Set<Integer> setOfAssetIds) {
		BcmChildLevelWrapper bcmChildLevelWrapper = new BcmChildLevelWrapper();
		bcmChildLevelWrapper.setId(indexBCMLevelBean.getBcmLevelId());
		bcmChildLevelWrapper.setName(indexBCMLevelBean.getBcmLevelName());
		bcmChildLevelWrapper.setDataExist(isDataExist(fcParameter, questionnaire, indexBCMBean, indexBCMLevelBean));
		bcmChildLevelWrapper.setAssetValues(createAssetValuesList(fcParameter, questionnaire, assetDetails, indexBCMLevelBean, setOfAssetIds));
		return bcmChildLevelWrapper;
	}

	private boolean isDataExist(Parameter fcParameter, Questionnaire questionnaire, IndexBCMBean indexBCMBean,
			IndexBCMLevelBean indexBCMLevelBean) {
		boolean isDataExist = false;
		List<IndexBCMLevelBean> listOfIndexBCMLevelBeansForDataExist = indexBCMBean.getBcmLevels().stream()
			     .filter(item -> item.getParentBCMLevelId() != null && item.getParentBCMLevelId().equals(indexBCMLevelBean.getBcmLevelId()))
			     .collect(Collectors.toList());
		if(!listOfIndexBCMLevelBeansForDataExist.isEmpty()) {
			for (IndexBCMLevelBean indexBCMLevel : listOfIndexBCMLevelBeansForDataExist) {
				Set<IndexBCMParameterBean> setOfIndexBCMParameterBeans = indexBCMLevel.getBcmEvaluatedValue().stream()
					     .filter(item -> item.getParameterId().equals(fcParameter.getId()) && item.getQuestionnaireId().equals(questionnaire.getId()))
					     .collect(Collectors.toSet());
				if(!setOfIndexBCMParameterBeans.isEmpty()) {
					isDataExist = true;
					break;
				}
			}
		}
		return isDataExist;
	}

	private List<Integer> createAssetValuesList(Parameter fcParameter, Questionnaire questionnaire, Set<AssetValueWrapper> assetDetails,
			IndexBCMLevelBean indexBCMLevelBean, Set<Integer> setOfAssetIds) {
		List<Integer> assetValues = new ArrayList<>();
		Set<IndexBCMParameterBean> setOfIndexBCMParameterBeans = indexBCMLevelBean.getBcmEvaluatedValue().stream()
			     .filter(item -> item.getParameterId().equals(fcParameter.getId()) && item.getQuestionnaireId().equals(questionnaire.getId()))
			     .collect(Collectors.toSet());
			for (IndexBCMParameterBean indexBCMParameterBean : setOfIndexBCMParameterBeans) {
				List<IndexBCMAssetValueBean> listOfIndexBCMAssetValueBeans = new ArrayList<>(indexBCMParameterBean.getValues());
				listOfIndexBCMAssetValueBeans.sort((obj1, obj2) -> obj1.getShortName().compareTo(obj2.getShortName()));
				for (IndexBCMAssetValueBean indexBCMAssetValueBean : listOfIndexBCMAssetValueBeans) {
					if(setOfAssetIds.contains(indexBCMAssetValueBean.getAssetId())) {
						if(assetDetails.isEmpty() || assetDetails.size() != listOfIndexBCMAssetValueBeans.size()){
							AssetValueWrapper assetValueWrapper = createAssetValueWrapperObject(indexBCMAssetValueBean);
							assetDetails.add(assetValueWrapper);
						}
						BigDecimal minusOneValue = new BigDecimal(-1);
						BigDecimal evaluatedIndexedValue = new BigDecimal(indexBCMAssetValueBean.getEvaluatedPercentage() != null ? indexBCMAssetValueBean.getEvaluatedPercentage() : minusOneValue.toString());
						if(evaluatedIndexedValue.equals(minusOneValue)){
							assetValues.add(0);
						} else {
							assetValues.add(Integer.valueOf(new BigDecimal(indexBCMAssetValueBean.getEvaluatedPercentage()).multiply(new BigDecimal(100)).divide(new BigDecimal(10), 2, BigDecimal.ROUND_HALF_UP).intValue()));
						}
					}
				}
			}
		return assetValues;
	}

	private AssetValueWrapper createAssetValueWrapperObject(IndexBCMAssetValueBean indexBCMAssetValueBean) {
		AssetValueWrapper assetValueWrapper = new AssetValueWrapper();
		assetValueWrapper.setId(indexBCMAssetValueBean.getAssetId());
		assetValueWrapper.setName(indexBCMAssetValueBean.getShortName());
		return assetValueWrapper;
	}

	private Map<IndexBCMLevelBean, List<IndexBCMLevelBean>> createMapForIndexBCMLevelBean(BcmLevel bcmLevel, String mode, IndexBCMBean indexBCMBean) {
		Map<IndexBCMLevelBean, List<IndexBCMLevelBean>> mapOfIndexBCMLevelBean = new HashMap<>();
		if("full".equals(mode)) {
			List<IndexBCMLevelBean> listOfIndexBCMLevelBeans = indexBCMBean.getBcmLevels().stream()
				     .filter(item -> item.getLevelNumber().equals(bcmLevel.getLevelNumber()))
				     .collect(Collectors.toList());
			for (IndexBCMLevelBean indexBCMLevelBean : listOfIndexBCMLevelBeans) {
				List<IndexBCMLevelBean> listOfChildrenIndexBCMLevelBeans = indexBCMBean.getBcmLevels().stream()
					     .filter(item -> item.getParentBCMLevelId() != null && item.getParentBCMLevelId().equals(indexBCMLevelBean.getBcmLevelId()))
					     .collect(Collectors.toList());
				mapOfIndexBCMLevelBean.put(indexBCMLevelBean, listOfChildrenIndexBCMLevelBeans);
			}
		}
		else if ("single".equals(mode)) {
			List<IndexBCMLevelBean> listOfIndexBCMLevelBeans = indexBCMBean.getBcmLevels().stream()
			     .filter(item -> item.getParentBCMLevelId() != null && item.getParentBCMLevelId().equals(bcmLevel.getId()))
			     .collect(Collectors.toList());
			mapOfIndexBCMLevelBean.put(new IndexBCMLevelBean(bcmLevel), listOfIndexBCMLevelBeans);
		}
		return mapOfIndexBCMLevelBean;
	}
	
	@Override
	public List<FunctionalMap> findByWorkspaceAndTypeNotIn(Workspace workspace, Set<String> levelTypes, boolean loaded) {
		List<FunctionalMap> listOfFm = functionalMapRepository.findByWorkspaceAndTypeNotIn(workspace, levelTypes);
		
		if(loaded){
			for (FunctionalMap functionalMap : listOfFm) {
				functionalMap.getAssetTemplate().getAssetTemplateName();
				functionalMap.getBcm().getBcmName();
				functionalMap.getQuestion().getTitle();
			}
		}
		
		return listOfFm;
	}
	
	/**
	 * 
	 * @param bcmLevel
	 * @param fcParameter
	 * @param rollUpLevel
	 * @return
	 */
	@Override
	public List<BcmResponseBean> getBcmResponsesAtLevel(BcmLevel bcmLevel, Parameter fcParameter,Integer rollUpLevel, Questionnaire questionnaire){		  
		return getBcmResponses(bcmLevel, fcParameter,rollUpLevel, questionnaire);		
	}

	@Override
	public List<FunctionalMapDataBean> fetchFunctionalMapData(FunctionalMap fm,	Questionnaire questionnaire) {
		List<FunctionalMapDataBean> listOfFMDataBean = new ArrayList<>();
		List<FunctionalMapData> fmDataList = functionalMapDataRepository.findByFunctionalMapAndQuestionnaire(fm, questionnaire);
		if (!fmDataList.isEmpty()) {
			for (FunctionalMapData oneFMData : fmDataList) {
				listOfFMDataBean.add(new FunctionalMapDataBean(oneFMData));
			}
		}
		return listOfFMDataBean;
	}
	
	private Set<Integer> getAssetIdsFromElasticSearch(Integer assetTemplateId, Map<String, List<String>> filterMap) {

		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/assetTemplate";

		RestTemplate restTemplate = new RestTemplate();
		Set<Integer> aidAssetIds = new HashSet<>();
		Set<Integer> assetids = new HashSet<>();
		ObjectMapper mapper = new ObjectMapper();

		SearchConfig searchConfig = setSearchConfigForAssetTemplate(assetTemplateId, filterMap);

		try {
			aidAssetIds = restTemplate.postForObject(uri, searchConfig, Set.class);
			assetids = mapper.convertValue(aidAssetIds, new TypeReference<Set<Integer>>() {
			});

		} catch (RestClientException e) {
			logger.error("Exception occured in FunctionalMapServiceImpl :: getAssetAidFromElasticsearch() :", e);
		}

		return assetids;
	}

	private SearchConfig setSearchConfigForAssetTemplate(Integer assetTemplateId, Map<String, List<String>> filterMap) {

		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig = new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String> list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		searchConfig.setFilterMap(filterMap);

		List<Integer> listOfIds = new ArrayList<>();
		listOfIds.add(assetTemplateId);
		searchConfig.setListOfIds(listOfIds);
		
		return searchConfig;
	}

	@Override
	public Set<String> getBcmLevelDepth(Set<String> uniqueLevelList, List<BcmLevel> listBcmLevel) {
		List<BcmLevel> sequenceNumberBcmLevelList = new ArrayList<>();
		Set<Integer> setBcmLevels = new HashSet<>();
		for(BcmLevel oneBcmLevel:listBcmLevel) {
			if(oneBcmLevel.getSequenceNumber() ==1) {
				sequenceNumberBcmLevelList.add(oneBcmLevel);
			}
		}
		List<Integer> levelNumberList = new ArrayList<>();
		for(BcmLevel oneLevelNumber: sequenceNumberBcmLevelList) {
			levelNumberList.add(oneLevelNumber.getLevelNumber());
		}
		List<Integer> duplicateLevelList = new ArrayList<>();
		ListIterator<Integer> listitr = levelNumberList.listIterator();
		while( listitr.hasNext())
		{
			Integer oneBcmLevel =  listitr.next();
			if(Collections.frequency(levelNumberList, oneBcmLevel) > 1) {
				duplicateLevelList.add(oneBcmLevel);
				listitr.remove();
			}
		}

		for (Integer oneBcmLevel : duplicateLevelList){
			if (!setBcmLevels.add(oneBcmLevel)){
				uniqueLevelList.add("L1");
				uniqueLevelList.add("L" +oneBcmLevel);
			}
		}
		if(uniqueLevelList.isEmpty()) {
			uniqueLevelList.add("L1");
			duplicateLevelList.forEach(oneLevel-> uniqueLevelList.add("L"+ oneLevel));
		}    
		return uniqueLevelList;
	}
}
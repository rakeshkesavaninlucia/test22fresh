package org.birlasoft.thirdeye.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.index.IncrementAssetIndexRequestBean;
import org.birlasoft.thirdeye.beans.index.IncrementBcmIndexRequestBean;
import org.birlasoft.thirdeye.beans.index.IndexResponseBean;
//import org.birlasoft.thirdeye.beans.index.UpdateRequestBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.service.IncrementIndexService;
import org.birlasoft.thirdeye.util.Utility;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class IncrementIndexServiceImpl implements IncrementIndexService {
	
private static Logger logger = LoggerFactory.getLogger(IncrementIndexServiceImpl.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;

	@Autowired
	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
	
	@Override
	public void buildIncrementAssetIndex(List<Integer> assetIdlst , String tenantId) {
	
		 String uri = elasticSearchHost + "thirdeye-search-web/api/buildIncAssetIndex/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementAssetIndexRequestBean requestBean = new IncrementAssetIndexRequestBean();
		 requestBean.setAssetIdlst(assetIdlst);
		 requestBean.setTenantURL(tenantURL);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage());
		 
		
	}
	
	private String createJSONString(Asset lstAsset) {
		String jSONString = null;
		// Assume quantifier length = encoded String Array
		if(lstAsset!=null){
				jSONString = Utility.convertObjectToJSONString(lstAsset);
		}
		return jSONString;
	}

	/*@Override
	public void updateAssetParameter(Integer qusId) {
		// TODO Auto-generated method stub
		
		 String uri = elasticSearchHost + "thirdeye-search-web/api/updateAssetParameter";
		
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 UpdateRequestBean updateRequestBean =
				new UpdateRequestBean();
		 updateRequestBean.setId(qusId);
		 
		 RestTemplate restTemplate = new RestTemplate(); 
		 IndexResponseBean response = restTemplate.postForObject(uri, updateRequestBean, IndexResponseBean.class,uriParams);
		
		 logger.info(response.getMessage());
		
	}*/

	/*@Override
	public void updateQustionnaireQues(Integer qusId) {
		// TODO Auto-generated method stub
		
		 String uri = elasticSearchHost + "thirdeye-search-web/api/updateQuestionnaireQs/{tenantURL}";
		
		
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
		
		 UpdateRequestBean updateRequestBean =	new UpdateRequestBean();
		 updateRequestBean.setId(qusId);
		 updateRequestBean.setTenantURL(tenantURL);
		 
		 RestTemplate restTemplate = new RestTemplate( ); 
		 IndexResponseBean response = restTemplate.postForObject(uri, updateRequestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage());
			
		
	}*/

	@Override
	public void updateAssetRelation(List<Integer> assetIdlst, String tenantId) {
		// TODO Auto-generated method stub
		 String uri = elasticSearchHost + "thirdeye-search-web/api/buildIncAssetRelationIndex/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementAssetIndexRequestBean requestBean = new IncrementAssetIndexRequestBean();
		 requestBean.setAssetIdlst(assetIdlst);
		 requestBean.setTenantURL(tenantURL);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage());
		
	}

	@Override
	public void updateAssetAid(Integer templateId, String tenantId) {
		
		 String uri = elasticSearchHost + "thirdeye-search-web/api/buildIncAssetAidData/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementAssetIndexRequestBean requestBean = new IncrementAssetIndexRequestBean();
		 requestBean.setAssetTemplateId(templateId);
		 requestBean.setTenantURL(tenantURL);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage());
	}

	@Override
	public void updateAssetTemplate(Integer templateId, String tenantId) {
		
		 String uri = elasticSearchHost + "thirdeye-search-web/api/udpateAssetTemplate/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementAssetIndexRequestBean requestBean = new IncrementAssetIndexRequestBean();
		 requestBean.setAssetTemplateId(templateId);
		 requestBean.setTenantURL(tenantURL);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage());
	}

	@Override
	public void deleteAsset(Integer assetId, String tenantId) {
		 
		 String uri = elasticSearchHost + "thirdeye-search-web/api/deleteAsset/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementAssetIndexRequestBean requestBean = new IncrementAssetIndexRequestBean();
		 requestBean.setAssetId(assetId);
		 requestBean.setTenantURL(tenantURL);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage()); 
		
	}

	@Override
	public void buildIncrementBCMIndex(Integer bcmId, String tenantId) {
		
		 String uri = elasticSearchHost + "thirdeye-search-web/api/addBcm/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementBcmIndexRequestBean requestBean = new IncrementBcmIndexRequestBean();
		 requestBean.setBcmId(bcmId);
		 requestBean.setTenantId(tenantURL);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage()); 
		
	}

	@Override
	public void updateAssetParameter(Integer qusId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateQustionnaireQues(Integer qusId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateAssetCostStucture(List<Integer> assetIdlst, String tenantId) {
		 
		 String uri = elasticSearchHost + "thirdeye-search-web/api/updateAssetCostSt/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementAssetIndexRequestBean requestBean = new IncrementAssetIndexRequestBean();
		 requestBean.setAssetIdlst(assetIdlst);
		 requestBean.setTenantURL(tenantId);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage()); 
		
		
	}

	@Override
	public void updateAssetQuestionnaire(List<Integer> assetIdlst, String tenantId) {
		
		 String uri = elasticSearchHost + "thirdeye-search-web/api/updateAssetQuestionnaire/{tenantURL}";
		 String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		 
		 Map<String, String> uriParams = new HashMap<>();
		 uriParams.put("tenantURL", tenantURL);
			
		 IncrementAssetIndexRequestBean requestBean = new IncrementAssetIndexRequestBean();
		 requestBean.setAssetIdlst(assetIdlst);
		 requestBean.setTenantURL(tenantId);
		 RestTemplate restTemplate = new RestTemplate(); 
		 
		 IndexResponseBean response = restTemplate.postForObject(uri, requestBean, IndexResponseBean.class,uriParams);
			
		 logger.info(response.getMessage()); 
		
	}
	
	

}

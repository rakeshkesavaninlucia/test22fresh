package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.birlasoft.thirdeye.beans.tco.TcoAnalysisGraphBean;
import org.birlasoft.thirdeye.beans.tco.TcoCombiGraphBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.api.beans.TcoAssetBean;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.TcoAnalysisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class TcoAnalysisServiceImpl implements TcoAnalysisService {
	
	private static Logger logger = LoggerFactory.getLogger(TcoAnalysisServiceImpl.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private ParameterSearchService parameterSearchService; 
	
	@Override
	public TcoAnalysisGraphBean getTotalCostOfAssets(Integer idOfChartOfAccount) {
		List<TcoAssetBean> tcoAssetBeans = getTotalCostOfAssetsInCoaFromElasticsearch(idOfChartOfAccount);
		TcoAnalysisGraphBean barGraphBean = new TcoAnalysisGraphBean();
		barGraphBean.setTcoAssetBeans(tcoAssetBeans);
		return barGraphBean;
	}

	private List<TcoAssetBean> getTotalCostOfAssetsInCoaFromElasticsearch(Integer idOfChartOfAccount) {
		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/tco/asset/total";

		RestTemplate restTemplate = new RestTemplate();
		List<TcoAssetBean> tcoAssetBeans = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		List<TcoAssetBean> list = new ArrayList<>();

		SearchConfig searchConfig = setSearchConfig(idOfChartOfAccount);
		
		//Firing a rest request to elastic search server
		try {
			tcoAssetBeans =  (List<TcoAssetBean>) restTemplate.postForObject(uri, searchConfig, List.class);
			list = mapper.convertValue(tcoAssetBeans, new TypeReference<List<TcoAssetBean>>() { });
		} catch (RestClientException e) {				
			logger.error("Exception occured in GraphFacetsServiceImpl :: getParameterValueFromElasticsearch() :", e);
		}
		return list;
	}

	private SearchConfig setSearchConfig(Integer idOfChartOfAccount) {
		String tenantId = customUserDetailsService.getCurrentAuthenticationDetails().getTenantURL();
		Integer activeWorkspaceId = customUserDetailsService.getActiveWorkSpaceForUser().getId();
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		
		searchConfig.setQuestionnaireId(idOfChartOfAccount);
		return searchConfig;
	}
	
	@Override
	public TcoAnalysisGraphBean getDataForCombiGraph(Integer idOfChartOfAccount, Integer questionnaireId,
			Integer parameterId) {
		TcoAnalysisGraphBean analysisGraphBean = new TcoAnalysisGraphBean();
		List<TcoAssetBean> tcoAssetBeans = getTotalCostOfAssetsInCoaFromElasticsearch(idOfChartOfAccount);
		AssetParameterWrapper assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(parameterId, questionnaireId, new HashSet<>(), null);
		
		List<String> listOfAllAssets = extractAllAssets(tcoAssetBeans, assetParameterWrapper);
		analysisGraphBean.addCombiBean(extractCombiBeanForCost(tcoAssetBeans, listOfAllAssets));
		analysisGraphBean.addCombiBean(extractCombiBeanForParameter(assetParameterWrapper, listOfAllAssets));
		analysisGraphBean.setGraphName(assetParameterWrapper.getParameter());
		return analysisGraphBean;
	}

	/**
	 * Extract all unique assets from Chart of account and parameter eval
	 * @param tcoAssetBeans
	 * @param assetParameterWrapper
	 * @return
	 */
	private List<String> extractAllAssets(List<TcoAssetBean> tcoAssetBeans, AssetParameterWrapper assetParameterWrapper) {
		List<String> listOfAssetsInParameter = assetParameterWrapper.getValues().stream()
													.map(AssetParameterBean::getAssetName)
													.collect(Collectors.toList());
		
		List<String> listOfAssetsInCoa = tcoAssetBeans.stream()
											.map(TcoAssetBean::getAssetName)
											.collect(Collectors.toList());
		
		return Stream.concat(listOfAssetsInParameter.stream(), listOfAssetsInCoa.stream()).distinct().collect(Collectors.toList());
	}

	/**
	 * Prepare bean for parameter values for combi chart
	 * @param listOfAllAssets 
	 * @param questionnaireId
	 * @param parameterId
	 * @return
	 */
	private TcoCombiGraphBean extractCombiBeanForParameter(AssetParameterWrapper assetParameterWrapper, List<String> listOfAllAssets) {
		
		TcoCombiGraphBean combiGraphBean = new TcoCombiGraphBean();
		combiGraphBean.setKey(assetParameterWrapper.getParameter());
		Map<String, BigDecimal> paramValues = new HashMap<>();
		List<Object> assets = new ArrayList<>(listOfAllAssets);
		for (AssetParameterBean oneAsset : assetParameterWrapper.getValues()) {
			paramValues.put(oneAsset.getAssetName(), oneAsset.getParameterValue());
		}
		
		List<List<Object>> values = extractValuesForCombiGraph(paramValues, assets);
		
		combiGraphBean.setValues(values);
		combiGraphBean.setAssets(assets);
		return combiGraphBean;
	}

	private List<List<Object>> extractValuesForCombiGraph(Map<String, BigDecimal> map, List<Object> assets) {
		List<List<Object>> values = new ArrayList<>();
		assets.forEach(e -> {
			List<Object> value = new ArrayList<>();
			value.add(e);
			if(map.containsKey(e)) {
				value.add(map.get(e));
			} else {				
				value.add(new BigDecimal(0));
			}
			values.add(value);
		});
		return values;
	}

	/**
	 * Prepare bean from chart of account for combi chart
	 * @param listOfAllAssets 
	 * @param idOfChartOfAccount
	 * @return
	 */
	private TcoCombiGraphBean extractCombiBeanForCost(List<TcoAssetBean> tcoAssetBeans, List<String> listOfAllAssets) {
		
		TcoCombiGraphBean combiGraphBean = new TcoCombiGraphBean();
		combiGraphBean.setKey("Cost");
		combiGraphBean.setBar(true);
		Map<String, BigDecimal> assetCost = new HashMap<>();
		List<Object> assets = new ArrayList<>(listOfAllAssets);
		for (TcoAssetBean tcoAssetBean : tcoAssetBeans) {
			assetCost.put(tcoAssetBean.getAssetName(), tcoAssetBean.getTotalCost());
		}
		
		List<List<Object>> values = extractValuesForCombiGraph(assetCost, assets);
		
		combiGraphBean.setValues(values);
		combiGraphBean.setAssets(assets);
		return combiGraphBean;
	}

}

/**
 * 
 */
package org.birlasoft.thirdeye.beans.tco;

import java.math.BigDecimal;
/**
 * Wrapper for asset details(As on Cost Structure) on TCO cost element  
 */

public class TCOAssetWrapper {
	
	private Integer assetId;
	private String assetName;
	private Integer costStructureId;
	private BigDecimal cost;
	
	
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	
	public Integer getCostStructureId() {
		return costStructureId;
	}
	public void setCostStructureId(Integer costStructureId) {
		this.costStructureId = costStructureId;
	}
	
	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
}

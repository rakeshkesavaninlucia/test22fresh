package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.add.ADDWrapper;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.GraphAssetTemplate;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;

/**
 * Service interface for graph.
 * @author samar.gupta
 */
public interface GraphService {
	
	/**
	 * Get Only One Graph with given Id
	 * @param id
	 * @param loaded
	 * @return Graph Object
	 */
	public Graph findOne(Integer id, boolean loaded);
	
	/**
	 * save Graph Object
	 * @param graph
	 * @return Graph Object
	 */
	public Graph save(Graph graph);
	/**
	 * List of all Graph 
	 * @return List{@code <Graph>} Object
	 */
	public List<Graph> findAll();
	/**
	 * Get all List of {@code Graphs} belongs to {@code Workspace}
	 * @param workspaces
	 * @return List{@code <Graph>} Object
	 */
	public List<Graph> findByWorkspaceIn(Set<Workspace> workspaces);
	/**
	 * Find list of graph by workspace and graph type.
	 * @param workspace
	 * @param graphType
	 * @return {@code List<Graph>}
	 */
	public List<Graph> findByWorkspaceAndGraphType(Workspace workspace, String graphType);
	/**
	 * Add the user details in the graph
	 * @param graph
	 * @param currentUser
	 * @param activeWorkspace
	 * @return {@code Graph}
	 */
	public Graph createNewGraphObject(Graph graph, User currentUser, Workspace activeWorkspace);
	/**
	 * Save Asset templates for graph.
	 * @param graphAssetTemplates
	 * @return {@code List<GraphAssetTemplate>}
	 */
	public List<GraphAssetTemplate> saveGraphAssetTemplates(List<GraphAssetTemplate> graphAssetTemplates);
	/**
	 * Get the {@code list} of {@link GraphAssetTemplate} By {@link Graph}.
	 * @param graph
	 * @param loaded
	 * @return {@code List<GraphAssetTemplate>}
	 */
	public List<GraphAssetTemplate> findByGraph(Graph graph, boolean loaded);

	/**
	 * method to update {@code Graph} object
	 * @param graph
	 * @param currentUser
	 * @param activeWorkSpaceForUser
	 * @return
	 */
	public Graph updateGraphObject(Graph graph, User currentUser, Workspace activeWorkSpaceForUser);
	
	/**
	 * Delete {@link GraphAssetTemplate} for {@code Graph}
	 * @param graphAssetTemplates
	 */
	public void deleteInBatch(List<GraphAssetTemplate> graphAssetTemplates);

	/**
	 * method to get {@code Set} of {@link ADDWrapper} for showing {@code Graph}
	 * @param graph
	 * @param filterMap 
	 * @param filterMap 
	 * @return
	 */
	public Set<ADDWrapper> getAddGraph(Graph graph, Map<String, List<String>> filterMap);

	/**
	 * Get asset details {@link ADDWrapper} for ADD by {@link Asset}
	 * @param asset
	 * @return
	 */
	public List<ADDWrapper> getAssetDetailsForAdd(List<Integer> listOfAssetIds, Map<String, List<String>> filterMap, Set<Integer> allAssets);
	
    /**
     * method to save graph
     * @param graph
     * @param assetTemplateIds
     */
	public void saveGraph(Graph graph, Set<Integer> assetTemplateIds);
	/**
	 * Get asset relationship from elastic-search
	 * @param assetId
	 * @return {@code IndexAssetParentChildBean}
	 */
	public List<IndexAssetBean> getAssetsRelationshipFromElasticsearch(List<Integer> listOfAssetIds, Map<String, List<String>> filterMap);
	
	/**
	 * Get the list of Graph Asset Templates from assetTemplate
	 * @param assetTemplateId
	 * @return
	 */
	public List<GraphAssetTemplate> findGraphAssetTemplate(AssetTemplate assetTemplate);
	
	/**
	 * Get the list of {@link Graph} from {@link GraphAssetTemplate}
	 * @param graphAssetTemplate
	 * @return
	 */
	public List<Graph> findByGraphAssetTemplates(GraphAssetTemplate graphAssetTemplate);
}

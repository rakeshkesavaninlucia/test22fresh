package org.birlasoft.thirdeye.service.impl;

import java.util.Date;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.ExportLog;
import org.birlasoft.thirdeye.repositories.ExportLogRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ExportLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class ExportLogServiceImpl implements ExportLogService {

	@Autowired
	private ExportLogRepository exportLogRepository;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Override
	public ExportLog save(ExportLog exportLog) {
		return exportLogRepository.save(exportLog);
	}
	
	@Override
	public ExportLog findByHash(String hash) {
		return exportLogRepository.findByHash(hash);
	}

	@Override
	public void saveExportLog(Integer refId, String hash, String purpose) {
		ExportLog exportLog = new ExportLog();
		exportLog.setHash(hash);
		exportLog.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		exportLog.setUser(customUserDetailsService.getCurrentUser());
		exportLog.setPurpose(purpose.toString());
		exportLog.setRefId(refId);
		exportLog.setTimeDown(new Date());
		exportLogRepository.save(exportLog);
	}
	@Override
	public void updateExportLog(ExportLog exportLog) {
		exportLog.setTimeUp(new Date());
		exportLogRepository.save(exportLog);
	}
}

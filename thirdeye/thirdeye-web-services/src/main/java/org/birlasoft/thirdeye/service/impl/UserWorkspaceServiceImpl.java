package org.birlasoft.thirdeye.service.impl;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.UserWorkspaceRepository;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * 
 * UserWorkspaceService.java - Service implementation class for UserWorkspaceService
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class UserWorkspaceServiceImpl implements UserWorkspaceService {
	
	@Autowired
	private UserWorkspaceRepository userWorkspaceRepository;
	
	@Autowired
	private WorkspaceService workspaceService ;
	
	@Override
	public void delete(UserWorkspace userWorkspace) {
		userWorkspaceRepository.delete(userWorkspace);
	}
	
	@Override
	public UserWorkspace findByUserAndWorkspace(User user,Workspace workspace) {
	return userWorkspaceRepository.findByUserAndWorkspace(user, workspace);
		
	}
	
	@Override
	public UserWorkspace save(UserWorkspace userworkspace) {
		return userWorkspaceRepository.save(userworkspace);
	}
	
	@Override
	public void deleteUserFromWorkspace(Workspace workspace, Integer idOfUser) {
		Set<UserWorkspace> userWorkspaces =  workspace.getUserWorkspaces();
		     for (UserWorkspace userWorkspace : userWorkspaces) {
				if(userWorkspace.getUser().getId() == idOfUser){
					delete(userWorkspace);
					break;
				}
			}
	}

	@Override
	public UserWorkspace createNewUserWorkspaceObject(User user, Workspace workspace) {
		UserWorkspace userworkspceToSave = new UserWorkspace();
		userworkspceToSave.setUser(user);
		userworkspceToSave.setWorkspace(workspace);
		return userworkspceToSave;
	}

	@Override
	public List<UserWorkspace> findByUser(User user) {
		return userWorkspaceRepository.findByUser(user);
	}
		
	@Override
	public UserWorkspace updateUserWorkspaceObject(List<User> user, Workspace workspace) {
		UserWorkspace userworkspceToSave = new UserWorkspace();
		for(User userToSave : user){
		userworkspceToSave.setUser(userToSave);
		userworkspceToSave.setWorkspace(workspace);
		}
		return userworkspceToSave;
	}


}

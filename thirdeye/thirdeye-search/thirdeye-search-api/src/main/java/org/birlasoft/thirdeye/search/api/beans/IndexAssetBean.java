package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.DataType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;

@SuppressWarnings("rawtypes")
public class IndexAssetBean extends AssetBeanBase {
	
	private Map<String, TemplateColumnBean> templateCols = new TreeMap<>();
	private List<IndexParameterBean> parameters =  new ArrayList<>();
	private List<IndexParameterBean> costStuctures =  new ArrayList<>();
	private Set<IndexQuestionBean> questions = new HashSet<>();	
	private List<IndexAidBean> aids = new ArrayList<>();
	private IndexAssetParentChildBean relationship;
	
	public IndexAssetBean() {}
	/**
	 * Constructor to initialize AssetBean.
	 * @param tenantURL
	 * @param sourceAsset
	 */
	@SuppressWarnings("rawtypes")
	public IndexAssetBean(String tenantURL, Asset sourceAsset) {
		super(tenantURL, sourceAsset.getAssetTemplate().getWorkspace().getId());
		this.id = sourceAsset.getId();
		this.uid = sourceAsset.getUid();
		
		if(sourceAsset.getAssetTemplate() != null){
			this.assetTemplateId = sourceAsset.getAssetTemplate().getId();
			this.assetTemplateName = sourceAsset.getAssetTemplate().getAssetTemplateName();
			this.assetClass = sourceAsset.getAssetTemplate().getAssetType().getAssetTypeName();
		}
		
		//@SuppressWarnings("rawtypes")
		List<TemplateColumnBean> listOfTemplateColumns = new ArrayList<>();
		for (AssetData onePieceOfData : sourceAsset.getAssetDatas()){
			
			TemplateColumnBean<Comparable> templateColumnBean = new TemplateColumnBean();
			templateColumnBean.setName(onePieceOfData.getAssetTemplateColumn().getAssetTemplateColName());
			
			switch (DataType.valueOf(onePieceOfData.getAssetTemplateColumn().getDataType())){
			case NUMBER:
				templateColumnBean.setValue("".equals(onePieceOfData.getData()) ? "" : new BigDecimal(onePieceOfData.getData()));
				break;
			case DATE:
				// TODO you have to fix a date format of the string going in						
				templateColumnBean.setValue(onePieceOfData.getData());
			    break;
			case TEXT:			
				templateColumnBean.setValue(onePieceOfData.getData());
				break;
			default :
				break;
			}
			templateColumnBean.setColumnDataType(DataType.valueOf(onePieceOfData.getAssetTemplateColumn().getDataType()));
			templateColumnBean.setSequenceNumber(onePieceOfData.getAssetTemplateColumn().getSequenceNumber());
			templateColumnBean.setFilterable(onePieceOfData.getAssetTemplateColumn().isFilterable());
			listOfTemplateColumns.add(templateColumnBean);
		}
		
		if(!listOfTemplateColumns.isEmpty()){
			Collections.sort(listOfTemplateColumns, new SequenceNumberComparator());
		}
		
		for (TemplateColumnBean templateColumnBean : listOfTemplateColumns) {
			templateCols.put(templateColumnBean.getName(), templateColumnBean);
			if(templateColumnBean.getName().equals(Constants.DEFAULT_COLUMN_NAME)){
				setName((String)templateColumnBean.getValue());
			}
		}		
	}
	
	public Set<IndexQuestionBean> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<IndexQuestionBean> questions) {
		this.questions = questions;
	}

	public List<IndexParameterBean> getParameters() {
		return parameters;
	}

	public void setParameters(List<IndexParameterBean> parameters) {
		this.parameters = parameters;
	}
	
	public List<IndexParameterBean> getCostStuctures() {
		return costStuctures;
	}
	public void setCostStuctures(List<IndexParameterBean> costStuctures) {
		this.costStuctures = costStuctures;
	}

	public Map<String, TemplateColumnBean> getTemplateCols() {
		return templateCols;
	}

	public void setTemplateCols(Map<String, TemplateColumnBean> templateCols) {
		this.templateCols = templateCols;
	}
	
	public List<IndexAidBean> getAids() {
		return aids;
	}
	public void setAids(List<IndexAidBean> aids) {
		this.aids = aids;
	}
	public IndexAssetParentChildBean getRelationship() {
		return relationship;
	}
	public void setRelationship(IndexAssetParentChildBean relationship) {
		this.relationship = relationship;
	}	
}

package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author tej.sarup
 *
 */
public enum IndexParameterValueTags {

	VALUEDATE("valueDate"),
	QUESTIONNAIREID("questionnaireId"),	
	VALUE("value"),
	COLOR("color"),
	COLORDESC("colorDesc");
	
	String tagKey;
	
	IndexParameterValueTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}

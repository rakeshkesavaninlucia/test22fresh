package org.birlasoft.thirdeye.search.api.beans;

import java.util.List;

/**
 * This class should be used to configure searches by higher privileges
 * user. 
 * This is the only way to configure a multi tenant query or a multi workspace query
 * 
 * @author tej.sarup
 *
 */
public class PrivilegedSearchConfig extends SearchConfig{

	public PrivilegedSearchConfig(String searchURL, List<String> tenantURL) {
		super(searchURL, tenantURL);
		
	}
	
}

package org.birlasoft.thirdeye.search.api.beans;

import java.util.List;

public class AssetQuestionWrapper {
	
	private String question;
	private Integer questionId;
	private List<AssetQuestionBean> response;
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public List<AssetQuestionBean> getResponse() {
		return response;
	}
	public void setResponse(List<AssetQuestionBean> response) {
		this.response = response;
	}

}

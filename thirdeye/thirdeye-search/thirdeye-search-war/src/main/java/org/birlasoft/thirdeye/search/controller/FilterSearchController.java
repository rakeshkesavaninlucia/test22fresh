/**
 * 
 */
package org.birlasoft.thirdeye.search.controller;

import java.util.List;

import org.birlasoft.thirdeye.search.api.beans.FacetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.FilterSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shaishav.dixit
 *
 */
@RestController
public class FilterSearchController extends BaseSearchController {
	
	@Autowired
	private FilterSearchService filterSearchService;
	
	@RequestMapping( value = "/filter/facet", method = RequestMethod.POST )
	public List<FacetBean> getFilter(@RequestBody SearchConfig searchConfig){		
		return filterSearchService.getFacetAndFilter(searchConfig);
	}

}

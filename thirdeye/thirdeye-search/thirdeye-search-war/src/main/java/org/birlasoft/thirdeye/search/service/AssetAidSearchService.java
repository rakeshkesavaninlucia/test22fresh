package org.birlasoft.thirdeye.search.service;

import java.util.Set;

import org.birlasoft.thirdeye.search.api.beans.IndexAssetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

/**
 * @author samar.gupta
 *
 */
public interface AssetAidSearchService {

	/**
	 * Get asset aid.
	 * @param searchConfig
	 * @return {@code IndexAssetBean}
	 */
	public IndexAssetBean getAssetAid(SearchConfig searchConfig);
	
	
	/**
	 * Get aid
	 * @param searchConfig
	 * @return {@code Aid}
	 */
	public Set<Integer> getAidAssetIds(SearchConfig searchConfig);
	
	/**
	 * Get Asset Id's by assetTemplate 
	 * @param searchConfig
	 * @return {@code Set<Integer>}
	 */
	public Set<Integer> getAssetIdsByAssetTemplate(SearchConfig searchConfig);

}

package org.birlasoft.thirdeye.search.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Load all property files. 
 */
@Configuration
@PropertySource("file:${app.home}/config/application-${spring.profiles.active}.properties")
public class PropertyLoader {

}

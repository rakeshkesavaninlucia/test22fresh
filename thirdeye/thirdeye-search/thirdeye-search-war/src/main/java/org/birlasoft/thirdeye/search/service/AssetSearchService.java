package org.birlasoft.thirdeye.search.service;

import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

public interface AssetSearchService {

	public void search(SearchConfig searchConfig);
	
}

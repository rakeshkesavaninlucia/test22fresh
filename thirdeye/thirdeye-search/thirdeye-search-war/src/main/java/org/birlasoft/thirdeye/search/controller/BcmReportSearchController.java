package org.birlasoft.thirdeye.search.controller;

import org.birlasoft.thirdeye.search.api.beans.IndexBCMBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.BcmReportSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller class for bcm report elastic search.
 * @author samar.gupta
 *
 */
@RestController
public class BcmReportSearchController extends BaseSearchController {
	
	@Autowired
	private BcmReportSearchService bcmReportSearchService;
	
	/**
	 * Search Bcm by Bcm Id.
	 * @param searchConfig
	 * @return {@code IndexBCMBean}
	 */
	@RequestMapping( value = "/bcm", method = RequestMethod.POST )
	public IndexBCMBean searchBcm(@RequestBody SearchConfig searchConfig){		
		return bcmReportSearchService.getBcmLevels(searchConfig);
	}

}

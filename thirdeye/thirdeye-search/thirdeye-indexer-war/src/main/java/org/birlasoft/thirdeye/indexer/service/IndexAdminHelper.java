package org.birlasoft.thirdeye.indexer.service;

import java.util.List;



public interface IndexAdminHelper {

void buildIndexForTenantWorkspace(String tenantURL, List<Integer> workspaceIds);
	
	public void buildIncrementAssetIndex(List<Integer> assetIdLst , String tenantId) ;
	
	public void buildAssetParameterIncrementIndex(Integer assetId) ;
	
	public void buildIncAssetAidDataIndex(Integer assetTempId , String tenantId) ;
	
	public void updateAssetTemplate(Integer assetTempId , String tenantId) ;
	
	public void deleteAsset(Integer assetId, String tenantId) ;
	
	public void addBcm(Integer bcmId, String tenantId) ;
	
	public void updateAssetCostStucture(List<Integer> assetIdlst, String tenantId);
	
	public void updateAssetQuestionnaire(List<Integer> assetIdlst, String tenantId) ;
	
	public void buildIncrementAssetRelationIndex(List<Integer> assetIdlst, String tenantId);
}

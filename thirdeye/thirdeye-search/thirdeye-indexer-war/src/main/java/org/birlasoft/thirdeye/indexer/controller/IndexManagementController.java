package org.birlasoft.thirdeye.indexer.controller;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.index.IncrementAssetIndexRequestBean;
import org.birlasoft.thirdeye.beans.index.IncrementBcmIndexRequestBean;
import org.birlasoft.thirdeye.indexer.service.IndexAdminHelper;
import org.birlasoft.thirdeye.search.api.beans.IndexRequestBean;
import org.birlasoft.thirdeye.search.api.beans.IndexResponseBean;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/api")
public class IndexManagementController {

	@Autowired
	IndexAdminHelper indexAdminHelper;
	
	@Autowired
	CurrentTenantIdentifierResolver identifierResolver;
		
	
	/**
	 * This method will be used to build the complete index for a tenant based on the 
	 * @param tenantURL
	 * @param workspaceIds
	 */
	@RequestMapping( value = "/buildFullIndex/tenant/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  buildIndexByWorkspace(@RequestBody IndexRequestBean indexRequestBean){
		
		String tenantURL = indexRequestBean.getTenantURL();
		List<Integer> listOfWorkspaceId = extractWorkspaces(indexRequestBean.getWsIds());
		IndexResponseBean responseBean = new IndexResponseBean(); 
		// Some basic security - however this ties us to the hibernate implementation
		if (identifierResolver.resolveCurrentTenantIdentifier().equals(tenantURL)){
			indexAdminHelper.buildIndexForTenantWorkspace(tenantURL, listOfWorkspaceId);
					
			responseBean.setMessage("Index created successfully");
		} else {
			// this is an illegal operation
			responseBean.setMessage("Index creation failed");
			
		}
		return new ResponseEntity<>(responseBean,HttpStatus.OK);
	}
	
	@RequestMapping( value = "/buildIncAssetIndex/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  buildIncrementAssetIndex(@RequestBody IncrementAssetIndexRequestBean indexRequestBean){
		
		String tenantURL = indexRequestBean.getTenantURL();
		
		IndexResponseBean responseBean = new IndexResponseBean(); 
		
		indexAdminHelper.buildIncrementAssetIndex(indexRequestBean.getAssetIdlst(), indexRequestBean.getTenantURL());
		//indexAdminHelper.buildIncrementAssetIndex( indexRequestBean.getAssetData(), tenantURL);
		//indexAdminHelper.buildAssetParameterIncrementIndex(new Integer(45));
		//responseBean.setMessage("Index created successfully");
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
		
	
	/*@RequestMapping( value = "/updateAssetParameter/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean> updateAssetParameter(@RequestBody UpdateRequestBean updateRequestBean){
		
		
		IndexResponseBean responseBean = new IndexResponseBean(); 
		indexAdminHelper.buildAssetParameterIncrementIndex(updateRequestBean.getId());
		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);
		}*/
		
		
	/*@RequestMapping( value = "/updateQuestionnaireQs/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean> updateQustionnaireQues(@RequestBody UpdateRequestBean updateRequestBean){
				
		IndexResponseBean responseBean = new IndexResponseBean();
		indexAdminHelper.updateQustionnaireQues(updateRequestBean.getId(),updateRequestBean.getTenantURL());
		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);
		}*/
	
	
	@RequestMapping( value = "/buildIncAssetRelationIndex/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  buildIncrementAssetRelationIndex(@RequestBody IncrementAssetIndexRequestBean indexRequestBean){
				
		IndexResponseBean responseBean = new IndexResponseBean();		
		indexAdminHelper.buildIncrementAssetRelationIndex(indexRequestBean.getAssetIdlst(), indexRequestBean.getTenantURL());		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
	
	
	@RequestMapping( value = "/buildIncAssetAidData/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  buildIncAssetAidDataIndex(@RequestBody IncrementAssetIndexRequestBean indexRequestBean){
						
		IndexResponseBean responseBean = new IndexResponseBean();		
		indexAdminHelper.buildIncAssetAidDataIndex(indexRequestBean.getAssetTemplateId(), indexRequestBean.getTenantURL());		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
	
	
	@RequestMapping( value = "/udpateAssetTemplate/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  updateAssetTemplate(@RequestBody IncrementAssetIndexRequestBean indexRequestBean){
						
		IndexResponseBean responseBean = new IndexResponseBean();		
		indexAdminHelper.updateAssetTemplate(indexRequestBean.getAssetTemplateId(), indexRequestBean.getTenantURL());		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
	
	
	@RequestMapping( value = "/deleteAsset/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  deleteAsset(@RequestBody IncrementAssetIndexRequestBean indexRequestBean){
						
		IndexResponseBean responseBean = new IndexResponseBean();		
		indexAdminHelper.deleteAsset(indexRequestBean.getAssetId(), indexRequestBean.getTenantURL());		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
	
	
	@RequestMapping( value = "/addBcm/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  addBcm(@RequestBody IncrementBcmIndexRequestBean indexRequestBean){
						
		IndexResponseBean responseBean = new IndexResponseBean();		
		indexAdminHelper.addBcm(indexRequestBean.getBcmId(), indexRequestBean.getTenantId());		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
	

	@RequestMapping( value = "/updateAssetCostSt/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  updateAssetCostStucture(@RequestBody IncrementAssetIndexRequestBean indexRequestBean){
						
		IndexResponseBean responseBean = new IndexResponseBean();		
		indexAdminHelper.updateAssetCostStucture(indexRequestBean.getAssetIdlst(),indexRequestBean.getTenantURL());		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
	
	
	@RequestMapping( value = "/updateAssetQuestionnaire/{tenantURL}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<IndexResponseBean>  updateAssetQuestionnaire(@RequestBody IncrementAssetIndexRequestBean indexRequestBean){
						
		IndexResponseBean responseBean = new IndexResponseBean();		
		indexAdminHelper.updateAssetQuestionnaire(indexRequestBean.getAssetIdlst(),indexRequestBean.getTenantURL());		
		return new ResponseEntity<>(responseBean,HttpStatus.OK);	
		}
	
	private List<Integer> extractWorkspaces(String[] arrayOfWorkspaceIds) {
		List<Integer> listOfWorkspaceId = new ArrayList<>();
		for (String oneWorkspaceAsString : arrayOfWorkspaceIds){
				listOfWorkspaceId.add(Integer.parseInt(oneWorkspaceAsString.trim()));
	    }
		return listOfWorkspaceId;
	}
}

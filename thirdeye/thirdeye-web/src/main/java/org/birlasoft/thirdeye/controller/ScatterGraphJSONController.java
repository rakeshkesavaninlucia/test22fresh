package org.birlasoft.thirdeye.controller;

import java.util.HashMap;
import java.util.Map;

import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/graph/scatter")
@Deprecated
public class ScatterGraphJSONController {
	
	@RequestMapping(value = "/graphData/{id}", method = { RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_SCATTER_GRAPH'})")
	public Map<String, Object> jsonGraphOne(@PathVariable(value = "id") Integer idOfGraph) {
		
		// TODO authorize the request to see if they can reach the graphData
		
		Map<String, Object> jsonMapToBeReturned = new HashMap<>();
		
		jsonMapToBeReturned.put("data", null);
		return jsonMapToBeReturned;	
	}
	
}

package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class BcmValidator implements Validator {
	private static final String BCM_NAME = "bcmName";
	
	@Autowired
	private BCMService bCMService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private Environment env;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Bcm.class.equals(clazz);
	}
                
	public void validate(Object target, Errors errors, Integer idOfBCMTemplate) {
		Bcm newBcm = (Bcm) target;
		
		if (StringUtils.isEmpty(newBcm.getBcmName())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, BCM_NAME, "bcm.errors.workspace.bcmname.empty", env.getProperty("bcm.errors.workspace.bcmname.empty"));
		}else if(idOfBCMTemplate == -1){
			errors.rejectValue("bcm", "bcm.errors.select.bcmtemplate", env.getProperty("bcm.errors.select.bcmtemplate"));
		}
		
		Bcm bcm = bCMService.findByBcmNameAndWorkspace(newBcm.getBcmName(), customUserDetailsService.getActiveWorkSpaceForUser());
		if(bcm != null) {
			errors.rejectValue(BCM_NAME, "bcm.error.bcm.name.unique", env.getProperty("bcm.error.bcm.name.unique"));
		}
		if (newBcm.getBcmName() != null && newBcm.getBcmName().length() > 45) {
			errors.rejectValue(BCM_NAME, "bcm.errors.workspace.bcmname.length", env.getProperty("bcm.errors.workspace.bcmname.length"));
		}
	}

	@Override
	public void validate(Object target, Errors errors) {
		Bcm newBcm = (Bcm) target;
		  
		  if (StringUtils.isEmpty(newBcm.getBcmName())) {
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, BCM_NAME, "bcm.errors.bcmtemplatename.empty");
		  }
		  if (newBcm.getBcmName() != null && newBcm.getBcmName().length() > 45) {
				errors.rejectValue(BCM_NAME, "error.bcmtemplate.name.length", env.getProperty("error.bcmtemplate.name.length"));
			}
	}
}

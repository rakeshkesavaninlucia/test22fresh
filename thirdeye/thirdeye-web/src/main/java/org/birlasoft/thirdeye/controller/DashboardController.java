package org.birlasoft.thirdeye.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.birlasoft.thirdeye.beans.widgets.BaseWidgetJSONConfig;
import org.birlasoft.thirdeye.constant.Permissions;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.UserRepository;
import org.birlasoft.thirdeye.repositories.UserWorkspaceRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.DashboardService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.validators.DashboardValidator;
import org.birlasoft.thirdeye.validators.WidgetValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller is responsible for managing dashboards
 * 
 * @author tej.sarup
 *
 */
@Controller
@RequestMapping(value="/dashboard")
public class DashboardController {
	
	@Autowired
	CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	WidgetValidator widgetValidator;
	
	@Autowired
	DashboardValidator dashboardValidator;
	
	@Autowired
	WorkspaceSecurityService workspaceSecurityService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	UserWorkspaceService userWorkspaceService;
	
	@Autowired
	UserWorkspaceRepository userWorkspaceRepository;
	
	@Autowired
	WorkspaceService workspaceService;
	
	
	/**
	 * Create a new dashboard in the active workspace. This returns the modal for the form.
	 * @param model
	 * @param dashboardId
	 * @return
	 */
	@RequestMapping(value = {"/create","/edit"}, method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	public String createANewDashboard (Model model, @RequestParam(value="dashboardId",required=false) Integer dashboardId) {
		
		if (dashboardId != null){
			// See if the dashboard is available in the active workspace
			Dashboard activeDashboard = dashboardService.findOne(dashboardId);
			workspaceSecurityService.authorizeDashboardAccess(activeDashboard);
			model.addAttribute("dashboardForm", activeDashboard);
		} else{
			model.addAttribute("dashboardForm", new Dashboard());
		}		
		return "dashboards/dashboardFragments :: createNewDashboard";
	}
	
	/**
	 * Method to Save or Update new Dashboard
	 * @param incomingDashboard
	 * @param result
	 * @param model
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/create/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	public Object saveANewDashboard (@ModelAttribute("dashboardForm") Dashboard incomingDashboard, BindingResult result, HttpServletResponse response) {
		
		dashboardValidator.validate(incomingDashboard, result);
		ModelAndView mav = new ModelAndView("dashboards/dashboardFragments :: createNewDashboard");
		if(result.hasErrors()){
			return mav;
		}			
		// Ask the service to go and update or save as required
		Dashboard newDashboard = dashboardService.saveOrUpdate(incomingDashboard);
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect", "dashboard/view/"+newDashboard.getId());
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * Method to delete a new Dashboard based on ID
	 * @param model
	 * @param dashboardId
	 */
	@RequestMapping(value = {"/delete/{id}"}, method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteADashboard (@PathVariable(value = "id") Integer dashboardId) {
		
		if (dashboardId != null){
			// See if the dashboard is available in the active workspace
			Dashboard activeDashboard = dashboardService.findOne(dashboardId);
			workspaceSecurityService.authorizeDashboardAccess(activeDashboard);
			
			List<Dashboard> listOfDelete = new ArrayList<>();
			listOfDelete.add(activeDashboard);
			dashboardService.delete(listOfDelete);			
		}
	}

	/**
	 * View a dashboard using the id of the dashboard
	 * 
	 * @param model
	 * @param dashboardId
	 * @See {@link Permissions#WORKSPACES_VIEW_DASHBOARD} and {@link Permissions#WORKSPACES_MODIFY_DASHBOARD}
	 * @return
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_VIEW_DASHBOARD','WORKSPACES_MODIFY_DASHBOARD'})")
	public String showADashBoard (Model model, @PathVariable(value = "id") Integer dashboardId) {
		Boolean buttonStatus = false;
		// See if the dashboard is available in the active workspace
		Dashboard activeDashboard = dashboardService.findOne(dashboardId);
		User currentUserId = userService.findUserById(customUserDetailsService.getCurrentUser().getId());
		Workspace currentWorkspaceId = workspaceService.findOne(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		UserWorkspace us = userWorkspaceService.findByUserAndWorkspace(currentUserId,currentWorkspaceId);
		if(activeDashboard.getDashboardName().equals(us.getSelectedHomePage())){
			buttonStatus = true;
			
		}
		// Validate if the active dashboard is in the current workspace of the user.
		workspaceSecurityService.authorizeDashboardAccess(activeDashboard);
		
		//Get all the widgets for a dashboard
		List<BaseWidgetJSONConfig> listWidgetConfigs = dashboardService.viewDashboard(activeDashboard);
		model.addAttribute("dashboard", activeDashboard);	
		// Put the place holders for all the on page widgets
		model.addAttribute("widgets", listWidgetConfigs);
		model.addAttribute("status", buttonStatus);
		// Put the dashboard id
		
		
		return "dashboards/defaultDashboardTemplate";
	}

	/**
	 * Method to add a new Widget to dashboard
	 * @param model
	 * @param dashboardId
	 * @return
	 */
	@RequestMapping(value = "/getWidgetOptions", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	public String getWidgetOptions(Model model, @RequestParam(value="dashboardId") Integer dashboardId){
		
		Dashboard activeDashboard = dashboardService.findOne(dashboardId);
		workspaceSecurityService.authorizeDashboardAccess(activeDashboard);
		
		// Return a list of available widgets in the system
		BaseWidgetJSONConfig newConfig = new BaseWidgetJSONConfig();
		newConfig.setDashboardId(dashboardId);
		model.addAttribute("addNewWidgetsForm", newConfig);
		return  "dashboards/dashboardFragments :: addNewWidgets";
	}
	
	/**
	 * Add new Widget
	 * @param newWidgetConfig
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addWidget", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	public String add (@ModelAttribute("addNewWidgetsForm") BaseWidgetJSONConfig newWidgetConfig, BindingResult result, Model model) {
		
		// Find the dashboard to which the widget is to be added
		widgetValidator.validate(newWidgetConfig, result);
		if(result.hasErrors()){
		return "dashboards/dashboardFragments :: addNewWidgets";
		}
		
		Dashboard activeDashboard = dashboardService.findOne(newWidgetConfig.getDashboardId());
		workspaceSecurityService.authorizeDashboardAccess(activeDashboard);
					
		// Save the widget to the database
		List<BaseWidgetJSONConfig> widgetConfigs = dashboardService.addNewWidgetToDashboard(activeDashboard, newWidgetConfig);
				
		model.addAttribute("widgetsInModel", widgetConfigs);
		model.addAttribute("dashboardId", newWidgetConfig.getDashboardId());
				
		// Send back the basic placeholder only. Javascript will fire up the widget automatically.
		return "dashboards/defaultDashboardTemplate :: widgetcontainer(widgets=${widgetsInModel})";
	}
	
	@RequestMapping(value = "{id}/updateSequence", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void updateNodeFromTree( @PathVariable(value = "id") Integer dashboardId, @RequestParam(value = "sequence[]") List<Integer> sequence){
		
		dashboardService.updateSequence(dashboardId, sequence);
	}
	
	@RequestMapping(value = "/saveAsHome", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void saveAsHomepage(@RequestParam(value="dashboardid",required=false) String dashboardUrl ,@RequestParam(value="functionName",required=false) String functionName){
	   User currentUser = userService.findUserById(customUserDetailsService.getCurrentUser().getId());
	   Workspace currentWorkspace = workspaceService.findOne(customUserDetailsService.getActiveWorkSpaceForUser().getId());
	   
	   UserWorkspace userWorkspace = userWorkspaceService.findByUserAndWorkspace(currentUser,currentWorkspace);
		userWorkspace.setHomePageUrl(dashboardUrl);
		userWorkspace.setSelectedHomePage(functionName);
		userWorkspaceRepository.save(userWorkspace);
 }	 	
}


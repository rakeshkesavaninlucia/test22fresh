package org.birlasoft.thirdeye.controller.fr;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.beans.fr.FunctionalRedundancyWrapper;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.BCMReportService;
import org.birlasoft.thirdeye.service.BCMService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author samar.gupta
 *
 */
@Controller
@RequestMapping(value="/functionalRed")
public class FunctionalRedundancyController {

	private CustomUserDetailsService customUserDetailsService;
	private BCMLevelService bcmLevelService;
	private FunctionalMapService functionalMapService;
	private BCMService bcmService;
	private ParameterService parameterService;
	private BCMReportService bCMReportService;
	private WorkspaceSecurityService workspaceSecurityService;
	private QuestionnaireService questionnaireService;

	/**
	 * @param customUserDetailsService
	 * @param bcmLevelService
	 * @param functionalMapService
	 * @param bcmService
	 * @param bCMReportService
	 * @param parameterService
	 * @param workspaceSecurityService
	 * @param questionnaireService
	 */
	@Autowired
	public FunctionalRedundancyController(CustomUserDetailsService customUserDetailsService,
			BCMLevelService bcmLevelService,
			FunctionalMapService functionalMapService,
			BCMService bcmService,
			BCMReportService bCMReportService,
			ParameterService parameterService,
			WorkspaceSecurityService workspaceSecurityService,
			QuestionnaireService questionnaireService) {
		this.customUserDetailsService = customUserDetailsService;
		this.bcmLevelService = bcmLevelService;
		this.functionalMapService = functionalMapService;
		this.bcmService = bcmService;
		this.bCMReportService = bCMReportService;
		this.parameterService = parameterService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.questionnaireService = questionnaireService;
	}
	
	/**
	 * View Bcm with functional coverage parameters.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value="/bcmView")
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FUNCTIONAL_REDUNDANCY'})")
	public String viewBcmForFunctionalRedundancy(Model model){		
		// Fetch the user's active workspaces
		Set<Workspace> listOfWorkspaces = new HashSet<> ();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());		
		
		model.addAttribute("listOfBcms", bcmService.findByWorkspaceInAndStatus(listOfWorkspaces, true));
		model.addAttribute("listOfParams", bCMReportService.getListOfQuestionnaireFCParameterBean(listOfWorkspaces));
		model.addAttribute("mode", 1);
		model.addAttribute(Constants.PAGE_TITLE,"Functional Redundancy");
		model.addAttribute("pageSubTitle","View Redundancies map of your asset support to BCM");
		return "bcm/bcmTemplate";
	}
	
	/**
	 * Create FR wrapper to fill data in FR view.
	 * @param model
	 * @param fcParameterId
	 * @param bcmLevelId
	 * @param qeId
	 * @param session
	 * @return {@code FunctionalRedundancyWrapper}
	 */
	@RequestMapping(value="/view", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FUNCTIONAL_REDUNDANCY'})")
	@ResponseBody
	public FunctionalRedundancyWrapper viewFunctionalRedundancy(@RequestParam(value = "fcid") Integer fcParameterId,
			@RequestParam(value = "levelid") Integer bcmLevelId,
			@RequestParam (value="qeid") Integer qeId,
			@RequestParam(value = "mode") String mode, HttpSession session){	
		
		Map<String, List<String>> filterMap = (Map<String, List<String>>) session.getAttribute("filterMap");
		BcmLevel bcmLevel = bcmLevelService.findOneLoadedBcmLevel(bcmLevelId);
		
		Parameter fcParameter = parameterService.findOne(fcParameterId);
		
		Questionnaire questionnaire = questionnaireService.findOne(qeId);
		
		// Check if the user can access this parameter. 
		workspaceSecurityService.authorizeParameterAccess(fcParameter);
		
		// Check if the user can access this BCM level. 
		workspaceSecurityService.authorizeBcmAccess(bcmService.findOne(bcmLevel.getBcm().getId()));
		
		return functionalMapService.getFunctionalRedundancyWrapper(bcmLevel, fcParameter, questionnaire, mode, filterMap);		
	}

	
	/**
	 * View functional redundancy
	 * and set functional coverage id, bcm level id in model 
	 * @param model
	 * @param fcParameterId
	 * @param bcmLevelId
	 * @param qeId
	 * @return {@code String}
	 */
	@RequestMapping(value="/view/mode/{mode}/{fcid}/qe/{qeid}/level/{levelid}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_FUNCTIONAL_REDUNDANCY'})")
	public String viewFR(Model model, @PathVariable(value = "fcid") Integer fcParameterId,
			@PathVariable(value = "levelid") Integer bcmLevelId, @PathVariable(value = "qeid") Integer qeId,
			@PathVariable(value = "mode") String mode, HttpServletRequest request, HttpSession session){		
		Map<String, List<String>> filterMap = extractFilterMapFromRequestParam(request);
		session.setAttribute("filterMap",filterMap);
		model.addAttribute("fcid",fcParameterId);
		model.addAttribute("levelid",bcmLevelId);
		model.addAttribute("qeid",qeId);
		model.addAttribute("mode",mode);
		return "fr/viewFunctionalRedundancy";
	}
	
	private Map<String, List<String>> extractFilterMapFromRequestParam(HttpServletRequest request) {
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		return filterMap;
	}
}

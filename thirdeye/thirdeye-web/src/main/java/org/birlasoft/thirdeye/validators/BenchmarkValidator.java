package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary} fields , logic of
 * {@code validation} is here.
 */
@Component
public class BenchmarkValidator implements Validator {
	private static final String BENCHMARK_NAME = "name";

	@Autowired
	private Environment env;

	@Override
	public boolean supports(Class<?> clazz) {
		return Benchmark.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Benchmark benchmark = (Benchmark) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, BENCHMARK_NAME, "error.benchmark.name",env.getProperty("error.benchmark.name"));

	}
}

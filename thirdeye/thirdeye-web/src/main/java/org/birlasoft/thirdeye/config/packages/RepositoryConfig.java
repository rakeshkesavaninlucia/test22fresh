package org.birlasoft.thirdeye.config.packages;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan({"org.birlasoft.thirdeye.repositories"})
public class RepositoryConfig {

}

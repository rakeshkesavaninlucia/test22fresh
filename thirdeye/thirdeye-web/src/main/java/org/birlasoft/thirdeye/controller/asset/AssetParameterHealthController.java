package org.birlasoft.thirdeye.controller.asset;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.asset.AssetChartRequestParamBean;
import org.birlasoft.thirdeye.beans.asset.AssetChartResponseParamBean;
import org.birlasoft.thirdeye.service.AssetParameterHealthService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for getModalContentForAddingParameters {@code Map<String, Object>} 
 */
@RestController
@RequestMapping(value="/healthChart")
public class AssetParameterHealthController {
	
	@Autowired
	private AssetParameterHealthService assetParameterHealthService;

	/**
	 * Get model content for Adding Parameters
	 * @param chartRequestParamBean
	 * @return {@code Map<String, Object>}
	 */
	@RequestMapping(value="/fetchSpiderData", method = { RequestMethod.POST, RequestMethod.GET })	
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_FILLDETAILS'})")
	public Map<String, Object> getModalContentForAddingParameters(@ModelAttribute("assetChartRequestParamBean") AssetChartRequestParamBean chartRequestParamBean){

		 Map<String, Object> mapOfJsondata = new HashMap<>();
		 List<List<AssetChartResponseParamBean>> assetParamHealthData = assetParameterHealthService.getAssetParamHealthData(chartRequestParamBean);
		
		mapOfJsondata.put("data", assetParamHealthData);
	    return mapOfJsondata;
	}	
	
}

package org.birlasoft.thirdeye.security;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.master.entity.Tenant;
import org.birlasoft.thirdeye.master.repository.TenantRepository;
import org.birlasoft.thirdeye.service.impl.CurrentTenantIdentifierResolverImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.handler.DispatcherServletWebRequest;

/**
 * This is effectively the login form details coming in.
 * 
 * @author shaishav.dixit
 *
 */
public class CustomDetailsFilter extends UsernamePasswordAuthenticationFilter {
	
	private static Logger LOGGER = LoggerFactory.getLogger(CustomDetailsFilter.class);
	
	@Autowired
	private TenantRepository tenantRepository;

	/**
	 * 
	 * Sets details in the request only for the purpose of the {@link CurrentTenantIdentifierResolverImpl#resolveCurrentTenantIdentifier()}
	 */
	@Override
	protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
		
		
		Map<String, String> detailMap = new HashMap<String, String>();
		detailMap.put("tenantCode", request.getParameter("tenantCode"));
		
		if (!StringUtils.isEmpty(request.getParameter("tenantCode"))){
			Tenant tenant = tenantRepository.findByTenantAccountId(StringUtils.trimAllWhitespace(request.getParameter("tenantCode")));
			
			if (tenant != null && !StringUtils.isEmpty(tenant.getTenantAccountId()) && !StringUtils.isEmpty(tenant.getTenantUrlId())){
				// Setup the details for the session
				UserAuthenticationDetailsBean detailsBean = new UserAuthenticationDetailsBean(tenant.getTenantAccountId(), tenant.getTenantUrlId());
				
				// Setup the request attributes so that going forward for this request the 
				// correct tenant db is hit
				RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
				if ( requestAttributes == null || requestAttributes.getAttribute("CURRENT_TENANT_IDENTIFIER", RequestAttributes.SCOPE_REQUEST) == null){
					RequestContextHolder.setRequestAttributes(new DispatcherServletWebRequest(request));;
					requestAttributes = RequestContextHolder.getRequestAttributes();
				}
				
				requestAttributes.setAttribute("CURRENT_TENANT_IDENTIFIER", tenant.getTenantUrlId(), RequestAttributes.SCOPE_REQUEST);
				
				authRequest.setDetails(detailsBean);
			} else {
				LOGGER.info("No record found for account ID: " + request.getParameter("tenantCode"));
				throw new BadCredentialsException("No record found for account ID: " + request.getParameter("tenantCode"));
			}
		} else {
			LOGGER.info("Account ID is empty");
		}
	}
}

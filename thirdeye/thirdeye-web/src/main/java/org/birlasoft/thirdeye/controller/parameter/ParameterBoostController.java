/**
 * 
 */
package org.birlasoft.thirdeye.controller.parameter;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.parameter.ParameterValueBoostBean;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameterAsset;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.ParameterBoostService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for boosting the parameter evaluated value
 * @author dhruv.sood
 *
 */
@Controller
@RequestMapping(value="/parameter/boost")
public class ParameterBoostController {
	
	@Autowired
	private ParameterService parameterService;
	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private AssetService assetService;
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService;
	@Autowired
	private ParameterBoostService parameterBoostService;
	@Autowired
	private QuestionnaireParameterAssetService questionnaireParameterAssetService;

		
	/**@author dhruv.sood
	 * Method to get the Assets for a questionnaire
	 * @param model
	 * @param questionnaireId
	 * @return
	 */
	@RequestMapping(value = "/{id}/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BOOST_EVALUATED_VALUE'})")
	public String getAssets(Model model, @PathVariable(value = "id") Integer questionnaireId) {
		
		//Get the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(questionnaireId);
		
		//Check for authorization of QE
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		
		//Get the list of questionnaire asset form the questionnaire
		Set<QuestionnaireAsset> setOfQuestionnaireAsset = questionnaireAssetService.findByQuestionnaireLoadedAsset(questionnaire);
		
		Set<Asset> setOfAssets = new HashSet<>();
		//Get the list of all the Assets	
		for (QuestionnaireAsset oneQuestionnaireAsset : setOfQuestionnaireAsset) {
			setOfAssets.add(oneQuestionnaireAsset.getAsset());
		}		
		//Get the list of Asset Beans
		List<AssetBean> listOfAssetBean = assetService.getAssetBeansFromAssets(setOfAssets);
		
		model.addAttribute("listOfAssetBean", listOfAssetBean);
		model.addAttribute("questionnaireId", questionnaireId);	
		
		return "param/viewPVB";
	}
	
	
	
	/**@author dhruv.sood
	 * Method to get the parameter for a Questionnaire and Selected asset
	 * @param model
	 * @param questionnaireId
	 * @param assetId
	 * @return
	 */
	@RequestMapping(value = "fetchParameters", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BOOST_EVALUATED_VALUE'})")
	public String getParameters(Model model, @RequestParam(value = "qeid") Integer questionnaireId, @RequestParam(value = "assetid") Integer assetId) {
		
		//Get the questionnaire
		Questionnaire questionnaire = questionnaireService.findOne(questionnaireId);
		
		//Check for authorization of QE
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);
		
		//Call the service and prepare the list of the PVB
		List<ParameterValueBoostBean> listOfParameterValueBoostBean = parameterBoostService.generatetListOfPVBbean(questionnaire, assetId);
		
		model.addAttribute("assetId", assetId);
		model.addAttribute("listOfParameterValueBoostBean", listOfParameterValueBoostBean);		
		
		return "param/viewPVB :: parameterFragment";		
	}
	
	
	/**@author dhruv.sood
	 * Method to edit the boost value
	 * @param qqid
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{qeid}/edit/{aid}/parameter/{pid}", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BOOST_EVALUATED_VALUE'})")
	public String editParameter(Model model, @PathVariable(value = "qeid") Integer qeId,@PathVariable(value = "pid") Integer parameterId,@PathVariable(value = "aid") Integer assetId){
		
		//Check for workspace security
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaireService.findOne(qeId));
		
		ParameterValueBoostBean pvbFormBean = new ParameterValueBoostBean();
		
		Questionnaire questionnaire = questionnaireService.findOne(qeId);
		Parameter parameter = parameterService.findOne(parameterId);
		Asset asset = assetService.findOne(assetId);
		
		pvbFormBean.setParameterId(parameter.getId());
		pvbFormBean.setParameterName(parameter.getDisplayName());		
		pvbFormBean.setQeId(qeId);
		pvbFormBean.setAssetId(assetId);
		
		QuestionnaireParameterAsset questionnaireParameterAsset = questionnaireParameterAssetService.findByQuestionnaireAndParameterAndAsset(questionnaire, parameter, asset);
		if(questionnaireParameterAsset!=null){						
				pvbFormBean.setBoostPercentage(questionnaireParameterAsset.getBoostPercentage().toString());			
		}else{
			pvbFormBean.setBoostPercentage("0");
		}
		
		model.addAttribute("pvbFormBean", pvbFormBean);	
		model.addAttribute("questionnaireId", questionnaire.getId());
		model.addAttribute("assetName", assetService.getAssetNameForUID(asset));
		model.addAttribute("questionnaireName", questionnaire.getName());
					
		return "param/editPVB";
	}
	

	/**@author dhruv.sood
	 * Method to save the new boost value
	 * @param parameterValueBoostBean
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/save", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'BOOST_EVALUATED_VALUE'})")
	public String save(@ModelAttribute("pvbFormBean") ParameterValueBoostBean parameterValueBoostBean, Model model) {
	
		int qeId = parameterValueBoostBean.getQeId();
		int assetId = parameterValueBoostBean.getAssetId();
		int parameterId = parameterValueBoostBean.getParameterId();
		
		//Validate and Create the PVB object
		QuestionnaireParameterAsset questionnaireParameterAsset = parameterBoostService.validateNewBoostValue(qeId, parameterId, assetId);
				
		try{
			if(Math.abs(Double.valueOf(parameterValueBoostBean.getBoostPercentage())) >= 0  && Math.abs(Double.valueOf(parameterValueBoostBean.getBoostPercentage())) <= 100 && parameterValueBoostBean.getBoostPercentage()!=null){
				questionnaireParameterAsset.setBoostPercentage(new BigDecimal(parameterValueBoostBean.getBoostPercentage()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
			}else{
				model.addAttribute("error", "*Value should be between -100 to 100.");
				return "forward:/parameter/boost/"+qeId+"/edit/"+assetId+"/parameter/"+parameterValueBoostBean.getParameterId();
			}
		}
		catch(NumberFormatException e){
			model.addAttribute("error", "*Only decimal value is allowed.");
			return "forward:/parameter/boost/"+qeId+"/edit/"+assetId+"/parameter/"+parameterValueBoostBean.getParameterId();
		}
							
		questionnaireParameterAssetService.save(questionnaireParameterAsset);
		
		model.addAttribute("success", "Saved");
		return "forward:/parameter/boost/"+qeId+"/edit/"+assetId+"/parameter/"+parameterValueBoostBean.getParameterId();
	}
}

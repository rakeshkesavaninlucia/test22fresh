package org.birlasoft.thirdeye.controller.assettype;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.validators.AssetValidation;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/assettype")
public class AssetTypeController {


	@Autowired
	private AssetTypeService assetTypeService;
	@Autowired
	private AssetValidation assetValidation;
	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'ASSET_TYPE_MANAGEMENT'})")
	public String showAssetType(Model model) {

		List<AssetType> listOfAssetType = assetTypeService.findByAssetTypeAndDeleteStatus(Constants.DELETE_STATUS_FALSE);
		Map<AssetType, Boolean> assetTypeHashMap = new HashMap<>();
		for (AssetType assetType : listOfAssetType) {
			if (assetTypeService.isTemplateExist(assetType.getId())) {
				assetTypeHashMap.put(assetType, true);
			} else
				assetTypeHashMap.put(assetType, false);
		}
		model.addAttribute("assetTypeMap",assetTypeHashMap);
		return "assetType/assetType";

	}
	/**
	 * @param assetTypeId
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN + ".hasPermission({'DELETE_ASSET_TYPE'})")
	public String deleteAssetType(@RequestParam(value = "assetTypeId") Integer assetTypeId){

		assetTypeService.softDeleteAssetType(assetTypeId);
		return "redirect:/assettype/list";
	}

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/assetTypes", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_TYPE_MANAGEMENT'})")
	public String addNewAssetType( Model model) {

		model.addAttribute("addNewAssetTypeForm", new AssetType());

		return "assetTemplate/assetTypeTemplate::addNewAssetType";
	}	
	/**
	 * @param newAssetType
	 * @param result
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/saveAssetType", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'ASSET_TYPE_MANAGEMENT'})")
	public Object saveAssetType(@ModelAttribute("addNewAssetTypeForm") AssetType newAssetType,BindingResult result, HttpServletResponse response){
		AssetType assetType = assetTypeService.createAssetType(newAssetType);
		assetValidation.validateAsseTypeModel(newAssetType, result);
		if(result.hasErrors()){
			return new ModelAndView("assetTemplate/assetTypeTemplate::addNewAssetType");
		}
		assetTypeService.save(assetType);	
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect","assettype/list");
		return JSONView.render(jsonToReturn, response);
	}
}
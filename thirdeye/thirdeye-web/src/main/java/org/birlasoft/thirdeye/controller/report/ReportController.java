package org.birlasoft.thirdeye.controller.report;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.report.ReportConfigBean;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.repositories.UserRepository;
import org.birlasoft.thirdeye.repositories.UserWorkspaceRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ReportService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.UserService;
import org.birlasoft.thirdeye.service.UserWorkspaceService;
import org.birlasoft.thirdeye.service.WorkspaceService;
import org.birlasoft.thirdeye.validators.ReportValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for create {@code Report} , save {@code report} ,
 *  list of all{@code report} , edit {@code report} ,delete {@code report}}.
 * @author sunil1.gupta
 */
@Controller
@RequestMapping("/report")
public class ReportController {
	
	
	private ReportService reportService;
	private CustomUserDetailsService customUserDetailsService; 
	
	@Autowired
	UserService userService;
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	private ReportValidator reportValidator;
	
	@Autowired
	UserWorkspaceService userWorkspaceService;
	
	@Autowired
	UserWorkspaceRepository userWorkspaceRepository;
	
	@Autowired
	WorkspaceService workspaceService;
	
		
	/**
	 * Constructor to initialize services
	 *
	 */
	@Autowired
	public ReportController(ReportService reportService,CustomUserDetailsService customUserDetailsService)
	{
		this.reportService = reportService;
		this.customUserDetailsService = customUserDetailsService;
	}
	
	/**
	 *
	 */
	@RequestMapping( value = "/myReports", method = RequestMethod.GET ) 
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_SAVED_REPORT'})")
	public String showMyReports(Model model) {
		
		List<ReportConfigBean> listOfReports = reportService.findAllMySavedAndAssingedReport(customUserDetailsService.getActiveWorkSpaceForUser(),customUserDetailsService.getCurrentUser());
		model.addAttribute("listOfReports", listOfReports);
        return "report/listReports";
	}
	
	@RequestMapping(value = "/createReport", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'SAVE_EDIT_DELETE_REPORT'})")
	public String createReport (@ModelAttribute("reportForm") ReportConfigBean reportConfigBean , Model model, @RequestParam(value="id",required=false) Integer reportId) {
		
		model.addAttribute("reportForm", reportConfigBean);
		Report report = reportService.findReportById(reportId);
		Boolean buttonStatus = false;
		User currentUser = userService.findUserById(customUserDetailsService.getCurrentUser().getId());
		   Workspace currentWorkspace = workspaceService.findOne(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		   UserWorkspace userWorkspace = userWorkspaceService.findByUserAndWorkspace(currentUser,currentWorkspace);
		 if( report.getReportName().equals(userWorkspace.getSelectedHomePage())){
			buttonStatus = true; 
		}
			
		model.addAttribute("reportCheckBoxStatus", buttonStatus);
		
		return "report/reportModalFragment :: createReport";
	}

	
	@RequestMapping(value = "/saveReport", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'SAVE_EDIT_DELETE_REPORT'})")
	public Object saveReport(@ModelAttribute("reportForm") ReportConfigBean reportConfigBean,BindingResult result, HttpServletResponse response){
		
		reportValidator.validate(reportConfigBean , result);
		if(result.hasErrors()){
			return new ModelAndView("report/reportModalFragment :: createReport");
		}
		reportService.saveOrUpdateReport(reportConfigBean);
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect","true");
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * Edit a single Report row  .
	 * @param model
	 * @param idOfColumn
	 * @param session
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/editRow/{id}", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'SAVE_EDIT_DELETE_REPORT'})")
	public String editReportRow(Model model, @PathVariable(value = "id") Integer idOfColumn) {		
		
		Report report = reportService.findReportById(idOfColumn);		
		model.addAttribute("reportConfigBean", new ReportConfigBean());
		model.addAttribute("oneRow", new ReportConfigBean(report));
		return "report/reportFragment :: editOneReport";
	}

	/**
	 * Save single Report  Row.
	 * @param benchmarkItemScoreBean
	 * @param result
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/columns/saveRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'SAVE_EDIT_DELETE_REPORT'})")
	public String saveReportRow(@ModelAttribute("reportForm") ReportConfigBean reportConfigBean, BindingResult result, Model model) {
		
		return null;
	}
	
	/**
	 * Delete Report
	 * @param reportId	
	 */
	@RequestMapping(value = "/columns/deleteRow", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'SAVE_EDIT_DELETE_REPORT'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteReport(@RequestParam(value="id") Integer reportId) {
		reportService.deleteReport(reportId);
	}
	
	/**
	 * View {@code report} by Id.
	 * @param model
	 * @param idOfReport
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'SAVE_EDIT_DELETE_REPORT'})")
	public String viewGraphByID(Model model, @PathVariable(value = "id") Integer idOfReport) {
		
		Report report = reportService.findReportById(idOfReport);				
		// TODO Check if the user can access this graph.
		//workspaceSecurityService.authorizeReportAccess(report);
		
		//for changing the status of button
		model.addAttribute("report", report);
		return "report/viewSavedReport";
	}
	
	@RequestMapping(value = "/updateReportStatus", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_SAVED_REPORT'})")
	@ResponseBody 
	public void updateReportStatus (Model model, @RequestParam(value = "reporturl",required=false) String reportUrl,@RequestParam(value="functionname",required=false) String functionname,@RequestParam(value="id",required=false) Integer id) {
		User currentUser = userService.findUserById(customUserDetailsService.getCurrentUser().getId());
		   Workspace currentWorkspace = workspaceService.findOne(customUserDetailsService.getActiveWorkSpaceForUser().getId());
		   UserWorkspace userWorkspace = userWorkspaceService.findByUserAndWorkspace(currentUser,currentWorkspace);
			userWorkspace.setHomePageUrl(reportUrl);
			userWorkspace.setSelectedHomePage(functionname);
			userWorkspaceRepository.save(userWorkspace);
		   reportService.activateReportAsHomepage(id);
	}
	
}

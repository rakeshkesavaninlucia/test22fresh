
/*************User role view permission */

REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('ASSET_TYPE_MANAGEMENT',1);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('DELETE_ASSET_TYPE',1);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('DELETE_USER_ROLE',1);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('DELETE_QUESTIONNAIRE',1);
REPLACE INTO `role_permission` (`permissionname`,`roleid`) VALUES ('DELETE_WORKSPACE',1);
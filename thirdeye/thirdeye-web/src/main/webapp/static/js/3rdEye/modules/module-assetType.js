/**
 * @author: ananta.sethi 
 * module for delete asset type
 * 
 */
Box.Application.addModule('module-assetType',function(context) {

	var $,moduleDiv,commonUtils;
	var createUrl = "assettype/assetTypes";
	var ddData = [
	              {    
	            	  value: "static/img/app.svg",
	                  imageSrc: "/thirdeye-web/static/img/app.svg"
	              },
	              {
	                  value: "static/img/businesServices.svg",
	                  imageSrc: "/thirdeye-web/static/img/businesServices.svg"
	              },
	              {
	                  value:  "static/img/database.svg",
	                  imageSrc: "/thirdeye-web/static/img/database.svg"
	              },
	              {
	                  value: "static/img/dataCenter.svg",
	                  imageSrc: "/thirdeye-web/static/img/dataCenter.svg"
	              },
	              {
	                  value: "static/img/desktop.svg",
	                  imageSrc: "/thirdeye-web/static/img/desktop.svg"
	              },
	              {
	                  value: "static/img/ITservices.svg",
	                  imageSrc: "/thirdeye-web/static/img/ITservices.svg"
	              },
	              {
	                  value: "static/img/laptop.svg",
	                  imageSrc: "/thirdeye-web/static/img/laptop.svg"
	              },
	              {
	                  value: "static/img/person.svg",
	                  imageSrc: "/thirdeye-web/static/img/person.svg"
	              },
	              {
	                  value: "static/img/server.svg",
	                  imageSrc: "/thirdeye-web/static/img/server.svg"
	              },
	          ];
	
	function initializeDropDown($newModal){
		//Code Starts

		$('#imagePath',$newModal).ddslick({

		    data:ddData,
		    height:150,
		    width:300,
		    selectText: "Select Image",
		    imagePosition:"left",

		    onSelected: function(selectedData){
		    	 $("#imagePath.dd-selected-value").prop ('name', 'imagePath');
		    	//$newModal.querySelector('[name="imagePath"]').value = path;
		        //callback function: do something with selectedData;
		    }  

		});

		//Code Ends
	}
	
	function addNewAssetTypeModal(){	
    	var url = commonUtils.getAppBaseURL(createUrl);
		commonUtils.getHTMLContent(url).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("addNewAssetTypeModal", content, true);
			bindNewAssetTypeModalRequirements($newModal);
			$newModal.modal({backdrop:false,show:true});
			Box.Application.startAll(moduleDiv);
			initializeDropDown($newModal);
		});
	}
	
	function bindNewAssetTypeModalRequirements ($newModal){
		// Bind the ajax form
		initializeDropDown($newModal);
		$('#addNewAssetTypeForm', $newModal).ajaxForm(function(htmlResponse) {

			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("addNewAssetTypeModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindNewAssetTypeModalRequirements($replacement);
			}
		}); 
	}
	return{
		// Initialize the page
		init:function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');

			var clickHandlerConfiguration = {};
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("assettype/delete");
			$("#assetTypeView").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		},
		onclick: function(event, element, elementType) {
        	if (elementType === 'newAssetType') {
        		addNewAssetTypeModal();
        	}
        }
	
	}
});
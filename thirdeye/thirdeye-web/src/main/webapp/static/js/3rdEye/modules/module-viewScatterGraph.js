/**
 * @author: ananta.sethi
 */
Box.Application.addModule('module-viewScatterGraph', function(context) {
	'use strict';
	// private methods here
	var $, commonUtils, $moduleDiv, title, message,	moduleConfig,filterMapData;
	var spinner = null;

	//Method to handle Spinner image loading
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	 }

	function showScatterGraph(scatterWrapper) {

		var dataToPlot = [];
		$ = context.getGlobal('jQuery');
		$.each(scatterWrapper.series, function(index, oneSeries) {
			var oneSeriesForPlotting = {};
			oneSeriesForPlotting.key = oneSeries.seriesName;
			oneSeriesForPlotting.values = [];
			oneSeriesForPlotting.color = oneSeries.seriesColor;
			$.each(oneSeries.dataPoints, function(index2, oneCoordinate) {
				oneSeriesForPlotting.values.push({
					x : oneCoordinate.x,
					y : oneCoordinate.y,
					size : oneCoordinate.z,
					shape : 'circle',
					label : oneCoordinate.label
				});
			});
			dataToPlot.push(oneSeriesForPlotting);
		});

		if(scatterWrapper.series.length > 0){			
			$(".graphTitle").text(scatterWrapper.graphName);
			title = scatterWrapper.graphName;
			
		}
			if(scatterWrapper.message != null){
				$(".bcmError").text(scatterWrapper.message);
				$(".bcmError").show();
		}else{
			$(".bcmError").hide();
			
		}
			
		nv.addGraph(function() {
			var chart = nv.models.scatterChart().showDistX(false).showDistY(false).color(function(d, i) {
				var colors = dataToPlot.color;
				return colors[i % colors.length - 1];
			});
			//.color(d3.scale.category10().range());

			//Configure how the tooltip looks.
			chart.tooltip.contentGenerator(function(obj) {
				return '<h3>' + obj.point.label + '</h3>';
			});

			chart.tooltip.enabled();
			chart.forceY([ 0, 1 ]);
			chart.forceX([ 0, 1 ]);

			chart.xAxis.tickFormat(d3.format('.02f'));
			chart.yAxis.tickFormat(d3.format('.02f'));

			chart.xAxis.axisLabel(scatterWrapper.xAxisLabel);
			chart.yAxis.axisLabel(scatterWrapper.yAxisLabel);

			d3.select('#chart svg').attr('id', 'graphDown');
			d3.select('#chart svg').datum(dataToPlot).transition().duration(1000).call(chart);

			nv.utils.windowResize(chart.update);

			return chart;
		});
	}
	

	function initPageBindings() {
		var ajaxFormOptions = {
			beforeSubmit : showRequest,
			success : function(scatterWrapper) {
				showScatterGraph(scatterWrapper);
				showLoading($('.scatterPlot'), false);
				if(scatterWrapper.series != null && scatterWrapper.series.length > 0){
					$("#saveAs").show();
				}else{
					$("#saveAs").hide();
				}
			}
		}
		$("#scatterGraphControl").ajaxForm(ajaxFormOptions);
	}
	
	function showRequest(formData, jqForm, options) {
		showLoading($('.scatterPlot'), true);
		var filterURL = commonUtils.readCookie("filterURL");
        
        if(filterURL !== undefined && filterURL !== null){			
			var urlData = filterURL.split('?').pop();
			if(urlData != ""){
				options.url = options.url + "?" + urlData;
				filterMapData = urlData;
			}
		}
	}
	
	function getFilterMap(){
		  return JSON.parse('{"' + decodeURI(filterMapData.replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}')
	}
	
	function getReportConfiguration(){ 
		var configData= {};
		configData.questionnaireIds= $('select[name="questionnaireIds"]').map(function(){ return this.value}).get() +"";
		configData.parameterIds= $('select[name="parameterIds"]').map(function(){ return this.value}).get()+"";
		configData.qualityGateId= $moduleDiv.querySelector('[name="qualityGateId"]').value;
		configData.idsOfLevel1= $('#idsOfLevel1',$moduleDiv).val() != null ? $('#idsOfLevel1',$moduleDiv).val()+"" : null;
		configData.idsOfLevel2= $('#idsOfLevel2',$moduleDiv).val() != null ? $('#idsOfLevel2',$moduleDiv).val()+"" : null;
		configData.idsOfLevel3= $('#idsOfLevel3',$moduleDiv).val() != null ? $('#idsOfLevel3',$moduleDiv).val()+"" : null;
		configData.idsOfLevel4= $('#idsOfLevel4',$moduleDiv).val() != null ? $('#idsOfLevel4',$moduleDiv).val()+"" : null;
		//configData.filterMap = getFilterMap();
		return JSON.stringify(configData);
	}

	return {
		behaviors : [ 'behavior-fetchParameterGraph', 'behavior-select2' ],
		
		messages: ['filterSelected','repo::HealthAnalysis'],

		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv = context.getElement();
			moduleConfig = context.getConfig();
			initPageBindings();
		},
		onclick: function(event, element, elementType) {
			if (elementType === "download"){
				saveSvgAsPng(document.getElementById("graphDown"), "scatterGraph-" + title + ".png")
			}else if(elementType === "saveReport"){
				var dataObj = {};
				dataObj.id = moduleConfig.id;
				dataObj.reportType = moduleConfig.reportType.replace(/[']/g, "");
				dataObj.reportConfig = getReportConfiguration(); 
				context.broadcast('repo::savereport', dataObj);
			}
			
		},
		onmessage: function(name, data) {
			 if(name === 'filterSelected') {
				 if($("#scatterGraphControl", $moduleDiv).find("select[name='parameterIds']").val() != -1 && $("#scatterGraphControl", $moduleDiv).find("select[name='questionnaireIds']").val() != -1){					 
					 $('button[type="submit"]').trigger('click');
				 }
			 }else if(name === 'repo::HealthAnalysis'){
				 showScatterGraph(data);
				 context.broadcast('repo:ViewSuccess',"");
			 }
		 }
	}
});
/**
 * @author: ananta.sethi
 * module for applying select2 on pre publish page grid format
 * 
 */
Box.Application.addModule('module-gridSelect', function(context) {

	// private methods here
	return {

		behaviors : [ 'behavior-select2'],

	}
});

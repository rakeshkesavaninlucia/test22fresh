/**
 * @author: dhruv.sood
 */
Box.Application.addModule('module-assetDataEntry', function(context) {

	'use strict'
	var $;
	// private methods here

	function validateParentChildDropDown(){

		$("#errorParentChild").hide();
		$(".childassetdropdown").change(function(){
			checkParentChildName();
			});
		$(".parentassetdropdown").change(function(){
			checkParentChildName();
			});
	}

	function checkParentChildName(){
		$("#errorParentChild").hide();
		if($(".childassetdropdown :selected").text()!="--Select--" && 
				$(".childassetdropdown :selected").text() ===$(".parentassetdropdown  :selected").text()){
			$("#errorParentChild").css('color','red').show()
			$('#submitButton').prop("disabled","disabled");
			return false;
		}else
			$('#submitButton').prop("disabled",false);
	}

	return {
		behaviors : [ 'behavior-select2'],

		init:function(){
			$ = context.getGlobal('jQuery');
			validateParentChildDropDown();
		}
	};
});
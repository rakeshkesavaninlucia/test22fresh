
/* 
 *  Author- Shagun.sharma
 *  module for user profile validations and display user image
 */
Box.Application.addModule('module-userProfileView', function(context) {

	'use strict';
	var $,notifyService,moduleEl,commonUtils,$moduleDiv,_moduleConfig;
	// private methods here
	function readURL(input){
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#user-img')
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	function showImage(){
		$('.photo').click(function () {
			$("input[type='file']").trigger('click');
		});
		$('input[type="file"]').on('change', function() {
			readURL(this);
		})
	}
	
	function validateFnameLname(){
		$(".textfield").on('keyup', function(e) {
		    var val = $(this).val();
		   if (val.match(/[^a-zA-Z]/g)) {
		       $(this).val(val.replace(/[^a-zA-Z]/g, ''));
		   }
		});
	}

	function showMessage(){
		var dataobj1= {};
		dataobj1.error= _moduleConfig.error;
		if(dataobj1.error === "false"){
			notifyService.setNotification("Saved", "", "success");
		}
	}
	return {
		behaviors : [ 'behavior-select2'],

		init: function (){
			// retrieve a reference to jQuery

			commonUtils = context.getService('commonUtils');
			$ = context.getGlobal('jQuery');
			$moduleDiv = context.getElement();
			notifyService = context.getService('service-notify');
			moduleEl = context.getElement();
		    _moduleConfig = context.getConfig(); 
			showImage();
			validateFnameLname();
			showMessage();

		},
	};
});
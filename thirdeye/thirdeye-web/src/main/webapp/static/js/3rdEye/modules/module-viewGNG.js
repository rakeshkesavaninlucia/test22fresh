/**
 * @author: mehak.guglani
 */
Box.Application.addModule('module-viewGNG', function(context) {
	'use strict';
	var $,commonUtils, $moduleDiv;
	var spinner = null;

	//Method to handle Spinner image loading
	function showLoading(container, show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		spinner = spinService.getSpinner(container,show,spinner);   	
	}

	function fetchAssetTemplateValues(asseturlToHit){
		var deferred = $.Deferred();
		commonUtils.getHTMLContent(asseturlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	}

	function fetchParameterValues(paramUrlToHit){
		var deferred = $.Deferred();
		commonUtils.getHTMLContent(paramUrlToHit).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			deferred.resolve(htmlResponse);
		});
		return deferred.promise();
	} 

	function initPageBindings() {
		var ajaxFormOptions = {
				beforeSubmit : function(){
					showLoading($('.gngReport'), true);
				},
				success: function(gngWrapper){
						$('.gngReportPlot').empty();
						$('.gngReportPlot').append(gngWrapper);
						Box.Application.startAll($('.gngReportPlot'));
					showLoading($('.gngReport'), false);
				}
		}
		$("#gngReportControl", $moduleDiv).ajaxForm(ajaxFormOptions); 
		$(".ajaxSelector", $moduleDiv).change(function(){
			var $selector = $(this);
			// find value 
			var paramUrlToHit = commonUtils.getAppBaseURL($(this).data("parameterurl")+ $selector.val());
			var asseturlToHit = commonUtils.getAppBaseURL($(this).data("asseturl")+ $selector.val());
			var depName = $selector.data("dependency");
			var depSelector = ".ajaxPopulated[data-dependson=" + depName+ "]";
			var $matchingSelectors = $(depSelector);

			$matchingSelectors.each(function(index) {
				var $this = $(this);
				if ($this.is(".assetTemplateSelector")){
					if ($selector.val() > 0){
						fetchAssetTemplateValues(asseturlToHit).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						})
					}
				}

				if ($this.is(".parameterSelector")){
					if ($selector.val() > 0){
						fetchParameterValues(paramUrlToHit).then(function(htmlResponse){
							$this.empty();
							$this.append(htmlResponse)
						})
					}
				}
			});
		});	
	}

	return{

		behaviors : [ 'behavior-select2', 'behavior-downloadD3' ],

		init : function(){
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			$moduleDiv = $(context.getElement());
			initPageBindings();
		}
	}
});




/**
 * This module is to populate the modal for creating, viewing and editing the cost element modal.
 * @author: dhruv.sood
 */
Box.Application.addModule('module-costElement', function(context) {

	 'use strict';

	var $;
	var commonUtils;
	var createUrl = "costElement/create";
	 
	
	//Function to generate modal for cost element
	function createCostElementModal(ceid){		
		var dataobj = {};
		dataobj.ceid = ceid;		
    	var url = commonUtils.getAppBaseURL(createUrl);
    	
		commonUtils.getHTMLContent(url, dataobj).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("createNewCostElementModal", content, true);
			bindNewCostElementModalRequirements($newModal);
			$newModal.modal({backdrop:false,show:true});
		});
	}
	
	function bindNewCostElementModalRequirements ($newModal){
		// Bind the ajax form
		$('#createNewCostElementForm', $newModal).ajaxForm(function(htmlResponse) {

			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("createNewCostElementModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindNewCostElementModalRequirements($replacement);
			}
		}); 
	
	}
	
    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');  
        },
            
        onclick: function(event, element, elementType) {
        	if (elementType === 'CostElement') {           		
        		var ceid = $(element).data("ceid");
        		createCostElementModal(ceid);
        	}
        }	
    };
});
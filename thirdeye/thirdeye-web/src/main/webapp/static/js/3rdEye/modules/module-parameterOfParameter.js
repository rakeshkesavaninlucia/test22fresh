/**
 * 
 */
Box.Application.addModule('module-parameterOfParameter', function(context) {
	'use strict';
	var $,commonUtils,d3,jsonService,$element,_moduleConfig;
	var dataObj = {};

	function fetchDataForAssetParameterHealth(dataObj){
		var deffMassage = $.Deferred();
		var dataPromise = jsonService.getJsonResponse(commonUtils.getAppBaseURL("templates/getAssetBarChartWrapper/"+dataObj.assetId)); 
		dataPromise.done ( function (data) {
			// need to massage it
			deffMassage.resolve (drawBarchart(data)); 
			loadSpiderChart(data[0]);
		})

		.fail (function (error) {
			console.log (error);
			deffMassage.reject(error);
		});

		return deffMassage.promise();
	}
	function drawBarchart(data){
		//sort bars based on value
//		data = data.sort(function (a, b) {
//			return d3.ascending(a.value, b.value);
//		})

		//set up svg using margin conventions - we'll need plenty of room on the left for labels
		var margin = {
			top: 15,
			right: 25,
			bottom: 15,
			left: 100
		};

		var width = $(".chart").width() - margin.left - margin.right,
		height = 200 - margin.top - margin.bottom;

		var svg = d3.select(".chart").append("svg")
		 .style("cursor", "pointer")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var x = d3.scale.linear()
		.range([0, width])
		.domain([0, d3.max(data, function (d) {
			return d.value;
		})]);

		var y = d3.scale.ordinal()
		.rangeRoundBands([height, 0], .1)
		.domain(data.map(function (d) {
			return d.label;
		}));

		//make y axis to show bar names
		var yAxis = d3.svg.axis()
		.scale(y)
		//no tick marks
		.tickSize(0)
		.orient("left");

		var gy = svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)

		
		
		var tip = d3.tip()
	      .attr('class', 'd3-tip')
	      .style("opacity", 0.9)
	      .style("border", "1px solid black")
	      .style("background-color", "white")
	      .offset([-10, 0])
	      .html(function(d) {
	        return '<table><tr><td style="text-align: right;"><strong>Parameter Name :</strong></td><td>&nbsp;' + d.label + '</td></tr><tr><td style="text-align: right;"><b> Value :</b></td><td>&nbsp;' +d.value + '</td></tr></table>';
	      })
		
		
		var bars = svg.selectAll(".bar")
		.data(data)
		.enter()
		.append("g")
		
		
		d3.selectAll('.x .tick')
        .data(data)
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);
		
		
	      svg.call(tip);
	      
		//append rects
		bars.append("rect")
		.attr("class", "bar")
		.attr("data-type","bararea")
		.attr("data-parameterid", function(d){
			return d.parameterId;
		})
		.attr("data-questionnaireid", function(d){
			return d.questionnaireId;
		})
		.attr("data-assetid", function(d){
			return d.assetId;
		})
		.attr("data-questionnairename", function(d){
			return d.questionnaireName;
		})
		.attr("data-label", function(d){
			return d.label;
		})
		.attr("data-value", function(d){
			return d.value;
		})

		.attr("y", function (d) {
			return y(d.label);
		})
		.attr("height", y.rangeBand())
		.attr("x", 0)
		.attr("width", function (d) {
			var xy = x(d.value);
			return x(d.value);
		})
		.on('mouseover', tip.show)
        .on('mouseout', tip.hide);


		//add a value label to the right of each bar
		bars.append("text")
		.attr("class", "label")
		//y position of the label is halfway down the bar
		.attr("y", function (d) {
			return y(d.label) + y.rangeBand() / 2 + 4;
		})
		//x position is 3 pixels to the right of the bar
		.attr("x", function (d) {
			return x(d.value) + 3;
		})
		.text(function (d) {
			return d.value;
		});
	}

	function drawAssetHealthSpiderChart(display){ 
		dataObj.assetId = display.parameter.assetId;
		fetchDataForAssetParameterHealth(dataObj);
	}

	function loadSpiderChart(data){

		var url = commonUtils.getAppBaseURL("templates/showSpider")

		commonUtils.getHTMLContent(url, data).then(function(htmlResponse){
			// Check for the modal indicator and then replace the content.
			var $response = $(htmlResponse);
			$("#spiderChart",$element).empty();
			$("#spiderChart",$element).append($response);
			Box.Application.startAll($element);
		});
	}


	return{
		init: function (){
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			jsonService = context.getService('jsonService');
			d3 = context.getGlobal('d3');
			$element = $(context.getElement());
			_moduleConfig = context.getConfig();  
			drawAssetHealthSpiderChart(_moduleConfig.display);

		},
		onclick: function(event, element, elementType) {
			if (elementType === 'bararea') {
				// Go drilling to the next level        		
				dataObj.parameterId = $(element).data("parameterid");
				dataObj.questionnaireId = $(element).data("questionnaireid");        		
				dataObj.assetId = $(element).data("assetid");
				dataObj.questionnaireName = $(element).data("questionnairename");
				dataObj.label = $(element).data("label");
				dataObj.value = $(element).data("value");

				loadSpiderChart(dataObj)
				//context.broadcast('sc::spiderchartrequest', dataObj);
			}
		}

	}
});
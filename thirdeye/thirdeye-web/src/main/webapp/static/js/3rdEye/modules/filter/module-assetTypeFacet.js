/**
 * This module is for Updating the Filter
 * @author dhruv.sood
 */

Box.Application.addModule('module-assetTypeFacet', function(context) {
	 
	'use strict';
	
	// private methods here
	var $, commonUtils, moduleDiv;	
	
	var dataObj ={};
	var spinner = null;
	
	//Method to handle Spinner image loading
	function showLoading(show){
		var spinService = context.getService('service-spinner');
		//Calling the service
		if (show) {
			spinner = spinService.getSpinner(moduleDiv, show, spinner);   	
		} else {
			spinner.stop();
		}
	 }
	
	//Method to get the asset type facet
	function getAssetTypes(){
		var url = commonUtils.getAppBaseURL("filter/assetTypes");		
		 var getRequest = $.get(url);
	        getRequest.done(function(incomingData){
	        	showLoading(false);	  
	        	$(moduleDiv).empty().append(incomingData).fadeIn(1300);	 
	        	loadFromCookie();
			 })
	}
	
	//Method to apply default filter to page from browser cookie
	function loadFromCookie(){
		var filterURL = commonUtils.readCookie("filterURL");
		
		if(filterURL!=null){
			var urlParams = filterURL.split("?")[1];
			//Fetch the facet data
			context.broadcast('fetchFacetData', filterURL);
			var paramArray = [];
			paramArray = urlParams.split('&');
			var paramObj ={};
			for(var i=0; i<paramArray.length-1;i++){
				paramObj.checked = true;
				paramObj.paramValue = paramArray[i];
				
				//Updating the tags
				context.broadcast('updateTag', paramObj);

				//Check the filter of Asset Class
				if(paramArray[i].split("=")[0]==='assetClass'){
					checkAssetType(paramArray[i]);
				}
			}
		}
	}
	
	function checkAssetType(data){
		var newIDValue = data.split(' ').join('_').replace("=","");		
		$("#"+newIDValue, moduleDiv).attr('checked', true);
	}
	
	return {
		messages: ['uncheckAssetClass'],
		
		init: function (){
			$ = context.getGlobal('jQuery');
			moduleDiv = $(context.getElement());
			commonUtils = context.getService('commonUtils');	
			showLoading(true);	  
			//Get the asset types
			getAssetTypes();
		},  	
		onclick: function(event, element, elementType) {
			if(element!=null){
				dataObj.checked = element.checked;
				dataObj.paramValue = elementType;
				context.broadcast('assetClassClicked', dataObj); 
			}			
        },
		onmessage: function(name, newIDValue) {
			if(name==='uncheckAssetClass'){
				$("#"+newIDValue, moduleDiv).attr('checked', false);
			}
		}
	};
});
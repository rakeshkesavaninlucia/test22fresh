/**
 * Module to export and import the chart of account excel
 * @author: dhruv.sood
 */
Box.Application.addModule('module-exportImportCOA', function(context) {

	var $;
	var commonUtils;
	var pageUrl = "/exportImport/fetchModal";
	var id;
	var dataObj = {};
	
	
	function bindFmExportImportModalRequirements($newModal){
		// Bind the ajax form 
		$('#importFmForm', $newModal).ajaxForm(function(htmlResponse) {
			
			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("exportImportCOAExcelModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindFmExportImportModalRequirements($replacement);
			}
		}); 
	}
	
	function createCOAExportImportModal(id){
		
    	var url = commonUtils.getAppBaseURL("tco/"+id+pageUrl);
    	
		commonUtils.getHTMLContent(url).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("exportImportCOAExcelModal", content, true);
			
			bindFmExportImportModalRequirements($newModal);
			
			$newModal.modal({backdrop:false,show:true});
		});
	}

    return {

        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            
        },
        destroy: function() {
			 dataObj = {};
        },   
    
        onclick: function(event, element, elementType) {
        	if (elementType === 'exportImportCOA') {
        		id = $(element).data("coaid");        		
        		createCOAExportImportModal(id);
        	}
        }	
    };
});
/**
* @author: mehak.guglani
* 
*  Module to generate the percentage for circular progress bar.
*/
Box.Application.addModule('module-circularProgress', function(context) {
	
    // private
       'use strict';
	   var $,$moduleDiv;
		      
		       //To generate the percentage for circular progress bar
		       function showCircularProgressBar(){
		    	   var percentage = $moduleDiv.data("percent").toFixed();
		    	   $moduleDiv.append('<div  class="c100 small circularProgress"><span>'+percentage+' % </span><div class="slice"><div class="bar"></div><div class="fill"></div></div>')
		    	   $('.circularProgress',$moduleDiv).addClass('p'+percentage);
		       }		       		       
		       return {
		              init : function(){
		                     $ = context.getGlobal('jQuery');
		                     $moduleDiv = $(context.getElement());
		                     showCircularProgressBar(); 
		              }
		       };
		});

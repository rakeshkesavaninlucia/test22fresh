<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.tco.view.cost.elements.nav})">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Cost Elements</title>
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.tco.available.cost.element.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.tco.available.cost.element.subtitle}"></span>
	</div>
	
	<div th:fragment="contentContainer">
		<div class="row">
				<div class="col-md-12">
					<div class="box box-primary" data-module="module-costElement">
		                <div class="box-body">
							<div class="table-responsive" data-module="common-data-table">
								<table class="table table-bordered table-condensed table-hover">
									<thead>
										<tr>
											<th><span>Name</span></th>
											<th><span>Updated Date</span></th>
											<th><span>Action</span></th>											
										</tr>
									</thead>
									<tbody th:if="${not #lists.isEmpty(listCostElement)}">
										<tr th:each="oneCostElement : ${listCostElement}" th:id="${oneCostElement.id}">
											<td><span th:text="${oneCostElement.displayName}"></span></td>
											<td><span th:text="${#dates.format(oneCostElement.updatedDate,'dd-MMM-yyyy')}"></span></td>
											<td><a class="fa fa-pencil-square-o clickable" th:title="#{icon.title.edit}" data-type="CostElement" th:attr="data-ceid=${oneCostElement.id}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})"></a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="box-footer clearfix" >
			            	<span class="btn btn-primary"  data-type="CostElement" th:title="#{icon.title.create}" th:text="#{create.cost.elements}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})"></span>							
						</div>
					</div>										
				</div>
			</div>	
	</div>
			
	<div th:fragment="scriptsContainer" th:remove="tag">	
		<script th:src="@{/static/js/3rdEye/modules/module-costElement.js}"></script>
	</div>
</body>
</html>
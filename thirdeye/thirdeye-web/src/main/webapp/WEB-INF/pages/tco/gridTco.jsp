<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle=#{pages.tcomanagement.sresponse.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	 <a class="fa fa-chevron-left btn btn-default" style="margin-left: 10px;" href="/thirdeye-web/tco/response/list"></a>
	<span th:text="#{pages.tco.sresponse.title}"></span> 
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.tco.sresponse.subtitle}"></span>
	</div>
	<div th:fragment="contentContainer">
		<div class="row" th:if="${chartOfAccountId}">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Submit Cost of Ownership
				<div th:replace="tco/tcoFragment :: exportImportCOAExcel"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="box box-primary userquestionnaire" data-module='module-questionnaireResponse'>
					<div class="box-header">
						<h3 class="box-title" th:text="${questionnaire.name}">This is the title of the questionnaire</h3>
						<small th:text="${questionnaire.description}"></small>
					</div>
					<div class="box-body">
					<form  method="POST"  th:action="@{/tco/response/saveChartOfAccount}" th:object ="${chartOfAccountGridWraper}" id = "gridTco">
						<div>
						<table class="table table-bordered table-striped table-condensed table-hover dataTable"	id="coaList">
							<thead>
								<tr>	
									<th><span th:text="#{tco.costelement} +'/'+ #{tco.asset}"></span></th>														
									<th th:each="coa : ${AssetName}"><span th:text="${coa}"></span></th>
								</tr>
							</thead>
							<tbody>
								<tr th:each="cs,loop : *{costResponse}">								
									<td style="white-space: nowrap;"><span th:text="${cs.costElement}" th:field="${cs.costElement}"></span></td>																			
									<td th:each ="coa1, stat : *{costResponse[__${loop.index}__].costResponses}">
									  <input type="number" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{costResponse[__${loop.index}__].costResponses[__${stat.index}__].response}" class="form-control editableParameter numeric" placeholder="Enter your amount Here" />																	
									  <input type="hidden"  th:id="'qqid_'+${coa1.questionnaireQuestionId}" th:value="${coa1.questionnaireQuestionId}" th:field="*{costResponse[__${loop.index}__].costResponses[__${stat.index}__].questionnaireQuestionId}"/></td>
								</tr>
							</tbody>
						</table>
						</div>
						<div class="box-footer" style="float:right;">
								<div th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).REVIEWED.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).EDITABLE.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).CLOSED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq false and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq true and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  type="submit" value="SAVE" class="btn btn-primary" data-type = "saveButton" />
							</div>
						</div>				
						</form>	
					</div>	
				</div>
			</div>
		</div>
	</div>
		<div th:fragment="prePublish">
		<div class="row" th:if="${chartOfAccountId}">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Submit Cost of Ownership
				<div th:replace="tco/tcoFragment :: exportImportCOAExcel"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="box box-primary userquestionnaire" data-module='module-questionnaireResponse'>
					<div class="box-header">
						<h3 class="box-title" th:text="${questionnaire.name}">This is the title of the questionnaire</h3>
						<small th:text="${questionnaire.description}"></small>
					</div>
					<div class="box-body">
					<form  method="POST"  th:action="@{/tco/response/saveChartOfAccount}" th:object ="${chartOfAccountGridWraper}" id = "gridTco">
						<div>
						<table class="table table-bordered table-striped table-condensed table-hover dataTable"	id="coaList">
							<thead>
								<tr>	
									<th><span th:text="#{tco.costelement} +'/'+ #{tco.asset}"></span></th>														
									<th th:each="coa : ${AssetName}"><span th:text="${coa}"></span></th>
								</tr>
							</thead>
							<tbody>
								<tr th:each="cs,loop : *{costResponse}">								
									<td style="white-space: nowrap;"><span th:text="${cs.costElement}" th:field="${cs.costElement}"></span></td>																			
									<td th:each ="coa1, stat : *{costResponse[__${loop.index}__].costResponses}">
									  <input type="number" style=" pointer-events: none;" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{costResponse[__${loop.index}__].costResponses[__${stat.index}__].response}" class="form-control editableParameter numeric greyOut" placeholder="Enter your amount Here" />																	
									  <input type="hidden"  th:id="'qqid_'+${coa1.questionnaireQuestionId}" th:value="${coa1.questionnaireQuestionId}" th:field="*{costResponse[__${loop.index}__].costResponses[__${stat.index}__].questionnaireQuestionId}"/></td>
								</tr>
							</tbody>
						</table>
						</div>
						<div class="box-footer" style="float:right;">
								<div th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).REVIEWED.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).EDITABLE.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).CLOSED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq false and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq true and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  type="submit" value="SAVE" class="btn btn-primary" data-type = "saveButton" />
							</div>
						</div>				
						</form>	
					</div>	
				</div>
			</div>
		</div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script	th:src="@{/static/js/3rdEye/modules/module-questionnaireResponse.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-exportImportCOA.js}"></script>
	</div>
</body>
</html>
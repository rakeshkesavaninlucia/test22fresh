<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.portfolio.nav})">
<head>
<meta charset="utf-8" />
<title>d3.js from Excel - ramblings.mcpher.com</title>
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
  Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<body>
<div th:fragment="pageTitle">
	<div>
		<span th:text="#{pages.reports.portfolio.title}"></span><span data-toggle="control-sidebar" style="float:right;"><i class="glyphicon glyphicon-filter clickable btn btn-primary"></i></span>
	</div>
</div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.reports.portfolio.subtitle}"></span></div>
	<div th:fragment="contentContainer">
		<div class="row" style="padding-bottom: 20px; padding-left: 20px;">
			<div style="padding-top: 15px;" data-module="module-filterTags">
				<div class="col-md-12">
					<div id="tags"></div>
				</div>
			</div>
		</div>

		<aside class="control-sidebar control-sidebar-dark" style="margin-top: 101px; padding-top: 0px;">
			<div class="tab-content" style="height: 39em; overflow-y: auto;">
				<div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
					<div style="text-align: center; text-decoration: underline;">
						<h4 th:text="#{filter.panel}"></h4>
					</div>
					<div data-module="module-assetTypeFacet"></div>
					<div data-module="module-facetUpdate"></div>
				</div>
			</div>
		</aside>
	
		<div data-module="module-viewPhp">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
							<div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						<div class="box-body">
							<form role="form" th:action="@{/portfoliohealth/plot}" method="post" id="phpHeatMapControl">
							<div class="row" >
								<div class="col-md-6 form-group">
									<label>Select the questionnaire</label> 
									<select class="form-control ajaxSelector select2" th:attr="data-dependency=qe,data-parameterurl='portfoliohealth/fetchRootParameters/',data-asseturl='portfoliohealth/fetchAssetTemplates/'" name="questionnaireId">
										<option value="-1">--Select--</option>
										<option th:each="oneQuestionnaire : ${listOfQuestionnaire}"
											th:value="${oneQuestionnaire.id}"
											th:text="${oneQuestionnaire.name}" />
									</select>
								</div>
							</div>
								<div class="row">
									<div class="col-md-6 form-group">
										<label>Select Parameters</label>
										<select class="form-control ajaxPopulated parameterSelector select2" th:attr="data-dependson=qe" id="parameterIds" name="parameterIds" multiple="multiple">
											<option value="-1">--Select--</option>
										</select>
									</div>
	
									<div class="col-md-6 form-group">
										<label>Select Asset Template</label>
										<select class="form-control ajaxPopulated assetTemplateSelector select2" th:attr="data-dependson=qe" id="assetTemplateIds" name="assetTemplateIds" multiple="multiple">
											<option value="-1">--Select--</option>
										</select>
									</div>
								</div>
								<div class="box-footer">
									<div class="pull-right">
										<button type="submit" class="btn btn-primary">Plot</button>
										<div id="saveAs" class="pull-right" style="display:none;margin-left: 5px;"><a sec:authorize="@securityService.hasPermission({'SAVE_EDIT_DELETE_REPORT'})"  class="btn btn-primary" data-type = "saveReport" th:text="#{report.modal.title}"></a></div>
						           </div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary phpHeatMap">
						<div class="box-header with-border">
							<h3 class="box-title graphTitle"></h3>
							<div class="box-tools pull-right">
					            <a id="link" href="" class="fa fa-file-excel-o clickable excelPhp"></a>
						   	 </div>	
						</div>
						<canvas name = "canvas" style="display:none"></canvas>
						<div class="box-body chart-responsive">
							<div id="chart" class='with-3d-shadow with-transitions'	style="overflow: auto;"></div>
						<div id="dataset-picker"></div>
						</div>
						<script type="text/x-config" th:inline="javascript">{"pngimagename":"PortfolioHealth", "id": "","reportType":"[[${T(org.birlasoft.thirdeye.constant.ReportType).PORTFOLIO_HEALTH.description}]]"}</script>
					</div>
				</div>
			</div>
		</div>
		<div data-module="module-report">
		  <script type="text/x-config" th:inline="javascript">{"url":"report/createReport", "modal": "saveReportModal"}</script>
		</div>
	</div>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/ext/colorbrewer/colorbrewer.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewPhp.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/report/module-report.js}"></script>
		
	</div>

	<select th:fragment="optionListOfParameters(listOfParameters)"
		th:remove="tag">
		<option th:each="oneParameter : ${listOfParameters}"
			th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
	</select>
	<select th:fragment="optionListOfAssetTemplates(listOfAssetTemplates)"
		th:remove="tag">
		<option th:each="oneAssetTemplate : ${listOfAssetTemplates}"
			th:value="${oneAssetTemplate.id}"
			th:text="${oneAssetTemplate.assetTemplateName}" />
	</select>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>Fragments for BCM</title>
</head>
<body>
	<div th:fragment="createRelationshipType">
		<div class="modal fade" id="createRelationshipTypeModal" tabindex="-1" role="dialog" aria-labelledby="createRelationshipTypeModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/relationship/save}" method="post" id="createRelationshipTypeForm" th:object="${relationshipTypeForm}">
							  <input type="hidden" th:field="*{id}" />
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" th:text="#{relationship.type.modal.title}"></h4>
						      </div>
						      <div class="modal-body">
						      	  <div class="form-group">
						      	  	  <label for="workspace" class="control-label" th:text="#{label.workspace}"></label>
						      	  	  <span class="asterisk">*</span>
						      	  	  <div th:if="${relationshipTypeForm.id eq null}">
								      	  <select id="workspaceId" name="workspace" class="form-control">
												<option th:selected="${relationshipTypeForm.id eq null or (relationshipTypeForm.workspace ne null and (relationshipTypeForm.workspace.id eq activeWorkspace.id))}" th:value="${activeWorkspace.id}" th:text="${activeWorkspace.workspaceName}" />
												<option th:selected="${relationshipTypeForm.id gt 0 and relationshipTypeForm.workspace eq null}" value="" th:text="#{option.systemlevel}"></option>
										  </select>
									  </div>
									  <div th:if="${relationshipTypeForm.id ne null}">
										<span th:if="${relationshipTypeForm.workspace eq null}" th:text="#{option.systemlevel}"></span>
										<span th:if="${relationshipTypeForm.workspace ne null}" th:text="${activeWorkspace.workspaceName}"></span>
										<input th:if="${relationshipTypeForm.workspace ne null}" type="hidden" th:field="*{workspace}" />
									  </div>
								  </div>
						      	  <div class="form-group">
						            <label for="parentDescriptor" class="control-label" th:text="#{relationship.parent.descriptor}"></label>
						            <span class="asterisk">*</span>
						            <input type="text" class="form-control" th:field="*{parentDescriptor}" />
			            			<span th:if="${#fields.hasErrors('parentDescriptor')}" th:errors="*{parentDescriptor}" class="error_msg">Test</span>
						          </div>
						          <div class="form-group">
						            <label for="childDescriptor" class="control-label" th:text="#{relationship.child.descriptor}"></label>
						            <span class="asterisk">*</span>
						            <input type="text" class="form-control" th:field="*{childDescriptor}" />
			            			<span th:if="${#fields.hasErrors('childDescriptor')}" th:errors="*{childDescriptor}" class="error_msg">Test</span>
						          </div>
						          <div class="form-group">
						              <label for="direction" class="control-label" th:text="#{relationship.type.direction}"></label>
							          <select class="form-control select2" th:field="*{direction}">
								          	<option th:each="elem: ${T(org.birlasoft.thirdeye.constant.InterfaceDirection).values()}"  th:value="${elem.name()}" th:text="${elem.description}"/>
								      </select>
							      </div>
						      </div>
						      <div class="modal-footer">
						        <input type="submit" th:value="*{id != null }? 'Edit Relationship Type' : 'Create Relationship Type'" class="btn btn-primary"></input>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
</body>
</html>
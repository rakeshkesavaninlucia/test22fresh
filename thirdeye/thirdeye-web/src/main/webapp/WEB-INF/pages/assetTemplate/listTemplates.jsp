<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.listtemplate.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.templatemanagement.viewtemplate.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.templatemanagement.viewtemplate.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
	 <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                 <div class="box-body" data-module="module-exportInventory">
	     <div class="table-responsive" data-module="common-data-table">
				<div class="overlay">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
			<table class="table table-bordered table-condensed table-hover" id="templateColumnsofview">
				<thead>
					<tr>
						<th><span th:text="#{asset.name}"></span></th>
						<th><span th:text="#{asset.type}"></span></th>
						<th><span th:text="#{asset.desc}"></span></th>
						<th><span th:text="#{asset.date}"></span></th>
						<th><span th:text="#{asset.action}"></span></th>
					</tr>
				</thead>
				<tbody>
					<tr th:each="oneTemplate : ${assettemplateHashMap}" th:id="${oneTemplate.key.id}">
					<td><span th:if="${oneTemplate.key.hasTemplateColumn eq false}" th:text="${oneTemplate.key.assetTemplateName}">Test</span>
						<a th:if="${oneTemplate.key.hasTemplateColumn eq true}" th:href="@{/templates/view/{id}(id=${oneTemplate.key.id})}">
							<span th:text="${oneTemplate.key.assetTemplateName}">Test</span>
						</a>
					</td>
					<td><span th:text="${oneTemplate.key.assetType.assetTypeName}"></span></td>
					<td><span th:text="${oneTemplate.key.description}">Test</span></td>
					<td><span th:text="${#dates.format(oneTemplate.key.updatedDate, 'dd-MMM-yyyy')}">Test</span></td>
					<td><a th:href="@{/templates/{id}/edit(id=${oneTemplate.key.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}"></a>&nbsp;<a class="fa fa-download exportImportAsset" data-type="exportImportAsset" th:attr="data-templateid=${oneTemplate.key.id}" th:title="#{icon.title.export/import}"></a>&nbsp;<a th:if="${oneTemplate.value eq false}" data-type="deleteTemplate" th:attr="data-templateid=${oneTemplate.key.id}" class="fa fa-trash-o"></a></td>
					</tr>				
				</tbody>
			</table>
		</div>
		</div>
		<div class="box-footer clearfix">
            <div>
	          	<a class="btn btn-primary" th:href="@{/templates/create}">Create Template</a>
	        </div>
	    </div>
		</div>
		</div>
		</div>
	</div>
	 <div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-exportInventory.js}"></script>
    </div>
	</body>
	</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>New Role</title>
</head>
<body>
	<div th:fragment="addNewAssetType">
		<div class="modal fade" id="addNewAssetTypeModal" role="dialog" aria-labelledby="addNewAssetTypeModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/assettype/saveAssetType}" method="post" th:object="${addNewAssetTypeForm}" id="addNewAssetTypeForm">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" th:text="#{pages.templatemanagement.AssetType.title}"></h4>
						      </div>
						      <div class="modal-body">						          
						          <div class="form-group">
						          	<label for="title" class="control-label">Asset Type:</label><span class="asterisk">*</span>
						           <input type="text" class="form-control"  th:field="*{assetTypeName}" />	
						           <span th:if="${#fields.hasErrors('assetTypeName')}" th:errors="*{assetTypeName}" style="color: red;"></span>
						          </div>
						          <div class="form-group" id="imagePath">
						      	</div>
						      	 <span th:if="${#fields.hasErrors('imagePath')}" th:errors="*{imagePath}" style="color: red;"></span>
						      </div>
						      <div class="modal-footer">
						     	  <input type="submit" th:value = "ADD" class="btn btn-primary"></input>
						     </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
</body>
</html>
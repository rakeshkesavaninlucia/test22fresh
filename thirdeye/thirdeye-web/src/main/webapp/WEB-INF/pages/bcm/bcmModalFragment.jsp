<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="bcmLevelModal">
		<div class="modal-dialog" role="document">
			    <div class="modal-content">
			<form th:action="@{/bcmlevel/{bcmId}/level/save(bcmId=${bcmId})}" method="post" th:object="${bcmLevelForm}" id="bcmLevelFormId">
					<input type="hidden" th:field="*{id}"/>
					<input type="hidden" th:field="*{levelNumber}"/>
					<input type="hidden" name="parentLevel" th:value="${parentLevel}"/>
					<input type="hidden" name="operation" th:value="${operation}"/>
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="exampleModalLabel" th:switch="${bcmLevelForm.id}">
							<span th:case="null" th:text="${bcmLevelForm.addIconTitle}"></span>
							<span th:case="*" th:text="${bcmLevelForm.titleOnEdit}"></span>
			        </h4>
			      </div>
			      <div class="modal-body">
			          <div class="form-group">
			            <label for="level-name" class="control-label">Level Name:</label><span class="asterisk">*</span>
			            <input type="text" class="form-control" id="level-name" th:field="*{bcmLevelName}" />
			            <span th:if="${#fields.hasErrors('bcmLevelName')}" th:errors="*{bcmLevelName}" style="color: red;">Test</span>
			          </div>
			          <div th:if="${(operation eq 'add' and bcmLevelForm.superNodeBcmLevel eq null) or (operation eq 'edit' and bcmLevelForm.levelNumber eq 2)}" class="form-group">
			            <label for="category-name" class="control-label">Category:</label>
			            <input type="text" class="form-control" id="category-name" th:field="*{category}" />
			            <span th:if="${#fields.hasErrors('category')}" th:errors="*{category}" style="color: red;">Test</span>
			          </div>
			          <div class="form-group">
			    	  <label for="label-description" class="control-label">Description:</label>						            
					  <input type="text" class="form-control" id ="label-description" th:field="*{description}" th:placeholder="#{bcm.description.placeholder}"/>
					</div>	
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        <input type="submit" value="Create Level" class="btn btn-primary"></input>
			      </div>
			    </form>
			    </div>
		 </div>
	</div>
	
</body>
</html>
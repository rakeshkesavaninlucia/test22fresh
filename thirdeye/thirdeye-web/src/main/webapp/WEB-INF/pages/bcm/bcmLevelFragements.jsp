<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>

	<ul class="treeview-menu" th:fragment="oneLevelWithWrapper(listOfLevels)" th:if="${not #lists.isEmpty(listOfLevels)}">
		<li th:fragment="oneLevelAdded(listOfLevels)" th:each="oneLevel : ${listOfLevels}" th:class="${oneLevel.levelNumber}">			
			<a th:fragment="oneAnchor(oneLevel)" th:id="${oneLevel.id}" th:attr="data-parentlevel=${oneLevel.parent != null ? oneLevel.parent.id : ''}" th:class="${oneLevel.isParent()} == true ? 'level isParent' : 'level'"> 
				<span th:text="${oneLevel.bcmLevelName}" data-toggle="popover" th:attr="data-content=${oneLevel.description}" data-trigger="hover" data-container="body" data-placement="top">Option in tree</span> 
				<span th:if="${oneLevel.category ne null and not oneLevel.category.equals('')}" th:text="'/' + ${oneLevel.category}">Option in tree</span>
			</a>
			<!-- Process children -->
			<div th:replace="bcm/bcmLevelFragements :: oneLevelWithWrapper(listOfLevels=${oneLevel.childLevels})" th:if="${not #lists.isEmpty(oneLevel.childLevels)}"></div>
		</li>
	</ul>
	
</body>
</html>
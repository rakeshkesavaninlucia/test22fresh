<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = ${pageTitle} )">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title ></title>
</head>
<body>
	 <div th:fragment="pageTitle"><span th:text="${pageTitle}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="${pageSubTitle}" ></span></div>
	<div th:fragment="contentContainer">
	  <div class="bcm" data-module="module-bcmTemplate" >
	          	<div class="row">
				<div class="col-md-12">		
				    <div class="box-body"> 
	                  <span class="pull-right">
	                    <a th:if ="${status eq false}" class="btn btn-primary" th:attr="data-bcmurl=${'/bcm/view'}" data-type="sendBCMURLtoUser">Save as home page</a>
				        <a th:if ="${status eq true}" class="btn btn-primary"  style="visibility:hidden;" th:attr="data-bcmurl=${'/bcm/view'}" data-type="sendBCMURLtoUser" > Save as home page </a>
	                  </span>
	                </div>
				    <div class="box box-primary bcmcontainer" >
						 <table class="table table-bordered table-condensed table-hover">					
								<tr>
									<td class="col-md-1 horizontal"><label for="parameterId" class="control-label" th:text="#{bcm.template.parameter}"></label></td>
									<td colspan="3">
									<select class="select2" data-type="parameterOnBcmTempalate">
											<option value="">--Select Parameter--</option>
											<option th:each="oneParameter : ${listOfParams}" th:value="${oneParameter.fcParameterId}" th:text="${oneParameter.fcParameterName} + '/' + ${oneParameter.questionnaireName}" th:attr="data-qeid=${oneParameter.questionnaireId}" />
									</select></td>
								</tr>
						 </table>
					</div>
					<div class="box box-primary bcmcontainer" th:if="${#lists.isEmpty(listOfBcms)}">
						<div class="box-header with-border">
							<h3 class="box-title">No BCM available</h3>
						</div>
					</div>
					<div class="box box-primary bcmcontainer" th:fragment="bcmcontainer(listOfBcms)" th:each="oneBcm : ${listOfBcms}">
						<div class="box-header with-border">
							<h3 class="box-title" th:text="${oneBcm.bcmName}">Parameter 
								title</h3>
						</div>
						<canvas name = "canvas" style="display:none"></canvas>
						<div class="box-body bcmplacement" th:id="'bcm-'+${oneBcm.id}"
							th:attr="data-bcmid=${oneBcm.id}" style="overflow: auto;">
						</div>
						<script type="text/x-config" th:inline="javascript">{"pngimagename":"[[${oneBcm.bcmName}]]","frurl":"functionalRed/view","asseturl":"bcm/viewBCMLevelAsset","mode":[[${mode}]]}</script>
						
						</div>					
				</div>
			</div>
		
		</div> 
		
	</div>
	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-bcmTemplate.js}"></script>
	</div>
</body>
</html>
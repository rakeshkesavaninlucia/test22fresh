<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.usermanagement.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	<a class="pull-left fa fa-chevron-left btn btn-default" href="/thirdeye-web/user/list" style="margin-left: 10px;"></a>
	<div th:switch="${UserForm.id}">
			<span th:case="null" th:text="#{pages.usermanegement.createuser.title}"></span>
			<span th:case="*" th:text="#{pages.usermanagement.updateuser.title}"></span>
		</div>
	</div>
	<div class="container"  th:fragment="contentContainer">
	<div  data-module="module-createUser">
		<div th:fragment="createUserForm">
			<form th:action="@{/user/save}" method="post" th:object="${UserForm}">
						<input type="hidden" th:field="*{id}" />
						<div class="form-group">
						<span>First Name :<span style="color: red;">*</span></span> <input type="text" th:field="*{firstName}" class="form-control" />
						<span th:if="${#fields.hasErrors('firstName')}" th:errors="*{firstName}" style="color: red;">Test</span> 
						<br/>
						<span>Last Name :<span style="color: red;">*</span></span> <input type="text" th:field="*{lastName}" class="form-control" />
						<span th:if="${#fields.hasErrors('lastName')}" th:errors="*{lastName}" style="color: red;">Test</span> 
						<br/>
						<span>Email :<span style="color: red;">*</span></span> 
						 <input  th:if ="${UserForm.getId() ne null}"  readonly="readonly"  type="email" th:field="*{emailAddress}"  class="form-control" />
						 <input  th:if ="${UserForm.getId() eq null}"   type="email" th:field="*{emailAddress}"  class="form-control" />
						<span th:if="${#fields.hasErrors('emailAddress')}" th:errors="*{emailAddress}" style="color: red;">Test</span> 
						<br/>
						<span>Password :<span style="color: red;">*</span></span> 
						<input th:if ="${UserForm.getId() ne null}" readonly= "readonly" type="password" th:field="*{password}" class="form-control" />
						<input th:if ="${UserForm.getId() eq  null}"  type="password" th:field="*{password}" class="form-control" />
						<span th:if="${#fields.hasErrors('password')}" th:errors="*{password}" style="color: red;">Test</span> 
						<br/>
						<span>Role Assign :</span><span style="color: red;">*</span>
						<div th:each="oneRole :${roles}">
		   				<input type="radio" th:value="${oneRole.id}" name="role" th:text="${oneRole.roleName}" th:checked="${#maps.containsKey(mapForCheckedRoleIds, oneRole.id)}" required="required"/>
						</div>
						<br/>
					    <span>Select Workspace :</span><span style="color: red;">*</span>
						<select class="form-control select2" name="workspaceId" required ="required" >
		   				<option id="one" value="">--Select--</option>
		   				<option th:each="workspace : ${listOfWorkspaces}" th:value="${workspace.id}" th:text="${workspace.workspaceName}"></option>
		   				</select>
		   				<br/><br/>
						<div><input th:if ="${UserForm.getId() eq null}" class="btn btn-primary" type="submit" value="Create"/>
						<input th:if ="${UserForm.getId() ne null}" class="btn btn-primary" type="submit" value="Save"/>
						</div>
							
						</div>
			</form>
	</div>		
	</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
        <script th:src="@{/static/js/3rdEye/modules/module-createUser.js}"></script>
    </div>
</body>
</html>
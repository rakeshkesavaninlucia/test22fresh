<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.usermanagement.userroles.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<span th:text="#{pages.usermanagement.userroles.title}"></span>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.usermanagement.userroles.subtitle}"></span>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-primary" data-module="module-addRole">
					<div class="box-body">
					<span th:text="${errorMessage}" style="color:red;"></span>
						<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover"
								id="userManagementId">
								<thead>
									<tr>
										<th><span>Role Name</span></th>
										<th><span>Role Description</span></th>
										<th><span>Actions</span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneRole : ${roleHashMap}">
										<td><span th:text="${oneRole.key.roleName}">Test</span></td>
										<td><span th:text="${oneRole.key.roleDescription}">Test</span></td>
										<td><a th:href="@{/user/roles/{id}/edit(id=${oneRole.key.id})}"	class="fa fa-pencil-square-o" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'USER_ROLE_MODIFY'})"></a>
											<input type="hidden" th:value = "${oneRole.key.id}" name= "userRoleId" />
											<a  th:if ="${oneRole.value eq false }" class="deleteRow  fa fa-trash-o clickable"	th:title="#{icon.title.delete}"	sec:authorize="@securityService.hasPermission({'DELETE_USER_ROLE'})"></a>
											<a  th:if ="${oneRole.value eq true }" class="fa fa-trash-o" th:title="#{icon.title.delete.userrole}" sec:authorize="@securityService.hasPermission({'DELETE_USER_ROLE'})"></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix">
						<div>
							<a class="btn btn-primary" data-type="newRole" sec:authorize="@securityService.hasPermission({'USER_ROLE_MODIFY'})">Create Role</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/3rdEye/modules/module-addRole.js}"></script>
	</div>
</body>
</html>
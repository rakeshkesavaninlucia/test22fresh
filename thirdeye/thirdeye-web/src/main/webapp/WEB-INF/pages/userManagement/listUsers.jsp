<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.usermanagement.viewuser.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.usermanagement.viewuser.title}"></span></div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.usermanagement.viewuser.subtitle}"></span>
	</div> 
	<div class="container" th:fragment="contentContainer">
		       <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body" data-module="module-editUser" >					
							<div class="table-responsive" data-module="common-data-table">
									<div class="overlay">
										<i class="fa fa-refresh fa-spin"></i>
									</div>
									<table class="table table-bordered table-condensed table-hover" id="userManagementId">
											<thead>
												<tr>
													<th><span th:text="#{user.first.name}"></span></th>
													<th><span th:text="#{user.last.name}"></span></th>
													<th><span th:text="#{user.email}"></span></th>
													<th><span th:text="#{question.action}"></span></th>
												</tr>
											</thead>
											<tbody >
											<tr th:each="listOfUsers : ${listOfUsers}">
												    <input type="hidden" name="userId" th:value="${listOfUsers.id}" />
													<td><span th:text="${listOfUsers.firstName}">Test</span></td>
													<td><span th:text="${listOfUsers.lastName}">Test</span></td>
													<td><span th:text="${listOfUsers.emailAddress}">Test</span></td>
													<td><a th:href="@{/user/{id}/edit(id=${listOfUsers.id})}" class="fa fa-pencil-square-o" th:title="#{icon.title.edit}"></a>
								        			<a class="deleteRow  fa fa-trash-o" th:title="#{icon.title.delete}"></a></td>
											    </tr>
											</tbody>
									</table>
							</div>
			          </div>
			          <div class="box-footer clearfix">
	                  <div>
				          <a class="btn btn-primary" th:href="@{/user/create}">Create User</a>
				        </div>
	                </div>
			          </div>
			          </div>
			          </div>
</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-editUser.js}"></script>
   </div>
</body>
	</html>
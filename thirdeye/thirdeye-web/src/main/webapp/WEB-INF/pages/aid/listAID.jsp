<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.viewaidlist.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.reports.viewaid.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.reports.viewaid.subtitle}"></span></div>

	<div class="container" th:fragment="contentContainer">
	  <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body">
	     <div class="table-responsive" data-module="common-data-table">
				<div class="overlay">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
			<table class="table table-bordered table-condensed table-hover" id="listOfAid">
				<thead>
					<tr>
						<th><span th:text="#{aid.name}"></span></th>
						<th><span th:text="#{aid.desc}"></span></th>
						<th><span th:text="#{aid.update.date}"></span></th>
						<th><span th:text="#{aid.action}"></span></th>
					</tr>
				</thead>
				<tbody>
					<tr  th:each="oneAid : ${listOfAids}" th:id="${oneAid.id}">
					<td><span th:text="${oneAid.name}">Test</span></td>
					<td><span th:text="${oneAid.description}">Test</span></td>
					<td><span th:text="${#dates.format(oneAid.updatedDate,'dd-MMM-yyyy')}">Test</span></td>
					<td><a th:href="@{/aid/{id}/view(id=${oneAid.id})}" class="fa fa-search" th:title="#{icon.title.view.aid}"></a>
					<a th:href="@{/aid/configure/{id}(id=${oneAid.id})}" class="fa fa-cog fa-fw" th:title="#{icon.title.configure}" sec:authorize="@securityService.hasPermission({'AID_MODIFY'})"></a></td>
					</tr>					
				</tbody>
			</table>
		</div>
		</div>
		<div class="box-footer clearfix">
	                  <div>
				          <a class="btn btn-primary" th:href="@{/aid/create}" sec:authorize="@securityService.hasPermission({'AID_MODIFY'})">Create AID</a>
				      </div>
		</div>
		</div>
		</div>
		</div>
	</div>

</body>
</html>
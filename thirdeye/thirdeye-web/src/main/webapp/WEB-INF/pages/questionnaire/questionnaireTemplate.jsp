<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle =${pageTitle})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	           <div th:switch="${questionnaireForm.id}">
                <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" class="pull-left fa fa-chevron-left btn btn-default" href="/thirdeye-web/tco/list" style="margin-left: 10px;"></a>
                <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" class="pull-left fa fa-chevron-left btn btn-default" href="/thirdeye-web/questionnaire/list" style="margin-left: 10px;"></a>
	            
	             <div th:case="null" >
	                 <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.questionnaire.createquestionnaire.title}"></span>
	                 <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.tco.createtco.title}"></span>
	             </div>
	             <div th:case="*">	
	                  <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.questionnaire.updatecreatequestionnaire.title}"></span>
	                  <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{pages.tco.updatetco.title}"></span>
	             </div>
		   </div>	             
	</div>
	<div class="container"  th:fragment="contentContainer">
			<div th:replace="questionnaire/questionnaireTemplateFragement :: createQuestionnaireForm"></div>		
	</div>
	<div th:fragment="scriptsContainer">
     	<script th:src="@{/static/js/3rdEye/modules/module-createQuestionnaireselect.js}"></script>
	</div>
</body>
</html>
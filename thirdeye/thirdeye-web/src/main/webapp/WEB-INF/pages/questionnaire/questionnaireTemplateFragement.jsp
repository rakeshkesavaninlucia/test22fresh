<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="createQuestionnaireForm">
	<div class="row">
			 <div class="col-md-12">
				 <div class="box box-primary">
						<form th:action="@{/{action}/save(action=${action})}" method="post" th:object="${questionnaireForm}">
								<input type="hidden" th:field="*{id}" />
				 				<div class="box-body">
									<div class="form-group" data-module="module-createQuestionnaireselect">
										<span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.name}"></span>
										<span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.name}"></span>
                                        <span class="asterisk">*</span>
										<input type="text" th:field="*{name}" class="form-control" />
										<span th:if="${#fields.hasErrors('name')}" th:errors="*{name}" style="color: red;">Test</span> 
										<br/>
										<span th:text="#{questionnaire.description}"></span>
										<textarea rows="3" class="form-control" th:field="*{description}"></textarea>
										<br/>
										<span th:text="#{questionnaire.select.asset.type}"></span>
										<span class="asterisk">*</span>
										<select th:field="*{assetType.id}" class="form-control select2">
											<option value="-1">--Select--</option>
											<option th:each="oneAssetType : ${assetTypes}" th:value="${oneAssetType.id}" th:text="${oneAssetType.assetTypeName}" />
										</select>
										<br/>
										<span th:if="${#fields.hasErrors('assetType.id')}" th:errors="*{assetType.id}" class="error_msg">Test</span>
										<br/>
										<span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnare.year.coa}"></span>
										<input th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" id="datepicker" style="width: 300px;" readonly="readonly" class="form-control" type="text" th:field="*{year}" />
										<span th:if="${#fields.hasErrors('year')}" th:errors="*{year}"  style="color: red;" class="error_msg">Test</span>
									</div>
								</div>
								<div class="box-footer">
									<input class=" box-tools pull-right btn btn-primary" type="submit" value="Next"/>
								</div>
						</form>
				</div>
			</div>
		</div>
	</div>
	
	<div th:fragment="createQuestionnaireAssetLink">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary" data-module="module-questionnaireAsset">
					<form th:action="@{/{action}/{id}/assets/save(id=${questionnaireId},action=${action})}" method="post" th:object="${questionnaireAssetLink}" name="assetQuesList">
						<div class="box-body">
							<div class="form-group">
								<section class="content">
									<div class="row">
									<span></span><input type="hidden" th:field="*{name}"/>
										<span th:if="${#fields.hasErrors('name')}" th:errors="*{name}" class="error_msg">Test</span>
										<table  >
											<tbody>
												<tr>
													<td class="col-md-5 horizontal"><span th:text="#{questionnaire.selected.asset.type}"></span></td>
													<td class="col-md-7"><span th:text="${questionnaireAssetType}"></span></td>
												</tr>
											</tbody>
										</table>
										<table class="table table-bordered dataTable" id="assetList">
											<thead>
												<tr>
													<th class="col-md-1 no-sort" style="text-align: center;"><input	type="checkbox" value="1" id="asset-select-all" /></th>
													<th><span th:text="#{questionnaire.assetName}"/></th>
													<th><span th:text="#{questionnaire.assetType}"/></th>
												</tr>
											</thead>
											<tbody>
												<div th:each="oneEntry : ${mapByAssetType}">
													<tr th:each="oneAsset : ${oneEntry.value}" th:if="${oneAsset.shortName !=''}">
														<td class="col-md-1" style="text-align: center;"><input	type="checkbox" 
															th:checked="${#maps.containsKey(mapsOfSelectedAsset, oneAsset.id)}"
															name="assetarray[]" th:value="${oneAsset.id}" /></td>
														<td><span th:text="${oneAsset.shortName}"></span></td>
														<td><span th:text="${oneEntry.key}"></span></td>
													</tr>
												</div>
											</tbody>
										</table>
								 	</div> 
								</section>
							</div>
						</div>
						<div class="box-footer">
							<a class="btn btn-primary" th:href="@{/{action}/{id}/edit(id=${questionnaireId},action=${action})}" th:text="#{button.previous}"></a>
							<input class=" box-tools pull-right btn btn-primary" type="submit" value="Next"/>
						</div>
				  </form>
		  	</div>
		  	</div>
		 </div>
	</div>
	
	<tr th:fragment="viewAllPublishedQuestionnaire(oneRow,two)" th:id="${oneRow.id}">
		<td><a th:href="@{/questionnaire/view/{id}(id=${oneRow.id})}"><span th:text="${oneRow.name}">Test</span></a></td>
		<td><span th:text="${oneRow.description}">Test</span></td>
		<td><span th:text="${two.value}">Test</span></td>
	</tr>
</body>
</html>
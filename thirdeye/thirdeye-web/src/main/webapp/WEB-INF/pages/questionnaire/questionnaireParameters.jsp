<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle =${pageTitle})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	   <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.parameter}"></span>
	   <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.coststructure}"></span>
	</div>
	
	<div class="container"  th:fragment="contentContainer">
	    <input type="hidden" id = "questionnaireid" name="questionnaireId" th:value="${questionnaireId}"/>
	    <input type="hidden" id = "workspaceid" name="workspaceId" th:value="${workspaceId}"/>
		<div class="row" data-module="module-questionnaireParameter">
		  <div th:replace="questionnaire/questionnaireParameters :: questionnaireParameterFragment(${rootParameterList})"></div>
		</div>
		<div class="modal fade" data-module="module-parameterModal" tabindex="-1" role="dialog" style="display: none;">
	      <script type="text/x-config">{"mode":"questionnaireParameter"}</script>
		</div>
		<div class="modal fade paramTreeModalModule" data-module="module-paramTreeModal"></div>
	</div>
    <div class="col-md-12" th:fragment="questionnaireParameterFragment(rootParameterList)" >
		<div class="parameterTree box box-primary" >		     
			<div class="box-body table-responsive" data-module="common-data-table">
			    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType) and error eq 1}" style="color: red;">Please select parameter</span>
			    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType) and error eq 1}" style="color: red;">Please select cost stucture</span>
				<table class="table table-bordered table-condensed table-hover">
					<thead>
						<th>
						   <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.parameter.displayname}"></span>
						   <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.coststructure.displayname}"></span>
						</th>
						<th><span th:text="#{questionnaire.parameter.action}"></span></th>
					</thead>
					<tbody>
				      <tr th:each="oneParameter : ${rootParameterList}" th:id="${oneParameter.id}">
			              <td ><span th:text="${oneParameter.displayName}">Option in tree</span></td>
			              <td >
			                 <i class="deleteNode fa fa-trash-o" style="cursor: pointer" title="Delete" data-type="deleteParamOnQuestionnaire" th:attr="data-questionnaireid=${questionnaireId},data-workspaceid=${workspaceId},data-questionnairetype=${questionnaireType}"></i>
		                     <a data-type="viewParameterTree" th:attr="data-parameterid=${oneParameter.id}" class="fa fa-tree"  style="cursor: pointer" th:title="#{icon.title.treeview}"></a>
		                  </td>
		              </tr>
					</tbody>
			    </table>
	            <div class="box-footer">
	              <a th:href="@{/{action}/{questionnaireId}/assets(questionnaireId=${questionnaireId},action=${action})}" class="btn btn-default" th:text="#{button.previous}"></a>
			      <a class="addParameter btn btn-primary" data-type="addParamOnQuestionnaire" th:attr="data-questionnaireid=${questionnaireId},data-workspaceid=${workspaceId},data-questionnairetype=${questionnaireType}">
			        <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:text="#{questionnaire.add.parameters}"></span>
				    <span th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:text="#{tco.add.coststructures}"></span>
			      </a>
				  <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).DEF.name().equalsIgnoreCase(questionnaireType)}" th:href="@{/{action}/{questionnaireId}/orderquestions(questionnaireId=${questionnaireId},action=${action})}" class="btn btn-default" th:text="#{button.next}"></a>
				  <a th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireType).TCO.name().equalsIgnoreCase(questionnaireType)}" th:href="@{/{action}/{questionnaireId}/prepublish(questionnaireId=${questionnaireId},action=${action})}" class="btn btn-default" th:text="#{button.next}"></a>
	            </div>
	         </div>
	    </div>				
	</div>
    <div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-questionnaireParameter.js}"></script>
       <script	th:src="@{/static/js/3rdEye/modules/module-parameterModal.js}"></script>  
       <script	th:src="@{/static/js/3rdEye/modules/module-paramTreeModal.js}"></script>
    </div>
</body>
</html>

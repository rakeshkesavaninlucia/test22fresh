<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Grouped Chart</title>
</head>
<body>
	<div th:fragment="widgetContentWrapper" class="widgetContentWrapper">
		<div class="box box-primary widget facet-widget direct-chat" th:attr="data-widgetid=${oneWidget.id}" data-widgettype="widget_grouped">
			<div class="box-header with-border ">
				<h3 class="box-title" th:text="${oneWidget.title}">Widget name</h3>
				<input type="hidden" name="widgetType" th:value="${oneWidget.widgetType}" />
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="chat-pane-toggle" data-type="chat-pane-toggle">
						<i class="fa fa-cog"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="delete-widget" data-type="deleteWidget">
						<i class="fa fa-times"></i>
					</button>
				</div>
			</div>
			<div class="box-body chart-responsive ">
				<div class="row">
					<div class="col-md-12 direct-chat-messages">
						<div th:id="'widget_grouped_'+${oneWidget.id}" class='groupedGraph with-3d-shadow with-transitions'>
							<svg></svg>
						</div>
					</div>
					<div class="col-md-6 direct-chat-contacts">
						<form role="form" th:action="@{/widgets/grouped/plot}" method="post" class="graphFacetsControl">
							<input type="hidden" name="saveWidget"></input> 
							<input type="hidden" name="id" th:value="${oneWidget.id}" />
							<div class="col-md-12 form-group">
								<label>Select the questionnaire</label> 
								<select	class="form-control ajaxSelector questionnaireId" th:attr="data-dependency=${'graph1QE-' + oneWidget.id}" name="questionnaireId" data-type="ajaxSelectorGroup">
									<option value="-1">--Select--</option>
									<option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:selected="${oneQuestionnaire.id == oneWidget.questionnaireId}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
								</select>
							</div>
							<div class="col-md-12 form-group">
								<label>Select the question/parameter</label>
								<div class="radio fieldOne" data-type="fieldOne">
									<label> <input type="radio" name="fieldOne" value="question" th:checked="${'question' == oneWidget.fieldOne}" />Question</label>
									<label> <input type="radio" name="fieldOne" value="parameter" th:checked="${'parameter' == oneWidget.fieldOne}" />Parameter</label>
								</div>
							</div>
							<div class="col-md-12 form-group" id="questionOrParameterListDiv" th:fragment="questionOrParameterListSelect">
								<label>Select the question or parameter</label> 
								<select class="form-control ajaxPopulated fieldOneSelector" th:attr="data-dependson=${'graph1QE-' + oneWidget.id}" 	name="fieldOneId" th:fragment="questionList(listOfFieldOneItems)" th:remove="${ajaxRequest}? tag : none">
									<option value="-1">--Select--</option>
									<option th:each="oneQuestionOrParameter : ${listOfFieldOneItems}" th:selected="${oneQuestionOrParameter.id == oneWidget.fieldOneId}" th:value="${oneQuestionOrParameter.id}" th:text="${oneWidget.fieldOne == 'parameter'}? ${oneQuestionOrParameter.name} : ${oneQuestionOrParameter.title}" />
								</select>
							</div>
							<div class="col-md-12 form-group">
								<label>Select the question/parameter</label>
								<div class="radio fieldTwo" data-type="fieldTwo">
									<label> <input type="radio" name="fieldTwo" value="question" th:checked="${'question' == oneWidget.fieldTwo}" />Question</label>
									<label> <input type="radio" name="fieldTwo" value="parameter" th:checked="${'parameter' == oneWidget.fieldTwo}" />Parameter</label>
								</div>
							</div>
							<div class="col-md-12 form-group" id="questionOrParameterGroupByListDiv" th:fragment="questionOrParameterGroupByListSelect">
								<label>Select the question or parameter</label>
								<select class="form-control ajaxPopulated fieldTwoSelector" th:attr="data-dependson=${'graph1QE-' + oneWidget.id}" name="fieldTwoId" th:fragment="questionOrParameterGroupByList(listOfFieldTwoItems)" th:remove="${ajaxRequest}? tag : none">
									<option value="-1">--Select--</option>
									<option th:each="oneQuestionOrParameterGroupBy : ${listOfFieldTwoItems}" th:selected="${oneQuestionOrParameterGroupBy.id == oneWidget.fieldTwoId}" th:value="${oneQuestionOrParameterGroupBy.id}" th:text="${oneWidget.fieldTwo == 'parameter'}? ${oneQuestionOrParameterGroupBy.name} : ${oneQuestionOrParameterGroupBy.title}" />
								</select>
							</div>
							<div class="pull-right">
								<input type="button" class="btn btn-primary plotChart" value="Plot Chart" data-type="plotChart" />
								<input type="button" class="btn btn-primary saveWidget" value="Save Widget" data-type="saveWidget" />
							</div>
						</form>
					</div>
				</div>
				<div class="overlay" style="display: none;">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
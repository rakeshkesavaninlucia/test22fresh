<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.dashboard.nav}+ '-'+${dashboard.dashboardName})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">


text{
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

.toolTip {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    position: absolute;
    display: none;
    width: auto;
    height: auto;
    background: none repeat scroll 0 0 white;
    border: 0 none;
    border-radius: 8px 8px 8px 8px;
    box-shadow: -3px 3px 15px #888888;
    color: black;
    font: 12px sans-serif;
    padding: 5px;
    text-align: center;
}

.legend {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 60%;
}

rect {
    stroke-width: 2;
}

text {
  font: 10px sans-serif;
}

.axis text {
  font: 10px sans-serif;
  /* text-anchor: end !important; */
}

.axis path{
  fill: none;
  stroke: #000;
}

.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.axis .tick line {
  stroke-width: 1;
  stroke: rgba(0, 0, 0, 0.2);
}

.axisHorizontal path{
  fill: none;
}

.axisHorizontal line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.axisHorizontal .tick line {
  stroke-width: 1;
  stroke: rgba(0, 0, 0, 0.2);
}

.bar {
  fill: steelblue;
  fill-opacity: .9;
}

.x.axis path {
  display: none;
}

  </style>
</head>
<body>
		<div th:fragment="pageTitle">
			<span th:text="${dashboard.dashboardName}"></span><span data-toggle="control-sidebar" style="float:right;"><i class="glyphicon glyphicon-filter clickable btn btn-primary"></i></span>
			<div class="dropdown pull-right" sec:authorize="@securityService.hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})"  data-module="module-defaultDashboardTemplate" style = "right:3px;">
			  <button th:if="${status eq true}" class="btn btn-primary" style="display:none; position:  relative;top: 1px;" th:value="${dashboard.dashboardName}" th:id="${dashboard.id}" th:attr="data-currenturl=${'/dashboard/view/'+dashboard.id},data-idenfiervalue=${dashboard.dashboardName}" data-type="sendURLtoUser">Save as HomegPage</button>
			  <button th:if="${status eq false}"  type="submit"  class="btn btn-primary" style="position:  relative;top: 1px;" th:value="${dashboard.dashboardName}" th:id="${dashboard.id}" th:attr="data-currenturl=${'/dashboard/view/'+dashboard.id},data-idenfiervalue=${dashboard.dashboardName}" data-type="sendURLtoUser">Save as HomegPage</button>
			  <button class="btn btn-default dropdown-toggle" style="float:left;" type="button" id="dashboardActionDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    <i class="fa fa-cog"></i>
			    <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" aria-labelledby="dashboardActionDropDown">
			    <li><a class="editDashboard clickable" th:attr="data-dashboardid=${dashboard.id}" data-type="editDashboard">Edit Dashboard</a></li>
			    <li><a class="addWidget clickable" th:attr="data-dashboardid=${dashboard.id}" data-type="addWidget">Add Widget to Dashboard</a></li>
			    <li><a class="deleteDashboard clickable" th:attr="data-dashboardid=${dashboard.id}" data-type="deleteDashboard">Delete Dashboard</a></li>
			  </ul>
			</div>
		
		</div>
		
		<div th:fragment="contentContainer">
		<!-- Filter Impelmentation -->
				<div class="row" style="padding-bottom: 20px;padding-left: 20px;">
				<div style="padding-top:15px;" data-module="module-filterTags">
					<div class="col-md-12">					
						<div id="tags"></div>
					</div>
				</div>
				</div>
				
			      <aside class="control-sidebar control-sidebar-dark" style="margin-top: 101px;padding-top: 0px;">                
			        <div class="tab-content" style="height: 39em;overflow-y: auto;">            
			          <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active"> 
			          <div style="text-align: center;text-decoration: underline;"><h4 th:text="#{filter.panel}"></h4></div>      
						<div data-module="module-assetTypeFacet"></div>
						<div data-module="module-facetUpdate"></div>         
			          </div><!-- /.tab-pane -->          
			        </div>
			      </aside>     
	<!-- Filter Impelmentation End -->
		
			<div class="dashboard" th:attr="data-dashboardid=${dashboard.id}" data-module="module-widgetDrag">
				<div class="row widgetDrag" id='wizDrag'>
					<div th:fragment="widgetcontainer(widgets)" th:each="oneWidget : ${widgets}" class="widgetcontainer" th:classappend="${oneWidget.widgetType.size}">
						<div class="widgetplacement" th:attr="data-widgetid=${oneWidget.id},data-id=${oneWidget.id}" data-module="module-widget">
							<input type="hidden" name="widgetType" th:value="${oneWidget.widgetType}"/>
						</div>						
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<!-- <button class="btn btn-block btn-primary addWidget">Add a widget</button> -->
					</div>
				</div>
			</div>
		</div>	
		
	
	 <div th:fragment="scriptsContainer" th:remove="tag">
	  	<script	th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
	  	<script	th:src="@{/static/js/ext/nv.d3/1.8.1-dev/nv.d3.min.js}"></script>
	  	<script	th:src="@{/static/js/3rdEye/modules/module-defaultDashboardTemplate.js}"></script>
	  	<script	th:src="@{/static/js/3rdEye/modules/module-widget.js}"></script>
	  	<script	th:src="@{/static/js/3rdEye/modules/module-widgetDrag.js}"></script>
	  	<script th:src="@{/static/js/ext/sortable/Sortable.min.js}"></script>
	</div>
</body>
</html>
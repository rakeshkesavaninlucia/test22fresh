<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.workspacemanagement.workspacelist.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text = "#{pages.workspacemanagement.workspaceuser.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.workspacemanagement.workspaceuser.subtitle}"></span></div>
<div class="container"  th:fragment="contentContainer">
 <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary" data-module="module-editworkSpace">
	         <div class="box-body">
	         	        <div class="table-responsive" data-module="common-data-table">
				<div class="overlay">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
			<table class="table table-bordered table-condensed table-hover" id="templateColumnsofview">
				<thead>
					<tr>
						<th><span th:text="#{workspace.name.table}"></span></th>
						<th><span th:text="#{workspace.desc.table}"></span></th>
						<th><span th:text="#{workspace.date.table}"></span></th>
						<th><span th:text="#{workspace.action.table}"></span></th>
					</tr>
				</thead>
				<tbody>
					<tr th:each="oneOfworkspaces : ${allworkspaces}" th:id="${oneOfworkspaces.id}">
					<td><a th:href="@{/workspace/view/{id}(id=${oneOfworkspaces.id})}"><span th:text="${oneOfworkspaces.workspaceName}">Test</span></a></td>
					<td><span th:text="${oneOfworkspaces.workspaceDescription}">Test</span></td>
					<td><span th:text="${#dates.format(oneOfworkspaces.updatedDate, 'dd-MMM-yyyy')}">Test</span></td>
					<td><a class="fa fa-pencil-square-o clickable" th:title="#{icon.title.edit}" data-type="workspaceEdit" th:attr="data-wsid=${oneOfworkspaces.id}"></a>
					<a class="deleteRow fa fa-trash-o" th:href="@{/workspace/viewdeleteworkspace/{id}(id=${oneOfworkspaces.id})}" th:title="#{icon.title.delete}" sec:authorize="@securityService.hasPermission({'DELETE_WORKSPACE'})"></a></td>
					</tr>				
				</tbody>
			</table>
	</div>
	</div>
	 <div class="box-footer clearfix">	
	     <div>
            <a class="btn btn-primary" th:href="@{/workspace/create}">Create Workspace</a>
	     </div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div th:fragment="scriptsContainer" th:remove="tag">	
		<script th:src="@{/static/js/3rdEye/modules/module-editworkSpace.js}"></script>
	</div>
</body>
</html>
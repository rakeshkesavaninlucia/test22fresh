<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
</head>
<body>
<!-- create WorkSpace for User -->
	<div th:fragment="createWorkSpaceForm">
		<form th:action="@{/workspace/save}" method="post" th:object="${workspace}">
				<div class="form-group">
					<span th:text="#{workspce.name}"></span><span class="asterisk">*</span><input type="text" th:field="*{workspaceName}"  class="form-control" />
					<span style ="color:red;" th:if="${#fields.hasErrors('workspaceName')}" th:errors="*{workspaceName}">Test</span>				
	                  <br/>
					<span th:text="#{workspace.desc}"></span>
					<textarea rows="3" class="form-control" th:field="*{workspaceDescription}"></textarea>
					<br/>
					<input class="btn btn-primary" type="submit" value="SAVE"/>
				</div>
		</form>
	</div>
	<!-- create WorkSpace for User End -->
	
	<div th:fragment="createWorkSpaceForm">
	<div class="modal fade" id="editWorkspaceModal" tabindex="-1" role="dialog" aria-labelledby="editWorkspaceModal">
			<div class="modal-dialog" role="document" >
			  <div class="modal-content">
		<form th:action="@{/workspace/updateworkspacemodal}" method="post" th:object="${workspace}" id="editWorkspaceForm" >
			<input type="hidden" th:field="*{id}"/>	
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" th:text="#{workspace.edit}"></h4>
				</div>
				<div class="modal-body">	
				<div class="form-group">
					<span th:text="#{workspce.name}"></span><span class="asterisk">*</span><input type="text" th:field="*{workspaceName}"  class="form-control" />
					<span style ="color:red;" th:if="${#fields.hasErrors('workspaceName')}" th:errors="*{workspaceName}">Test</span>				
	                  <br/>
					<span th:text="#{workspace.desc}"></span>
					<textarea rows="3" class="form-control" th:field="*{workspaceDescription}"></textarea>
					<br/>
					</div>
					 <div class="modal-footer">
					<input class="btn btn-primary" type="submit" value="SAVE"/>
					</div>
				</div>
		</form>
		</div>
	</div>
	</div>
	</div>
	
	<tr th:fragment="detailofusersInWorkspace(oneRow, workspaceId)" th:id="${oneRow.id}">
	<input type="hidden" name="userId" th:value="${oneRow.id}" />
	<input type="hidden" name="workspaceId" th:value="${workspaceId}" />
	<td><span th:text="${oneRow.firstName}">Test</span></td>
	<td><span th:text="${oneRow.lastName}">Test</span></td>
	<td><span th:text="${oneRow.emailAddress}">Test</span></td>
	<td><a class="deleteRow fa fa-trash-o" th:title="#{icon.title.delete}"></a></td>
	</tr>
	
	<div th:fragment="scriptsContainer" th:remove="tag">	
		<script th:src="@{/static/js/3rdEye/modules/module-editworkSpace.js}"></script>
	</div>
</body>
</html>
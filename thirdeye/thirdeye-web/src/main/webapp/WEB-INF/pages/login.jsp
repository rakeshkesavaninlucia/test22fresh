<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
  <head>
    <title>Welcome to 3rdEye&trade;</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<!-- Latest compiled and minified CSS -->
	<link th:href="@{/static/css/ext/bootstrap-3.3.6.min.css}" rel="stylesheet" type="text/css" />
	
	
	<!-- Latest compiled and minified JavaScript -->
	<script	th:src="@{/static/js/ext/jquery/2.1.4/jquery.min.js}"></script>
	<script th:src="@{/static/js/ext/bootstrap/3.3.6/bootstrap.min.js}"></script>
	<style type="text/css">
		body, html {
		    height: 100%;
		    background-repeat: no-repeat;
		    background-image: linear-gradient(rgb(60,141,188), rgb(255,255,255));
		}
		
		.card-container.card {
		    max-width: 350px;
		    padding: 40px 40px;
		}
		
		.btn {
		    font-weight: 700;
		    height: 36px;
		    -moz-user-select: none;
		    -webkit-user-select: none;
		    user-select: none;
		    cursor: default;
		}
		
		/*
		 * Card component
		 */
		.card {
		    background-color: #F7F7F7;
		    /* just in case there no content*/
		    padding: 20px 25px 30px;
		    margin: 0 auto 25px;
		    margin-top: 50px;
		    /* shadows and rounded borders */
		    -moz-border-radius: 2px;
		    -webkit-border-radius: 2px;
		    border-radius: 2px;
		    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
		    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
		    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
		}
		
		.profile-img-card {
		    width: 96px;
		    height: 96px;
		    margin: 0 auto 10px;
		    display: block;
		    -moz-border-radius: 50%;
		    -webkit-border-radius: 50%;
		    border-radius: 50%;
		}
		
		/*
		 * Form styles
		 */
		.profile-name-card {
		    font-size: 16px;
		    font-weight: bold;
		    text-align: center;
		    margin: 10px 0 0;
		    min-height: 1em;
		}
		
		.reauth-email {
		    display: block;
		    color: #404040;
		    line-height: 2;
		    margin-bottom: 10px;
		    font-size: 14px;
		    text-align: center;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    -moz-box-sizing: border-box;
		    -webkit-box-sizing: border-box;
		    box-sizing: border-box;
		}
		
		.form-signin #inputEmail,
		.form-signin #inputPassword {
		    direction: ltr;
		    height: 44px;
		    font-size: 16px;
		}
		
		.form-signin input[type=email],
		.form-signin input[type=password],
		.form-signin input[type=text],
		.form-signin button {
		    width: 100%;
		    display: block;
		    margin-bottom: 10px;
		    z-index: 1;
		    position: relative;
		    -moz-box-sizing: border-box;
		    -webkit-box-sizing: border-box;
		    box-sizing: border-box;
		}
		
		.form-signin .form-control:focus {
		    border-color: rgb(104, 145, 162);
		    outline: 0;
		    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
		    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
		}
		
		.btn.btn-signin {
		    /*background-color: #4d90fe; */
		    background-color: rgb(60,141,188);
		    /* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
		    padding: 0px;
		    font-weight: 700;
		    font-size: 14px;
		    height: 36px;
		    -moz-border-radius: 3px;
		    -webkit-border-radius: 3px;
		    border-radius: 3px;
		    border: none;
		    -o-transition: all 0.218s;
		    -moz-transition: all 0.218s;
		    -webkit-transition: all 0.218s;
		    transition: all 0.218s;
		}
		
		.btn.btn-signin:hover,
		.btn.btn-signin:active,
		.btn.btn-signin:focus {
		    background-color: rgb(12, 97, 33);
		}
		
		.forgot-password {
		    color: rgb(104, 145, 162);
		}
		
		.forgot-password:hover,
		.forgot-password:active,
		.forgot-password:focus{
		    color: rgb(12, 97, 33);
		}
		.form-group.has-success span {
          color: #00a65a;
        }
        .form-group.has-error span {
          color: #dd4b39;
        }
	</style>
  </head>
  <body>
   
    
    
    <!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
    <div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <!-- <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" /> -->
            <b>Welcome to <span th:utext="#{3rdEye.logo}"><i>3rd</i>i&trade;</span></b>
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" method="post" action="#" th:action="@{/login}" id="loginFormPage">
                <div th:if="${param.error}" class="form-group has-error" >    
                    <span>Authentication Failed: Bad credentials</span>
                </div>
                <div th:if="${param.logout}" class="form-group has-success"> 
                    <span>You have been logged out.</span>
                </div>
                <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="true" autofocus="true"/>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="true"/>
            	<input type="text" id="tenantCode" name="tenantCode" class="form-control" placeholder="Account ID" required="true" autofocus="true"/>
                <!-- <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"/> Remember me
                    </label>
                </div> -->
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
                <a href="forgot" class="forgot-password">Forgot the password?</a>
                
            </form>
            <!-- /form -->
            <!-- <a href="#" class="forgot-password">
                Forgot the password?
            </a> -->
        </div><!-- /card-container -->
    </div><!-- /container -->
  </body>
</html>
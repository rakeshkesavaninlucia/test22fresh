<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.portfolio.nav})">
<head>
<meta charset="utf-8" />
<title>d3.js from Excel - ramblings.mcpher.com</title>
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
  Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.reports.goNoGo.title}"></span>
		</div>
	</div>
	
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.reports.goNoGo.subtitle}"></span>
	</div>

	<div th:fragment="contentContainer">
		<div data-module="module-viewGNG">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
						<div class="box-body" id="gngReportControl">
							<form role="form" th:action="@{/gng/plot}" method="post"
								id="gngReportControl">
								<div class="row">
									<div class="col-md-6 form-group">
										<label>Select the questionnaire</label> <select
											class="form-control ajaxSelector select2"
											th:attr="data-dependency=qe,data-parameterurl='/gng/fetchRootParameters/',data-asseturl='gng/fetchAssetTemplates/'"
											name="questionnaireId">
											<option value="-1">--Select--</option>
											<option th:each="oneQuestionnaire : ${listOfQuestionnaire}"
												th:value="${oneQuestionnaire.id}"
												th:text="${oneQuestionnaire.name}" />
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 form-group">
										<label>Select Parameters</label> <select
											class="form-control ajaxPopulated parameterSelector select2"
											th:attr="data-dependson=qe" name="parameterIds"
											multiple="multiple">
											<option value="-1">--Select--</option>
										</select>
									</div>

									<div class="col-md-6 form-group">
										<label>Select Asset Template</label> <select
											class="form-control ajaxPopulated assetTemplateSelector select2"
											th:attr="data-dependson=qe" name="assetTemplateIds"
											multiple="multiple">
											<option value="-1">--Select--</option>
										</select>
									</div>
								</div>
								<div class="box-footer">
									<div class="pull-right">
										<button type="submit" class="btn btn-primary">Plot</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary gngReport">
						<div class="box-header with-border">
							<h3 class="box-title mapTitle"></h3>
						</div>
						<div class="box-body chart-responsive">
							<div id="gngChart">
								<div id="gngReportPlot" class="gngReportPlot"
									style="height: 25em; width: 100%; overflow-x: scroll; white-space: nowrap"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<select th:fragment="optionListOfParameters(listOfParameters)"
		th:remove="tag">
		<option th:each="oneParameter : ${listOfParameters}"
			th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
	</select>

	<select th:fragment="optionListOfAssetTemplates(listOfAssetTemplates)"
		th:remove="tag">
		<option th:each="oneAssetTemplate : ${listOfAssetTemplates}"
			th:value="${oneAssetTemplate.id}"
			th:text="${oneAssetTemplate.assetTemplateName}" />
	</select>

	<div th:fragment="scriptsContainer" th:remove="tag">
		<script th:src="@{/static/js/ext/d3/3.5.6/d3.min.js}"></script>
		<script th:src="@{/static/js/ext/d3.tip/0.6.3/d3.tip.js}"></script>
		<script th:src="@{/static/js/ext/colorbrewer/colorbrewer.js}"></script>
		<script th:src="@{/static/js/3rdEye/modules/module-viewGNG.js}"></script>
		<script type="text/javascript" th:inline="javascript"></script>
	</div>
</body>
</html>
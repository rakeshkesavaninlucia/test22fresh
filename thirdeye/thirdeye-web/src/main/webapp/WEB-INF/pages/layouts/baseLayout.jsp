<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org" th:fragment="baseLayoutContainer(dataController, dataAction,pageTitle)"
	xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity4">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title th:text="${pageTitle}"></title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
<meta name="_csrf" th:content="${_csrf.token}" />
<meta name="_csrf_header" th:content="${_csrf.headerName}" />
<link th:href="@{/static/css/ext/jquery-ui/1.11.4/jquery-ui.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/ext/bootstrap-3.3.6.min.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/ext/bootstrap-datepicker.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/ext/select2-4.0.1.min.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/3rdEye/dataTables.bootstrap.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/ext/font-awesome-4.3.0.min.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/ext/buttons.dataTables.min.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/ext/ionicons-2.0.1.min.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/3rdEye/circle.css}" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link th:href="@{/static/css/ext/AdminLTE.min.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/ext/skin-blue.min.css}" rel="stylesheet" type="text/css" />
<!-- Application CSS -->
<link th:href="@{/static/css/3rdEye/3rdi.css}" rel="stylesheet" type="text/css" />
<link th:href="@{/static/css/3rdEye/nv.d3.css}" rel="stylesheet" type="text/css" />
<!-- jsTree style CSS -->
<link th:href="@{/static/css/ext/jstree/3.0.9/jsTree-style.min.css}" rel="stylesheet" type="text/css" />

<div th:replace="this :: onPageStyles"></div>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="skin-blue sidebar-mini" th:attr="data-controller=${dataController},data-action=${dataAction}">
	<div class="wrapper">

		<!-- Main Header -->
		<header class="main-header">

			<!-- Logo -->
			<a th:href="@{/home}" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><span th:utext="#{3rdEye.logo}"><i>3rd</i>i&trade;</span></span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><span th:utext="#{3rdEye.logo}"><i>3rd</i>i&trade;</span></span>
			</a>

			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span></a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu" data-module="navbar-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="active-workspacename" th:text=" ${@customUserDetailsServiceImpl.getActiveWorkSpaceForUser().getWorkspaceName()}">Active Workspace</span>
							</a>
							<ul class="dropdown-menu">
								<li class="header text-bold">Please select a workspace</li>
								<li>
									<ul class="menu">
										<li th:each="oneWs : ${@customUserDetailsServiceImpl.getCurrentUser().getUserWorkspaces()}" th:id="${oneWs.workspace.id}">
											<a class="workspaceSwitch" data-type="workspaceSwitch" th:attr="data-workspacename=${oneWs.workspace.workspaceName},data-workspaceid=${oneWs.workspace.id}">
												<h4 style="margin: 0 0 0 5px;" th:text="${oneWs.workspace.workspaceName}">Workspace 1</h4>
												<p style="margin: 0 0 0 5px;" th:text="${oneWs.workspace.workspaceDescription}">Why not buy a new awesome theme?</p>
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>

						<!-- User Account Menu -->
						<li class="dropdown user user-menu">
							<!-- Menu Toggle Button --> 
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<!-- The user image in the navbar-->
								<img th:if ="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() eq null}" th:src="@{/static/img/user2-160x160.jpg}" class="user-image" alt="User Image" />
									<img th:if ="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() ne null}" th:src="@{/user/{id}/showimage(id=${@customUserDetailsServiceImpl.getCurrentUser().getId()})}" class="user-image" alt="User Image" />
								<!-- hidden-xs hides the username on small devices so only the image appears. -->
								<span class="hidden-xs" th:text="${@customUserDetailsServiceImpl.getCurrentUser().getFirstName()} + ' ' + ${@customUserDetailsServiceImpl.getCurrentUser().getLastName()}"></span>
							</a>
							<ul class="dropdown-menu">
								<!-- The user image in the menu -->
								<li class="user-header" >
								<img th:if ="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() eq null}" th:src="@{/static/img/user2-160x160.jpg}" class="img-circle" alt="User Image"/>
									<img th:if ="${@customUserDetailsServiceImpl.getCurrentUser().getPhoto() ne null}" th:src="@{/user/{id}/showimage(id=${@customUserDetailsServiceImpl.getCurrentUser().getId()})}" class="img-circle" alt="User Image" />
									<p>
										<span th:text="${@customUserDetailsServiceImpl.getCurrentUser().getFirstName()} + ' ' + ${@customUserDetailsServiceImpl.getCurrentUser().getLastName()}"></span>
										<small>Member since <span th:text="${#dates.format(@customUserDetailsServiceImpl.getCurrentUser().getCreatedDate(), 'MMM.yyyy')}"></span></small>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a th:href="@{/user/{id}/editprofile(id=${@customUserDetailsServiceImpl.getCurrentUser().getId()})}"  class="btn btn-default btn-flat">Profile</a>
									</div>
									
									<div class="pull-right">
										<a th:href="@{/logout}" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar" data-module="module-3rdiApp">
				<!-- Sidebar Menu -->
				<ul class="sidebar-menu">
					<li class="header">Main Navigation</li>
					<li class="treeview">
						<a href="#"> 
							<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li sec:authorize="@securityService.hasPermission({'WORKSPACES_MODIFY_DASHBOARD'})"><a href="#" class="createDashboard" data-type="createDashboard">Add a new dashboard</a></li>
							<li th:each="oneDashboard : ${@dashboardService.listDashboardForActiveWorkspace()}" sec:authorize="@securityService.hasPermission({'WORKSPACES_MODIFY_DASHBOARD','WORKSPACES_VIEW_DASHBOARD'})">
								<a th:href="@{/dashboard/view/{id}(id=${oneDashboard.id})}" th:text="${oneDashboard.dashboardName}"><i class="fa fa-circle-o"></i>Dash 2 </a>
							</li>
						</ul>
					</li>
	
					<li class="treeview" sec:authorize="@securityService.hasPermission({'BCM_MODIFY','BCM_VIEW','FUNCTIONAL_MAP_MODIFY','FUNCTIONAL_MAP_VIEW'})">
						<a href="#"> <i class="fa fa-sitemap"></i> <span>Business Capability Maps</span> <i class="fa fa-angle-left pull-right"></i>	</a>
						<ul class="treeview-menu">
							<li class="treeview" sec:authorize="@securityService.hasPermission({'BCM_VIEW'})">
								<a th:href="@{/bcm/list}">View BCM Templates</a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'BCM_MODIFY'})" data-type="createWorkspaceBCM">
								<a href="#" class="createWorkspaceBCM" th:text="#{bcm.workspace.manageCapabilityMap}"></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'BCM_VIEW'})">
								<a th:href="@{/wsbcm/list}">View Workspace BCM</a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'FUNCTIONAL_MAP_VIEW'})">
								<a th:href="@{/fm/view}">Functional Maps</a>
							</li>
						</ul>
					</li>
					
					<li class="treeview" sec:authorize="@securityService.hasPermission({'REPORT_ASSET_GRAPH','REPORT_SCATTER_GRAPH','REPORT_FACET_GRAPH', 'VIEW_TCO', 'GNG_VIEW'})">
						<a href="#"> <i class="fa fa-bar-chart"></i> <span>Reports</span><i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
						    <li class="treeview" sec:authorize="@securityService.hasPermission({'VIEW_SAVED_REPORT'})">
								<a th:href="@{/report/myReports}"><span>My Saved Reports</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'REPORT_ASSET_GRAPH'})">
								<a th:href="@{/add/list}"><span>Dependency Diagram</span></a>
							</li>
							 <li class="treeview" sec:authorize="@securityService.hasPermission({'GNG_VIEW'})">
								<a th:href="@{/gng/view}"><span>Inventory Asset GO/No-GO Analysis</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'REPORT_SCATTER_GRAPH'})">
								<a th:href="@{/graph/scatter/view}"><span>Health Analysis</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'AID_VIEW'})">
								<a th:href="@{/aid/list}"><span>Asset Interface</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'REPORT_PHP'})">
								<a th:href="@{/portfoliohealth/view}"><span>Portfolio Health</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'BCM_VIEW'})">
								<a th:href="@{/bcm/view}"><span>Asset Mapping</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'REPORT_FUNCTIONAL_REDUNDANCY'})">
								<a th:href="@{/functionalRed/bcmView}"><span>Functional Redundancy</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'VIEW_TCO'})">
								<a th:href="@{/tco/reports/list}"> <span>Chart of Account</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'REPORT_WAVE_ANALYSIS'})">
								<a th:href="@{/graph/scatter/viewWave}"><span>Wave Analysis</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'VIEW_TCO'})">
								<a th:href="@{/graph/tco/view}"><span>TCO Analysis</span></a>
							</li>
						</ul>
					</li>

					<!-- TCO Management menu -->
					<li class="treeview" sec:authorize="@securityService.hasPermission({'VIEW_TCO','MODIFY_TCO'})">
						<a href="#"> <i class="fa fa-usd"></i> <span>TCO Management</span><i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li class="treeview" sec:authorize="@securityService.hasPermission({'VIEW_TCO'})">
								<a th:href="@{/costElement/view}"> <span>Cost Element</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'VIEW_TCO'})">
								<a th:href="@{/coststructure/list}"><span>Cost Structure</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})">
								<a th:href="@{/tco/list}"> <span>Chart of Account</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'VIEW_TCO'})">
								<a th:href="@{/tco/response/list}"> <span>Submit Cost</span></a>
							</li>
						</ul>
					</li>
					
					<li class="treeview" sec:authorize="@securityService.hasPermission({'RELATIONSHIP_MODIFY','RELATIONSHIP_VIEW','PARAMETER_MODIFY','PARAMETER_VIEW'})">
						<a href="#"> <i class="fa fa fa-cogs"></i> <span>Configuration</span><i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
						 	<li class="treeview" sec:authorize="@securityService.hasPermission({'RELATIONSHIP_MODIFY','RELATIONSHIP_VIEW'})">
								<a th:href="@{/relationship/list}" th:text="#{relationship.type.menu}"></a>
							</li> 
							<li class="treeview" sec:authorize="@securityService.hasPermission({'TEMPLATE_VIEW','TEMPLATE_MODIFY'})">
								<a href="#"> <i class="fa fa-file-text"></i> <span>Template Management</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'TEMPLATE_MODIFY'})">
										<a th:href="@{/templates/create}"><span>Create Template</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'TEMPLATE_VIEW'})">
										<a th:href="@{/templates/list}"> <span>View Templates</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'TEMPLATE_VIEW','TEMPLATE_MODIFY'})">
										<a href="#"> <i class="fa fa-archive"></i> <span>Inventory</span><i class="fa fa-angle-left pull-right"></i></a>
										<ul class="treeview-menu">
											<li th:each="oneTemplate : ${@assetTemplateService.listTemplateForActiveWorkspace()}">
												<a th:href="@{/templates/data/view/{id}(id=${oneTemplate.id})}" th:text="${#strings.abbreviate(oneTemplate.assetTemplateName,30)}"><i class="fa fa-circle-o"></i></a>
											</li>
											<li class="treeview">
												<a href="#"> <i class="fa fa-arrows-h"></i> <span>Relationship Template</span><i class="fa fa-angle-left pull-right"></i></a>
												<ul class="treeview-menu">
													<li th:each="oneTemplate : ${@assetTemplateService.listRelationshipTemplateForActiveWorkspace()}">
														<a th:href="@{/templates/data/view/{id}(id=${oneTemplate.id})}" th:text="${#strings.abbreviate(oneTemplate.assetTemplateName,28)}"><i class="fa fa-circle-o"></i></a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'COLOR_SCHEME_MODIFY','COLOR_SCHEME_VIEW'})">
								<a href="#"> <i class="fa fa-paint-brush"></i> <span>Color Scheme</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'COLOR_SCHEME_MODIFY','COLOR_SCHEME_VIEW'})">
										<a th:href="@{/qualityGates/view}" th:text="#{color.quality.gates}"></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'COLOR_SCHEME_VIEW'})">
										<a th:text="#{color.asset}"></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'COLOR_SCHEME_VIEW'})">
										<a th:text="#{color.adhoc}"></a>
									</li>
								</ul>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'INDEX_CREATION'})">
								<a href="#"> <i class="fa fa-th-list"></i> <span>Index Creation</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'INDEX_CREATION'})">
										<a th:href="@{/index/indexHome}" th:text="#{search_index_creation}"></a>
									</li>
								</ul>
							</li>
							
							<li class="treeview" sec:authorize="@securityService.hasPermission({'BENCHMARK_MODIFY','BENCHMARK_VIEW'})">
								<a href="#"> <i class="fa fa-line-chart"></i> <span>Benchmark</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'BENCHMARK_VIEW'})">
										<a th:href="@{/benchmark/listBenchmark}" th:text="#{benchmark.view}"></a>
									</li>
								</ul>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_MODIFY','QUESTIONNAIRE_VIEW'})">
								<a href="#"><i class="fa fa-check-square-o"></i><span>Questionnaire Management</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_MODIFY'})">
										<a th:href="@{/questionnaire/create}"> <span>Create Questionnaire</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_VIEW'})">
										<a th:href="@{/questionnaire/list}"> <span>View Questionnaire</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_VIEW'})">
										<a th:href="@{/questionnaire/response/list}"> <span>Response Status</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTIONNAIRE_VIEW'})">
										<a th:href="@{/questionnaire/submit/response/list}"> <span>Submit Response</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTION_MODIFY','QUESTION_VIEW'})">
										<a href="#"> <i class="fa fa-question"></i> <span>Question Management</span><i class="fa fa-angle-left pull-right"></i></a>
										<ul class="treeview-menu">
											<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTION_MODIFY'})">
												<a th:href="@{/question/create}"> <span>Create Question</span></a>
											</li>
											<li class="treeview" sec:authorize="@securityService.hasPermission({'QUESTION_VIEW'})">
												<a th:href="@{/question/list}"> <span>View Question</span></a>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'PARAMETER_MODIFY','PARAMETER_VIEW'})">
								<a href="#"> <i class="fa fa-puzzle-piece"></i> <span>Parameter Management</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'PARAMETER_MODIFY','PARAMETER_VIEW'})">
										<a th:href="@{/parameter/list}"> <span>View Parameter</span></a>
									</li>
								</ul>
							</li>	
						</ul>
					</li>

					<li class="treeview" sec:authorize="@securityService.hasPermission({'SNOW_VIEW'})">
						<a href="#"> <i class="fa fa-file-text"></i> <span>Ticket Management</span><i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li class="treeview" sec:authorize="@securityService.hasPermission({'SNOW_VIEW'})">
								<a th:href="@{/snow/snow}"><span>SNOW Tickets</span></a>
							</li>
						</ul>
					</li>
					
					<li class="treeview" >
						<a href="#"> <i class="fa fa-user-plus"></i> <span>Admin</span><i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li class="treeview"
								sec:authorize="@securityService.hasPermission({'ADMIN_VIEW'})">
								<a th:href="@{/adminreports}"><span>Admin Reports</span></a>
							</li>
							<li class="treeview"
								sec:authorize="@securityService.hasPermission({'ASSET_TYPE_MANAGEMENT'})">
								<a th:href="@{/assettype/list}"><span>Asset Type Management</span></a>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'USER_MODIFY','USER_VIEW','USER_ROLE_VIEW','USER_ROLE_MODIFY'})">
								<a href="#"> <i class="fa fa-users"></i> <span>User Management</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'USER_MODIFY'})">
										<a th:href="@{/user/create}"> <span>Create User</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'USER_VIEW'})">
										<a th:href="@{/user/list}"> <span>View Users</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'USER_ROLE_VIEW'})">
										<a th:href="@{/user/roles/list}"> <span>View User Roles</span></a>
									</li>
								</ul>
							</li>
							<li class="treeview" sec:authorize="@securityService.hasPermission({'WORKSPACES_MODIFY','WORKSPACES_VIEW'})">
								<a href="#"> <i class="fa fa-laptop"></i> <span>Workspace Management</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview" sec:authorize="@securityService.hasPermission({'WORKSPACES_MODIFY'})">
										<a th:href="@{/workspace/create}"><span>Create Workspaces</span></a>
									</li>
									<li class="treeview" sec:authorize="@securityService.hasPermission({'WORKSPACES_MODIFY'})">
										<a th:href="@{/workspace/list}"><span>Workspace Management</span></a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					
					
				</ul>
				<!-- /.sidebar-menu -->
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					<div th:include="this :: pageTitle" />
					<small><div th:include="this :: pageSubTitle" /></small>
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<!-- Your Page Content Here -->
				<div th:include="this :: contentContainer" />
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<!-- Main Footer -->
		<footer class="main-footer">
			<!-- To the right -->
			<div class="pull-right hidden-xs"></div>
			<!-- Default to the left -->
			<strong>Copyright &copy; 2015 <a href="http://www.birlasoft.com">Birlasoft</a>.
			</strong> All rights reserved. <span class="pull-right hidden-sm" th:text="#{application.name} + '-' + #{application.version} + '(' + #{scm.build.version} + ')'"></span>
		</footer>

	</div>
	<!-- ./wrapper -->

	<!-- REQUIRED JS SCRIPTS -->
	<script th:src="@{/static/js/ext/jquery/1.7.1/jquery-1.7.1.js}"></script>
	<script th:src="@{/static/js/ext/jquery/2.1.4/jquery.min.js}"></script>
	<script th:src="@{/static/js/ext/jquery-ui/1.11.4/jquery-ui.js}"></script>
	<script th:src="@{/static/js/ext/bootstrap/3.3.6/bootstrap.min.js}"></script>
	<script th:src="@{/static/js/ext/bootstrap/bootstrap-datepicker.js}"></script>
	<script th:src="@{/static/js/ext/select2/4.0.1/select2.full.min.js}"></script>
	<script th:src="@{/static/js/ext/adminLTE/2.1.0/adminLTE.min.js}"></script>
	<script th:src="@{/static/js/ext/sidenav/side.nav.min.js}"></script>
	<script th:src="@{/static/js/ext/multiselect/2.1.1/multiselect.min.js}"></script>
	<script th:src="@{/static/js/ext/jquery.form/3.51.0/jquery.form.js}"></script>
	<script th:src="@{/static/js/ext/jquery.dataTables/1.10.7/jquery.dataTables.min.js}"></script>
	<script th:src="@{/static/js/ext/jquery.dataTables/dataTables.bootstrap.min.js}"></script>
	<script th:src="@{/static/js/ext/jquery.dataTables/dataTables.buttons.min.js}"></script>
	<script th:src="@{/static/js/ext/jquery.dataTables/dataTables.fixedColumns.min.js}"></script>
	<script th:src="@{/static/js/ext/jszip/jszip.min.js}"></script>
	<script th:src="@{/static/js/ext/buttons.html5/buttons.html5.min.js}"></script>
	<script th:src="@{/static/js/ext/pdfmake/pdfmake.min.js}"></script>
	<script th:src="@{/static/js/ext/pdfmake/vfs_fonts.js}"></script>
	<script th:src="@{/static/js/ext/jstree/3.0.9/jstree.min.js}"></script>
	<script th:src="@{/static/js/ext/t3/2.1.0/t3-jquery.min.js}"></script>
	<script th:src="@{/static/js/ext/colorbrewer/colorbrewer.js}"></script>
	<script th:src="@{/static/js/ext/spinjs/2.3.2/spin.min.js}"></script>
	<script th:src="@{/static/js/ext/notify/notify.min.js}"></script>
	<script th:src="@{/static/js/ext/html2canvas/0.5.0-alpha1/html2canvas.min.js}"></script>
	<script th:src="@{/static/js/ext/canvas2Image/canvas2image.js}"></script>
	<script th:src="@{/static/js/ext/ddslick/jquery.ddslick.min.js}"></script>
    <script th:src="@{/static/js/ext/countrySelect/jquery.countryselector.es5.min.js}"></script>
     <script th:src="@{/static/js/ext/jqueryNumeric/jquery.numeric.min.js}"></script>
	<script th:src="@{/static/js/3rdEye/3rdi.js}"></script>
	<script th:src="@{/static/js/3rdEye/3rdi-app.js}"></script>

	<script th:src="@{/static/js/3rdEye/modules/navbar-menu.js}"></script>
	<script th:src="@{/static/js/3rdEye/modules/common-data-table.js}"></script>
	<script th:src="@{/static/js/3rdEye/modules/module-3rdiApp.js}"></script>
	<script th:src="@{/static/js/3rdEye/modules/filter/module-assetTypeFacet.js}"></script>
	<script th:src="@{/static/js/3rdEye/modules/filter/module-facetUpdate.js}"></script>
	<script th:src="@{/static/js/3rdEye/modules/filter/module-filterTags.js}"></script>
	<script th:src="@{/static/js/3rdEye/services/commonUtils.js}"></script>
	<script th:src="@{/static/js/3rdEye/services/jsonService.js}"></script>
	<script th:src="@{/static/js/3rdEye/services/service-notify.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-select2.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behaviour-showsParamTree.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-collapseable.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-lazyLoaded.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-fetchParameterGraph.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-datepicker.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-download.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-downloadD3.js}"></script>
	<script th:src="@{/static/js/3rdEye/behaviours/behavior-popOver.js}"></script>
	<script th:src="@{/static/js/3rdEye/services/service-spinner.js}"></script>
	

	<!-- Every page can add page specific scripts -->
	<div th:replace="this :: scriptsContainer"></div>

	<script th:inline="javascript">
		$.ThirdEye.appContext = [[@{/}]];
	</script>

	<!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience. Slimscroll is required when using the
          fixed layout. -->
</body>
</html>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.birlasoft.thirdeye</groupId>
	<artifactId>thirdeye</artifactId>
	<version>1.22.0</version>
	<packaging>pom</packaging>

	<name>3rdEye</name>
	<url>http://maven.apache.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<springVersion>4.1.7.RELEASE</springVersion>
		<java.version>1.8</java.version>
		<springSecurityVersion>4.0.1.RELEASE</springSecurityVersion>
		<spring-data-jpa.version>1.9.0.RELEASE</spring-data-jpa.version>
		<hibernateVersion>5.0.3.Final</hibernateVersion>
		<slf4j.version>1.7.5</slf4j.version>
		<logback.version>1.0.13</logback.version>
		<aspectj.version>1.7.2</aspectj.version>
		<mysql.version>5.1.41</mysql.version>

	</properties>


	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-war-plugin</artifactId>
					<version>2.6</version>
					<configuration>
						<failOnMissingWebXml>false</failOnMissingWebXml>
						<archive>
							<manifestEntries>
								<SCM-Revision>${scm.revision}</SCM-Revision>
							</manifestEntries>
						</archive>
					</configuration>
				</plugin>
			</plugins>
			

		</pluginManagement>
		
		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
		</plugins>

	</build>
	<dependencyManagement>
		<dependencies>

			<!-- Module dependencies - all to be on the same project -->
			<dependency>
				<groupId>${project.groupId}</groupId>
				<artifactId>thirdeye-persistence-deps</artifactId>
				<version>${project.version}</version>
				<type>pom</type>
			</dependency>
			
			<dependency>
				<groupId>${project.groupId}</groupId>
				<artifactId>thirdeye-web</artifactId>
				<version>${project.version}</version>
			</dependency>
			
			<dependency>
				<groupId>${project.groupId}</groupId>
				<artifactId>thirdeye-web-services</artifactId>
				<version>${project.version}</version>
			</dependency>
			
			<dependency>
				<groupId>${project.groupId}</groupId>
				<artifactId>thirdeye-persistence-core</artifactId>
				<version>${project.version}</version>
			</dependency>
			
			<dependency>
				<groupId>${project.groupId}</groupId>
				<artifactId>thirdeye-common-utils</artifactId>
				<version>${project.version}</version>
			</dependency>
			
			<dependency>
				<groupId>${project.groupId}</groupId>
				<artifactId>thirdeye-search-api</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
				<groupId>${project.groupId}</groupId>
				<artifactId>thirdeye-indexer-web</artifactId>
				<version>${project.version}</version>
			</dependency>
			<dependency>
			    <groupId>org.apache.commons</groupId>
			    <artifactId>commons-lang3</artifactId>
			    <version>3.0</version>
			</dependency>
			
			<!-- Spring dependencies -->
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-core</artifactId>
				<version>${springVersion}</version>
				<exclusions>
				    <exclusion>
				      <artifactId>commons-logging</artifactId>
				      <groupId>commons-logging</groupId>
				    </exclusion>
				  </exclusions>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-webmvc</artifactId>
				<version>${springVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-web</artifactId>
				<version>${springVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-aop</artifactId>
				<version>${springVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-orm</artifactId>
				<version>${springVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
		  		<artifactId>spring-tx</artifactId>
				<version>${springVersion}</version>
			</dependency>
			<dependency>
				<groupId>commons-dbcp</groupId>
				<artifactId>commons-dbcp</artifactId>
				<version>1.4</version>
			</dependency>
	
			<!-- Spring Security dependencies -->
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-core</artifactId>
				<version>${springSecurityVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-taglibs</artifactId>
				<version>${springSecurityVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-web</artifactId>
				<version>${springSecurityVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-config</artifactId>
				<version>${springSecurityVersion}</version>
			</dependency>
	
			<dependency>
				<groupId>org.aspectj</groupId>
				<artifactId>aspectjweaver</artifactId>
				<version>${aspectj.version}</version>
			</dependency>
			<dependency>
				<groupId>org.aspectj</groupId>
				<artifactId>aspectjrt</artifactId>
				<version>${aspectj.version}</version>
			</dependency>
			
			
			

			<!-- Hibernate dependencies -->
			<dependency>
				<groupId>org.springframework.data</groupId>
				<artifactId>spring-data-jpa</artifactId>
				<version>${spring-data-jpa.version}</version>
			</dependency>
			<dependency>
				<groupId>org.hibernate</groupId>
				<artifactId>hibernate-core</artifactId>
				<version>${hibernateVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.hibernate</groupId>
				<artifactId>hibernate-entitymanager</artifactId>
				<version>${hibernateVersion}</version>
			</dependency>
			<dependency>
				<groupId>org.hibernate</groupId>
				<artifactId>hibernate-validator</artifactId>
				<version>5.1.3.Final</version>
			</dependency>

			<!-- Connectors to the database -->
			<dependency>
				<groupId>mysql</groupId>
				<artifactId>mysql-connector-java</artifactId>
				<version>${mysql.version}</version>
			</dependency>
			
			<!-- Logging dependencies -->
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>jcl-over-slf4j</artifactId>
				<version>${slf4j.version}</version>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>${slf4j.version}</version>
			</dependency>
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-log4j12</artifactId>
				<version>${slf4j.version}</version>
			</dependency>

			<dependency>
				<groupId>javax.servlet</groupId>
				<artifactId>javax.servlet-api</artifactId>
				<version>3.1.0</version>
			</dependency>
		<!-- <dependency> <groupId>javax.servlet</groupId> <artifactId>jsp-api</artifactId> 
			<version>2.0</version> </dependency> -->

			<!-- jstl for jsp page -->
			<dependency>
				<groupId>jstl</groupId>
				<artifactId>jstl</artifactId>
				<version>1.2</version>
			</dependency>
			<dependency>
				<groupId>org.thymeleaf</groupId>
				<artifactId>thymeleaf-spring4</artifactId>
				<version>2.1.4.RELEASE</version>
			</dependency>
			<dependency>
				<groupId>org.thymeleaf.extras</groupId>
				<artifactId>thymeleaf-extras-springsecurity4</artifactId>
				<version>2.1.2.RELEASE</version>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-core</artifactId>
				<version>2.6.6</version>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-annotations</artifactId>
				<version>2.6.6</version>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-databind</artifactId>
				<version>2.6.6</version>
			</dependency>
			
			<!-- POI -->
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>poi</artifactId>
				<version>3.14</version>
			</dependency>
			
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>poi-ooxml</artifactId>
				<version>3.14</version>
			</dependency>
			
			<dependency>
				<groupId>commons-fileupload</groupId>
				<artifactId>commons-fileupload</artifactId>
				<version>1.3.1</version>
			</dependency>
	
			<dependency>
				<groupId>commons-io</groupId>
				<artifactId>commons-io</artifactId>
				<version>2.4</version>
			</dependency>
			
			<dependency>
				<groupId>org.elasticsearch</groupId>
				<artifactId>elasticsearch</artifactId>
				<version>5.4.1</version>
			</dependency>
			
			<dependency>
   				 <groupId>org.apache.httpcomponents</groupId>
    			 <artifactId>httpclient</artifactId>
   		 		 <version>4.3.6</version>
			</dependency>
			
		</dependencies>
		
		
	</dependencyManagement>

	<modules>
		<module>thirdeye-persistence-deps</module>
		<module>thirdeye-web</module>
		<module>thirdeye-web-services</module>
		<module>thirdeye-persistence-core</module>
		<module>thirdeye-common-utils</module>
		<module>thirdeye-search</module>

	</modules>
	
	
</project>

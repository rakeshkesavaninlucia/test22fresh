package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;

import org.birlasoft.thirdeye.entity.AidBlock;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AIDBlockRepository extends JpaRepository<AidBlock, Serializable> {
	
	
}

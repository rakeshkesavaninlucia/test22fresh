package org.birlasoft.thirdeye.repositories;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.GraphAssetTemplate;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *	Repository interface for graph.
 */
public interface GraphRepository extends JpaRepository<Graph, Integer> {
	/**
	 * Get all List of {@code Graphs} belongs to {@code Workspace}
	 * @param workspaces
	 * @return List{@code <Graph>} Object
	 */
	public List<Graph> findByWorkspaceIn(Set<Workspace> workspaces);
	/**
	 * Find list of graph by workspace and graph type.
	 * @param workspace
	 * @param graphType
	 * @return {@code List<Graph>}
	 */
	public List<Graph> findByWorkspaceAndGraphType(Workspace workspace, String graphType);
	
	/**
	 * Find list of graph by {@link GraphAssetTemplate}
	 * @param graphAssetTemplate
	 * @return
	 */
	public List<Graph> findByGraphAssetTemplates(GraphAssetTemplate graphAssetTemplate);
	
}

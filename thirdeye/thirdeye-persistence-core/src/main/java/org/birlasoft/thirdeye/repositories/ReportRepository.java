package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Report;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for Report.
 * @author sunil1.gupta
 */
public interface ReportRepository extends JpaRepository<Report, Serializable> {
	
    
	List<Report> findByWorkspaceAndUserByCreatedBy(Workspace activeWorkSpaceForUser, User currentUser);
	
	List<Report> findByWorkspaceIn(Set<Workspace> workspaces);

	Report findByWorkspaceAndStatus(Workspace activeWorkSpaceForUser, boolean b);
	
	Report findByreportName(String reportName);
	
}

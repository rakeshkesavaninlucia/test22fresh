package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;

import org.birlasoft.thirdeye.entity.AssetType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository interface for asset type.
 * @author samar.gupta
 */
public interface AssetTypeRepository extends JpaRepository<AssetType, Serializable> {
	/**
     * find By AssetType Name.
     * @param assetTypeName
     * @return {@code AssetType}
     */
	public AssetType findByAssetTypeName(String assetTypeName);
	
	
	/** update delete status
	 * @param assetId
	 */
	@Modifying
	@Query("UPDATE AssetType a SET a.deleteStatus=1 WHERE a.id =?1")
	public void updateDeleteStatus(Integer assetId);
	
	/**get asset type by delete status
	 * @param deleteStatus
	 * @return
	 */
	public List<AssetType> findByDeleteStatus(Boolean deleteStatus);
}

package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository interface for asset template.
 */
public interface AssetTemplateRepository extends JpaRepository<AssetTemplate, Serializable> {
	/** 
	 * Find list of asset template by asset type and createdBy.
	 * @param assetType
	 * @param currentuser
	 * @return {@code List<AssetTemplate>}
	 */
	public List<AssetTemplate> findByAssetTypeAndUserByCreatedBy(AssetType assetType, User currentuser);

	/**
	 * Get {@code List} of {@link AssetTemplate} by workspace
	 * @param listOfWorkspaces
	 * @return
	 */
	public List<AssetTemplate> findByWorkspaceIn(List<Workspace> listOfWorkspaces);
	
	/**
	 * Get {@code List} of {@link AssetTemplate} by {@code set} of asset template id's
	 * @param idsOfAssetTemplate
	 * @return
	 */
	public List<AssetTemplate> findByIdIn(Set<Integer> idsOfAssetTemplate);
	/**
	 * Get {@code List} of {@link AssetTemplate} by asset template name, asset type and user created
	 * @param assetTemplateName
	 * @param assetType
	 * @param currentuser
	 * @return
	 */
	public List<AssetTemplate> findByAssetTemplateNameAndAssetTypeAndUserByCreatedBy(String assetTemplateName, AssetType assetType, User currentuser);
	/**
	 * Find all templates by asset class and in workspaces
	 * @param assetType
	 * @param listOfWorkspaces
	 * @return {@code List<AssetTemplate>}
	 */
	public List<AssetTemplate> findByAssetTypeAndWorkspaceIn(AssetType assetType, List<Workspace> listOfWorkspaces);
	/**
	 * Find By WorkspaceIn And DeleteStatus.
	 * @param listOfWorkspaces
	 * @param deleteStatus
	 * @return {@code List<AssetTemplate>}
	 */
	public List<AssetTemplate> findByWorkspaceInAndDeleteStatus(List<Workspace> listOfWorkspaces, boolean deleteStatus);
	/**
	 * Find all templates by list of asset class and in workspaces
	 * @param listOfAssetType
	 * @param listOfWorkspaces
	 * @return {@code List<AssetTemplate>}
	 */
	public List<AssetTemplate> findByAssetTypeInAndWorkspaceIn(List<AssetType> listOfAssetType, List<Workspace> listOfWorkspaces);
	
	/**
	 * Delete template by template id
	 * @param name
	 */
	@Modifying
	@Query(value = "Delete from AssetTemplate where id = ?1")
	public void deleteByAssetTemplateId(Integer assetTemplateId);
	
	/**
	 * Find Template by assetTypeId
	 * @param assetTypeId
	 * @return
	 */
	public List<AssetTemplate> findByAssetTypeId(Integer assetTypeId);
}

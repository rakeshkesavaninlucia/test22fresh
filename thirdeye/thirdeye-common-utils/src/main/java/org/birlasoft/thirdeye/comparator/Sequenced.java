package org.birlasoft.thirdeye.comparator;

/**
 * {@code Sequence Interface} for get the {@code sequence} {@code number}
 */
public interface Sequenced {
	
	/**
	 * Get the {@code sequence} {@code number}.
	 * @return {@code Integer}
	 */
	public Integer getSequenceNumber();
}

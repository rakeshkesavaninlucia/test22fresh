package org.birlasoft.thirdeye.config;

public abstract class EntityManagers {

	public static final String MASTERENTITYMANAGER = "masterEntityManager";
	
	public static final String TENANTENTITYMANAGER = "tenantEntityManager";
}

package org.birlasoft.thirdeye.constant;

/**
 * 
 * Enum Types for Widgets
 *
 */
public enum WidgetTypes {

	FACETGRAPH ("Show your data across various dimensions","col-md-6"), 
	FACETGRAPH3 ("Show your data across various dimensions (3*3)","col-md-4"),
	GROUPEDGRAPH ("Show your data in grouping across various dimensions","col-md-12");
	
	String description;
	String size;
	
	WidgetTypes(String description, String size) {
		this.description = description;
		this.size = size;
	}

	public String getDescription() {
		return description;
	}

	public String getSize() {
		return size;
	}
	
}



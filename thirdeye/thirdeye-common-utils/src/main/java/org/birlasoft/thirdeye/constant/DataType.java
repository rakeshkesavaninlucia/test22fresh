package org.birlasoft.thirdeye.constant;

/**
 * Data Type Enum for asset template column.
 * @author samar.gupta
 *
 */
public enum DataType {
	NUMBER("Number"), TEXT("Text"), DATE("Date");
	
	private final String value;       

    private DataType(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}
}

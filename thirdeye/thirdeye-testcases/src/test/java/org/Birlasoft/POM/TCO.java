package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TCO extends Util {
	
	

	public  TCO(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Available Cost Elements')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//*[@id='mod-module-costElement-1']/div[2]/span")
    public WebElement ClickCost_element;
	              
	       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Available Cost Elements";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	
	   	   	
	   	
	   	public void Click_Cost_element() {
	   		click_Method(ClickCost_element);
	   		
	   		System.out.println("Cost element created");
	       }
	
}

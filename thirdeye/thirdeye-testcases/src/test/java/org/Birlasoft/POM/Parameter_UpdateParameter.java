package org.Birlasoft.POM;



import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Parameter_UpdateParameter extends Util {
	
	
	public Parameter_UpdateParameter(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	/* Locators  */
	
	//page title
	@FindBy(xpath = "//span[contains(text(),'Update the Parameter')]")
	public WebElement UpdateParameter_pagetitle;
	
	
	//display name (mandatory)
	@FindBy(id = "displayName")
	public WebElement UpdateParameter_Displayname;
	
	//Description
	@FindBy(id = "description")
	public WebElement UpdateParameter_Description;
	
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement UpdateParameter_submitbtn;
	
	
	
	public String get_pagetitle()
	{
		String pageTitle = UpdateParameter_pagetitle.getText();
		
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper page or not
	public void pageetitle_verify()
	{
			try{
				String expectedTitle = "Update the Parameter";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	public void UpdateParameter_Displayname(String textbox) 
	{
			
		
				
		//calling the method settext	
		Set_Text(UpdateParameter_Displayname, textbox);
			
		System.out.println("Entered Template name is" +textbox);
			
	}
	
	//enter the template name

	public void UpdateParameter_Description(String textbox) 
	{
				
	     if(UpdateParameter_Description.isDisplayed())
					
	     //calling the method settext	
		 Set_Text(UpdateParameter_Description, textbox);
				
	     System.out.println("Entered Template name is" +textbox);
				
	}
	
	public void UpdateParameter_submitbtn()

	{
		if(UpdateParameter_submitbtn.isDisplayed())
			//calling click method
		click_Method(UpdateParameter_submitbtn);
		System.out.println("User click on Submit button");
	}

}


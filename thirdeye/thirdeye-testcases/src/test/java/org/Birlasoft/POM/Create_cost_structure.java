package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Create_cost_structure extends Util{

	public  Create_cost_structure(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Available Cost Structure')]")
    public WebElement verify_page_title;      
	
	@FindBy(xpath = "//a[contains(text(),'Create Cost Structure')]")
    public WebElement ClickCost_Structure;
	
	
	//@FindBy(xpath = "//*[@id='DataTables_Table_0']/tbody/tr")
	//public List<WebElement> AvaialbleCostStructure_ActionIcon;
	              
	       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Available Cost Structure";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	
	   	   	
	   	
	   	public void Click_Cost_Struct() 
	   	{
	   		
	   		click_Method(ClickCost_Structure);
	   	}
	   		
	    
	   	
	   	
	   	
	   	/*public void AvaialbleCostStructure_ActionIcon(String Cost_Element_Name)
	   	{
	   		
	   		WebTable_ActionColumn_click(AvaialbleCostStructure_ActionIcon, Cost_Element_Name);
	   		
	   		}*/
	   	
	   	
	
}

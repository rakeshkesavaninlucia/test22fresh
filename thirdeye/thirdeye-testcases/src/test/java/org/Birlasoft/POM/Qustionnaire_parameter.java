package org.Birlasoft.POM;



import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Qustionnaire_parameter extends Util {

	public  Qustionnaire_parameter (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	       
	       @FindBy(xpath = "//*[@id='mod-common-data-table-1']/div[2]/a[2]")
		     public WebElement Parameter_Create;
	       
	       @FindBy(xpath = "//div[@class='dataTables_filter']/following::input[1]")
		     public WebElement Parameter_Search;
	       
	       @FindBy(xpath = "//td[@class='sorting_1']")
		     public WebElement Click_checkbox;
	     //td[@class='sorting_1']
	     //*[@id='502']/td[1]/input
	       
	       @FindBy(xpath = "//*[@id='parameterSelectorId']/div/input")
		     public WebElement Click_done;
	       
	       @FindBy(xpath = "//a[@title='Tree View']")
		     public WebElement Click_Tree;
	       
	       @FindBy(xpath = "//button[contains(text(),'Close')]")
		     public WebElement Click_Close; 
		     
	       
	       @FindBy(xpath = "//a[@class='btn btn-default'][contains(text(),'Next')]")

		     public WebElement Click_next1;
	     
	       
	       
	       public void get_param() {
	    	   click_Method(Parameter_Create);
	       }
	  
	       public void Search_Param(String Search) throws InterruptedException
	       {
	    	   Webtable_Search(Parameter_Search,Search);
	        }
	      
	       public void get_clickcheck() {
	    	   click_Method(Click_checkbox);
	    	  
	       }
	       
	       public void get_clickdone() {
	    	   click_Method(Click_done);
	       }
	       
	      
	      public void get_clickTree() {
	    	   click_Method(Click_Tree);
	    	  //Webtable_Element_check(Click_Tree,Tree);
	       }
	       
	      public void get_clickClose() throws InterruptedException {
	    	   
	    	  
	    	   Popup_table_close(Click_Close);
		        }
	        
	       
	      public void get_clickNext() {
	    	   
		    	  
		        click_Method(Click_next1);
		        }
	
	}

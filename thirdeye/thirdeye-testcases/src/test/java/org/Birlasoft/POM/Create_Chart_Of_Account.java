package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Create_Chart_Of_Account extends Util{

	public  Create_Chart_Of_Account(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Create a New Chart Of Account')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//*[@id='name']")
    public WebElement Chart_of_account_text;
	
	@FindBy(xpath = "//*[@id='description']")
    public WebElement Chart_of_account_desc_text;
	
	@FindBy(xpath = "//span[@title='--Select--']")
    public WebElement Chart_of_account_dropdown;
	
	@FindBy(id = "select2-assetType.id-results")  //"//input[@class='select2-search__field']"
    public WebElement Chart_of_account_dropdown1;
	
	//@FindBy(xpath = "//span/input[@role='textbox']")
    //public WebElement Chart_of_account_dropdown2;
	              
	@FindBy(xpath = "//*[@value='Next']")
    public WebElement Click_next_button;
	              
	       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Create a New Chart Of Account";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	
	   	   //	public void Chart_of_account_method(String chart_OfAccount) {
			//Chart_of_account_name_text.sendKeys(chart_OfAccount);
			
			public void Chart_of_account_name(String Chart_ofacc) {
				Chart_of_account_text.sendKeys(Chart_ofacc);
	   		}
	
			public void Chart_of_account_Desc(String Chart_ofacc) {
				Chart_of_account_desc_text.sendKeys(Chart_ofacc);
	   		}
			
			public void Chart_of_account_Dropdown() {
				click_Method(Chart_of_account_dropdown);
	   		}
			
			public void Chart_of_account_Dropdown1(String value) throws InterruptedException {
				
					if(Chart_of_account_dropdown.isDisplayed())
						Thread.sleep(1000);
					Chart_of_account_dropdown.click();
					//calling the method ajax auto select
					AjaxAuto_select(Chart_of_account_dropdown1,value);//method(WebElement name , value)
					

				
	   		}
			
			
			
			public void Click_next() {
				click_Method(Click_next_button);
	   		}
	
}

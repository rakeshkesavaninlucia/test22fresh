package org.Birlasoft.POM;

import org.testng.annotations.BeforeMethod;
import java.util.Map;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Configuration_Relationship_Type;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;


public class TC_Check_Configuration extends Util{


	@BeforeMethod
	public void launchApp()
	{
		
		browser();
		
	}
	
	@Test(dataProvider="dp_Type",dataProviderClass=org.Birlasoft.DataProvider.DataProviderLoadData.class)
	public void CreateConfigurtaion(Map<String,String> createtemplate) throws InterruptedException
	{	// getting all the column name from excel i.e (key)
		String Username=createtemplate.get("Username");
		String Password=createtemplate.get("Password");
		String AccountId=createtemplate.get("Tenant_ID");
		String Workspace=createtemplate.get("Workspace");
		String ParentDescriptor=createtemplate.get("Parent Descriptor");
		String ChildDescriptor=createtemplate.get("Child Descriptor");
		String Direction=createtemplate.get("Direction");
	
		
		// create a object reference for the page objects involved in this scenario
		Login_page_object loginpage =new Login_page_object(driver);
		Home_PageObjecet homepage=new Home_PageObjecet(driver);
		Configuration_Relationship_Type createConfigurtaionpg = new Configuration_Relationship_Type(driver);
		
		//sequence of flow
		//1 .login into the application
		Thread.sleep(2000);
		loginpage.Login_username(Username);
		Thread.sleep(2000);
		loginpage.Login_Password(Password);
		Thread.sleep(2000);
		loginpage.Login_Accountid1(AccountId);
		Thread.sleep(2000);
		loginpage.Login_Submit();
		Thread.sleep(2000);
		
		//2.navigate to create dashboard page
		homepage.Homepage_Configuration();
		Thread.sleep(2000);
		homepage.Homepage_Configuration_RelationshipTypes();
		Thread.sleep(2000);
		
		//3.creating a Configurtaion
	
		createConfigurtaionpg.Create_Relationship_Type();
		Thread.sleep(2000);
		createConfigurtaionpg.Workspace_Dropdown("Workspace");
		Thread.sleep(2000);
		
		createConfigurtaionpg.get_Parent("Parent Descriptor");
		Thread.sleep(2000);
		createConfigurtaionpg.get_Child("Child Descriptor");
		Thread.sleep(2000);
		//4.Add a widget to your dashboard
		createConfigurtaionpg.Direction_Dropdown("Direction");
		Thread.sleep(2000);
		createConfigurtaionpg.Relationship_Type_Button();
		Thread.sleep(2000);
		
	}
	
	
	@AfterMethod
	public void teardown()
	{
		driver.quit();
	}
	
	
}	


	
	


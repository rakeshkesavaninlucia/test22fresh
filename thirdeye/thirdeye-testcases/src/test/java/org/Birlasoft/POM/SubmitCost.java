package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SubmitCost extends Util{

	public  SubmitCost(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Chart Of Account - Submit Response Management')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//span[contains(text(),'Create Chart Of Account')]")
    public WebElement Chart_of_account_click;
	
	@FindBy(xpath = "//Table[@id='DataTables_Table_0']/tbody/tr")
    public List<WebElement> Submit_cost_click_edit;
	
	@FindBy(xpath = "//*[@id='selectExport']/a[contains(text(),'Export')]")
    public WebElement Click_on_export_modal;
	              
	       
	       
public String get_PageTitle() throws InterruptedException {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	    	 //elementHighlight(verify_page_title);
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Chart Of Account - Submit Response Management";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	public void Chart_of_account_creation() throws InterruptedException {
				click_Method(Chart_of_account_click);
				elementHighlight(Chart_of_account_click);
	   		}
	   	
	   	//Method for clicking on edit icon
	   	
	   	public void Submit_Response_table(String cost_ele_table1) throws InterruptedException {
   			
	   		Web_Table_Click_edit(Submit_cost_click_edit,cost_ele_table1);
	   		
		   	}
	   	//Method for cliking on first column link
	   	
      public void Submit_Response_table1(String cost_str_link) throws InterruptedException {
   			
    	  Web_Table_Click_importExport(Submit_cost_click_edit,cost_str_link);
    	  }
      
      public void Submit_Response_export_modal() throws InterruptedException
          {
 			
          Popup_table_close(Click_on_export_modal);
    	  System.out.println("Clicked on modal export button ");
    	  
    	  }
	
}

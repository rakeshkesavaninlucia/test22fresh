package org.Birlasoft.POM;

import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Publish_chart_of_account extends Util {

	public  Publish_chart_of_account (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	
	
	        @FindBy(xpath = "//span[contains(text(),'Publishing your cost of account')]")
             public WebElement Publishing_TCO;
	
	        @FindBy(xpath = "//a[contains(text(),'Editable')]")
		     public WebElement Click_Edit_now; 
	        
	        @FindBy(xpath = "//a[contains(text(),'Review')]")
		    public WebElement Click_Review_now;
	        
	        @FindBy(xpath = "//a[contains(text(),'Published')]")
		     public WebElement Click_Publish_now;
	       
	       

	       
             public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = Publishing_TCO.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
             
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle = "Publishing your cost of account";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is"+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		
	   	}
	       
	   
	    //checking status
	   	public void verify_status() 
	   	
	   	{
	   		
	   		
	   		WebElement TCO_Status =  driver.findElement(By.xpath("//div/div[2]/span"));
	   	
	   		
	   		String TcoStatus = TCO_Status.getText();
	   		
	   	    switch(TcoStatus){  
	   	  case "PUBLISHED": 
	   	        click_Method(Click_Edit_now);
	   	    	System.out.println("The status of chart of account is Editable");	
	   	       break;  
	   	    
	   	    
	   	  case "EDITABLE": 
	   	    	click_Method(Click_Publish_now);
	   	    	System.out.println("The status of chart of account is changed as PUBLISHED");
	   	    	break; 
	   	      
	      case "REVIEWED": 
   	    	   click_Method(Click_Review_now);
   	    	   System.out.println("The status of chart of account is REVIEWED");
	   	  
	   	  default:
	   	       System.out.println("The Status of the Chart of account is Reviewed");  
	   	    	
	   	    	
	   	    }
	    	 
	   	}
	        
	   	
	   	public void get_clickPubCOA() {
	    	   
		    	  //Webtable_Element_check(Click_Tree,Tree);
		        //click_Method(Click_Publish_TCO);
		        }
}
	


package org.Birlasoft.Utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Browser {
	
	public static WebDriver driver;
	public static WebDriver browser(){
		
		System.out.println("Chrome browser");
		
		   String downloadFilepath = "D:\\3RDI\\Hybrid\\3rdeye-Hybrid";
		   
	    HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
	    /*
	    Hash Map Methods :
	    get(Object KEY) � 
	    This will return the value associated with specified key in this Java hashmap.
        
        
        put(Object KEY, String VALUE) � 
        This method stores the specified value and associates it with the specified key in this map.
	    
	    
	    
	    
	    
	    */
	    chromePrefs.put("profile.default_content_settings.popups", 0);
	    chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
	    HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
	    options.setExperimentalOption("prefs", chromePrefs);
	    options.addArguments("--test-type");
		//options.addArguments("chrome.switches","--disable-extensions");
		
	       DesiredCapabilities cap = DesiredCapabilities.chrome();
	       cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
	       cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	       cap.setCapability(ChromeOptions.CAPABILITY, options);

		System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
		System.setProperty("webdriver.chrome.logfile", "D:\\chromedriver.log"); 
		driver = new ChromeDriver(cap);
		 driver.manage().window().maximize();
		 driver.navigate().to("http://130.1.4.252:8080/home"); 
		return driver;
		
	}
	
	public static WebDriver AjaxAuto_select(WebElement autoOptions, String value)
	{
		WebDriverWait wait;
		wait = new WebDriverWait(driver, 5);
		
		
		
		wait.until(ExpectedConditions.visibilityOf(autoOptions));

		List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
		for(WebElement option : optionsToSelect){
			try {
		        
	        if(option.getText().equals(value)) {
	        	System.out.println("Trying to select: "+value);
	        	Thread.sleep(3000);
	            option.click();
	            Thread.sleep(2000);
	            return driver; 
		      } 
		}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
        }
		return driver; 
		
    }
	
	
	public static WebDriver Set_Text(WebElement settext,String value)
	
	{
		try
		{
			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(10,TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);
			waitforelement.until(ExpectedConditions.visibilityOf(settext));
	        WebElement SETTEXT= settext;

	        	String Existingtest=SETTEXT.getText();
	        	System.out.println("Text displayed in textbox is " +Existingtest);
	        	SETTEXT.clear();
	        	SETTEXT.sendKeys(value);
	        	System.out.println("Entered value in textbox is" +value );
	        
		}catch(NoSuchElementException e)
		    {
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
			
			}
	       
		return driver;
		
		
	}
	
	
	
	public static WebDriver Set_Number(WebElement settext,String value)
	
	{
		try
		{
			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(10,TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);
			waitforelement.until(ExpectedConditions.visibilityOf(settext));
	        WebElement SETTEXT= settext;
			//Changing the string value to integer 
			//int iTest = Integer.parseInt(value);
			//Convert string (with decimal or a point) to a integer Ex:1.0 to 1
			int i = Double.valueOf(value).intValue();
			SETTEXT.sendKeys(String.valueOf(i));
	        if(SETTEXT.getText().isEmpty())
	        {
	        	SETTEXT.sendKeys(value);
	        	System.out.println("Entered value in textbox is" +value );
	        }
	        //Highlighter.highLightElement(driver, SETTEXT);
	        
		}catch(NoSuchElementException e)
		    {
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
			
			}
	       
		return driver;
		
		
	}
	
	
	
	
	
	
	public static WebDriver click_Method(WebElement clickable)
	{
		try
		{
				FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(20, TimeUnit.SECONDS)
						.pollingEvery(10,TimeUnit.SECONDS)
						.ignoring(NoSuchElementException.class);
				waitforelement.until(ExpectedConditions.elementToBeClickable(clickable));
			WebElement CLICK= clickable;
			//Highlighter.highLightElement(driver, CLICK);
			CLICK.click();
			//System.out.println("element click");
			
		}catch(NoSuchElementException e)
	     {
		  e.printStackTrace();
		  throw new AssertionError("A clear description of the failure", e);
		
		 }
		
		
		return driver;
	
		
	}
	
	public static WebDriver Webtable_Element_check(WebElement Elementtobecheck , String value ) throws InterruptedException
	{
	  try
	  {
			
			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver)
					.withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(10,TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);
			waitforelement.until(ExpectedConditions.visibilityOf( Elementtobecheck));
		WebElement mytable =  Elementtobecheck;
		//Highlighter.highLightElement(driver, mytable);
		List<WebElement> rows_table=mytable.findElements(By.tagName("tr"));
		int row_count=rows_table.size();
		for(int row=0;row<row_count;row++)
		{
			//String rowtext=rows_table.get(row).getText();
			List<WebElement> columns_row=rows_table.get(row).findElements(By.tagName("td"));
		    int column_count=columns_row.size();
		    System.out.println("Number of cells In Row "+row+" are "/*+column_count*/);
		    for(int column=0;column<column_count;column++)
		    {
		    	
	
		    	String celtext=columns_row.get(column).getText();//System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
		    	System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);

		    	try 
				{	
		    	  if(celtext.equals(value))
		    	    {
		    		
		    		System.out.println("update name is "+value);
		    		JavascriptExecutor javascript = (JavascriptExecutor) driver; 
		    		javascript.executeScript("alert('Created Name is found on datatable');"); 
		    		Thread.sleep(2000);
		    		driver.switchTo().alert().accept();
		    		System.out.println(value+"is found on the datatable successfully ");
		    		//driver.switchTo().alert().dismiss();
		    		break;
		    	
		    	    }
		    	}catch(NoSuchElementException e)
			       {
		  		    e.printStackTrace();
		  		    throw new AssertionError("A clear description of the failure", e);
		  		
		  		   }
		    }
		}
	}catch (NoSuchElementException exc) 
	        {
			exc.printStackTrace();
			throw new AssertionError("A clear description of the failure", exc);
	        }
	return driver;
		
   }
	
   public static WebDriver closeBrowser()
	
	  {
	   try{
		driver.close();
	      }catch (NoSuchElementException exc) 
       {
		exc.printStackTrace();
		throw new AssertionError("A clear description of the failure", exc);
       }
	return driver;
		
	  }
   
   public static WebDriver QuitBrowser()
	
	  {
	   try{
		driver.quit();
	      }catch (NoSuchElementException exc) 
       {
		exc.printStackTrace();
		throw new AssertionError("A clear description of the failure", exc);
       }
	return driver;
		
	  }
   
   
   public static WebDriver Webtable_element_click(WebElement Elementtobeclick,String value ) throws InterruptedException
   {
	   try
		  {
				
				FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver)
						.withTimeout(60, TimeUnit.SECONDS)
						.pollingEvery(10,TimeUnit.SECONDS)
						.ignoring(NoSuchElementException.class);
				waitforelement.until(ExpectedConditions.visibilityOf( Elementtobeclick));
			WebElement mytable =  Elementtobeclick;
			//Highlighter.highLightElement(driver, mytable);
			List<WebElement> rows_table=mytable.findElements(By.tagName("tr"));
			int row_count=rows_table.size();
			for(int row=0;row<row_count;row++)
			{
				//String rowtext=rows_table.get(row).getText();
				List<WebElement> columns_row=rows_table.get(row).findElements(By.tagName("td"));
			    int column_count=columns_row.size();
			    System.out.println("Number of cells In Row "+row+" are "/*+column_count*/);
			    for(int column=0;column<column_count;column++)
			    {
			    	
		
			    	String celtext=columns_row.get(column).getText();//System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);
			    	System.out.println("Cell Value Of row number "+row+" and column number "+column+" Is "+celtext);

			    	try 
					{	
			    	  if(celtext.equals(value))
			    	    {
			    		
			    		System.out.println("update name is "+value);
			    		JavascriptExecutor javascript = (JavascriptExecutor) driver; 
			    		javascript.executeScript("alert('Created Name is found on datatable');"); 
			    		Thread.sleep(2000);
			    		driver.switchTo().alert().accept();
			    		columns_row.get(column).click();
			    		break;
			    	
			    	    }
			    	}catch(NoSuchElementException e)
				       {
			  		    e.printStackTrace();
			  		    throw new AssertionError("A clear description of the failure", e);
			  		
			  		   }
			    }
			}
		}catch (NoSuchElementException exc) 
		        {
				exc.printStackTrace();
				throw new AssertionError("A clear description of the failure", exc);
		        }
	   
	   
	   
	return driver;
	   
   }
   
   public static WebDriver Webtable_Search(WebElement search,String value) throws InterruptedException 
   {
	   
	   try
		{
			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(10,TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);
			waitforelement.until(ExpectedConditions.visibilityOf(search));
	        WebElement SETTEXT= search;
	        //Highlighter.highLightElement(driver, SETTEXT);

	        	String Existingtest=SETTEXT.getText();
	        	System.out.println("Text displayed in textbox is " +Existingtest);
	        	Thread.sleep(1000);
	        	SETTEXT.clear();
	        	SETTEXT.sendKeys(value);
	        	System.out.println("Entered value in textbox is" +value );
	        
		}catch(NoSuchElementException e)
		    {
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
			
			}
	   
	   
	return driver;
	   
	   
   }
   
   
   /*public static WebDriver Webtable_Excel_Read(String Pathname) throws IOException
   {
	 //following the basic hierarchy from File path->file(xlsx)->workbook->worksheet
		
		File xlsxfile = new File("Pathname");//value give the path of file download
		  
		FileInputStream file = new FileInputStream(xlsxfile);// load the file into the stream
		
		XSSFWorkbook WB = new XSSFWorkbook(file); // object WB is workbook
		
		XSSFSheet WS = WB.getSheet("Map"); // Object WS is Worksheet for class Xssfsheet
		
		// row and column count 
		int rownum = WS.getLastRowNum()+1;// because it always start first row as zero.
		
		int colnum = WS.getRow(0).getLastCellNum();
		
		//passing the row and column into two dimensional array
		
		String[][] data = new String[rownum][colnum]; //
		
		//for loop to iterate the above condition
		
		for (int i=0; i<rownum ;i++)
		{
			XSSFRow xlrow = WS.getRow(i);
			
			System.out.println("current row is"+xlrow); 
			
			for(int j=0; j<colnum; j++)
			{
				XSSFCell xlcell = xlrow.getCell(j,Row.CREATE_NULL_AS_BLANK);
				System.out.println("Current column is "+xlcell); 
				
				String finalvalue  = cellToString(xlcell);
				
				data[i][j]         = finalvalue;
				
				System.out.println("the value is " + finalvalue);
			}
		}
	return driver;
	   
   }
   
   
   public static String cellToString(XSSFCell cell) 
   {

       int type;
       Object result ;
       type = cell.getCellType();

       switch (type) {

           case 0 : // numeric value in Excel
               result = cell.getNumericCellValue() ;
               break ;
           case 1 : // String Value in Excel 
               result = cell.getStringCellValue() ;
               break ;
           case 3 : // Blank Cell 
           	result="";


           default :   
               throw new RuntimeException("There are no support for this type of cell") ;                      
       }

       return result.toString() ;
    }*/
   
   
   
   public static WebDriver QuestionType_operation(WebElement AjaxElement, String value)
	{
	   
		WebDriverWait wait;
		wait = new WebDriverWait(driver, 5);
		
		
		
		wait.until(ExpectedConditions.visibilityOf(AjaxElement));

		List<WebElement> optionsToSelect = AjaxElement.findElements(By.tagName("li"));
		for(WebElement option : optionsToSelect){
			
		        
	       // if(option.getText().equals(value)) {
	        	//System.out.println("Trying to select: "+value);
	        	//Thread.sleep(3000);
	           // option.click();
		if	(option.getText().equals(value))
		{
	            switch (value) {

	            case "Text" : // numeric value in Excel
	            	
	            	option.click();
	                break ;
	            case "Paragraph text" : // String Value in Excel 
	            	option.click();
	                break ;
	          //  case  : // Blank Cell 
	            //	result="";


	            default :   
	                throw new RuntimeException("There are no support for this type of cell") ;                      
	        
	            }
		//}catch(Exception e){

		     // throw new AssertionError("A clear description of the failure", e);
			}
   }
		return driver; 	
  }
   
   public static WebDriver Radiobutton_click(List<WebElement> Radio_clickable)
   {
	
	    List<WebElement> options = Radio_clickable ;
	    Random random = new Random();
	    int index = random.nextInt(options.size());
	    options.get(index).click();   
	   
	   
	   return driver;
	   
	   
   }
   
   public static WebDriver Dropdown_Text(WebElement dropdown,String visibletext)
   {
	   try
		{
		FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS)
					.pollingEvery(5,TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);
		waitforelement.until(ExpectedConditions.elementToBeClickable(dropdown));	
		Select dropdown1 = new Select(dropdown);
		dropdown1.selectByVisibleText(visibletext);
		
		}catch(NoSuchElementException e)
		{
		 System.out.println("Dropdown value is not visible");
		 e.printStackTrace();
		 throw new AssertionError("error is"+e);
		}
	   return driver;
	   
   }
   
   
   
   public static WebDriver Popup_table_Search(WebElement search,String value) throws InterruptedException 
   {
	   
	   try
		{
		    driver.switchTo().activeElement();
			//FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
			//		.pollingEvery(10,TimeUnit.SECONDS)
			//		.ignoring(NoSuchElementException.class);
			//waitforelement.until(ExpectedConditions.visibilityOf(search));
			
			Thread.sleep(3000);
	        WebElement SETTEXT= search;
	        //Highlighter.highLightElement(driver, SETTEXT);
	        
	        SETTEXT.sendKeys(value);
		}catch(NoSuchElementException e)
		    {
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
			
			}
	   
	   
	return driver;
	   
	   
   }
   
   public static WebDriver Popup_table_Click(WebElement clickable1) throws InterruptedException
   {
	   
	   try
		{
		    
			WebElement CLICK= clickable1;
			driver.switchTo().activeElement();
			Thread.sleep(3000);
			CLICK.click();
			//System.out.println("element click");
			
		}catch(NoSuchElementException e)
	     {
		  e.printStackTrace();
		  throw new AssertionError("A clear description of the failure", e);
		
		 }
		
		
		return driver;
	   
   }
   
   
   public static WebDriver Popup_Set_text(WebElement settext,String value)
   {
	   try
		{
		    driver.switchTo().activeElement();
			FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS)
					.pollingEvery(10,TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);
			waitforelement.until(ExpectedConditions.visibilityOf(settext));
	        WebElement SETTEXT= settext;

	        	String Existingtest=SETTEXT.getText();
	        	System.out.println("Text displayed in textbox is " +Existingtest);
	        	SETTEXT.clear();
	        	SETTEXT.sendKeys(value);
	        	System.out.println("Entered value in textbox is" +value );
	        
		}catch(NoSuchElementException e)
		    {
			e.printStackTrace();
			throw new AssertionError("A clear description of the failure", e);
			
			}
	       
		return driver;
   }
   
   public static WebDriver Popup_Delete_Alert(WebElement Delete_click) throws InterruptedException
   {
	            // Switching to Alert    
				//driver.findElement(this.getObject(p,objectName,objectType)).click();
	            //Thread.sleep(1000);
	       try{ 
	    	   
				FluentWait<WebDriver> waitforelement = new FluentWait<WebDriver>(driver).withTimeout(20, TimeUnit.SECONDS)
						.pollingEvery(10,TimeUnit.SECONDS)
						.ignoring(NoSuchElementException.class);
				waitforelement.until(ExpectedConditions.elementToBeClickable(Delete_click));
			    WebElement CLICK= Delete_click;
			//Highlighter.highLightElement(driver, CLICK);
			    CLICK.click();
			    Thread.sleep(3000);
				driver.switchTo().alert().accept();    
	                     
	            // Capturing alert message.    
	           // String alertMessage=driver.switchTo().alert().getText();        
	                     
	            // Displaying alert message     
	           // System.out.println(alertMessage);           
	                     
	            // Accepting alert  
	            //Thread.sleep(2000);
	            //alert.accept(); 
	          }catch(NoSuchElementException e)
		     {
				  e.printStackTrace();
				  throw new AssertionError("A clear description of the failure", e);
				
			 }
		return driver;

	       
	   
	   
   }
   
   public static void fn_CheckVisibility_Button(WebElement ElementToClick)
   {
   		fn_WaitForVisibility(ElementToClick, 50);
   		boolean status=ElementToClick.isEnabled();
   		if(status==true){
   		ElementToClick.click();	
   	}else
   		{
   		  System.out.println("Button is Not Enable");
   		}	
   }
   
   public static void fn_WaitForVisibility(WebElement element, int Timeout)
   	
   {
		WebDriverWait WaitObj=new WebDriverWait(fn_Driverobj(),Timeout);
		WaitObj.until(ExpectedConditions.visibilityOf(element));
   }
   
   public static WebDriver fn_Driverobj()
  
   {
     return  driver;
   }


   
   
  //error handling if name properly not entered
  /* public static void Popup_Error_verify() throws InterruptedException
   {
	   	Thread.sleep(1000);
	    //driver.switchTo().activeElement();
	    WebElement Error_name = driver.findElement(By.xpath("//div[@class='form-group']/span")); 
	    String pageTitle = Error_name.getText();
	   
			if(pageTitle.isEmpty())
			{
				System.out.println("User successfully entered the name ");
				
		
			}   
			else{
				System.out.println("User not enter the name on textbox ");
				System.out.println("Displaying error is "+pageTitle);
			}
	   
	   fn_Driverobj();
	   
   }*/
   
   
   public static void Popup_title_verify(WebElement title,String Expectedtitle) throws InterruptedException
   {
	   Thread.sleep(1000);
	   driver.switchTo().activeElement();
	    WebElement Error_name = title ; 
	    String popuptitle = Error_name.getText();
	    try{
	    if(popuptitle.equals(Expectedtitle))
	    {
	    	System.out.println("User landed on proper page"+popuptitle);
	    }
	    
	    }catch(Exception e)
	    {
			  throw new AssertionError("A clear description of the failure", e);

	    }
	   
	   fn_Driverobj();
   }
   
   public static void Unordered_List_Click(List<WebElement> List_name,String Listname)
   {
		 List<WebElement> liList1 =List_name;
		 
		 int listcount=liList1.size();
		 for(int list=0;list<listcount;list++)
		 {
			 System.out.println("Number of List "+listcount);
			try
			{
			 String listtext=liList1.get(list).getText();
			 if(listtext.equals(Listname))
			 {
				 
				 liList1.get(list).click(); 
				 Thread.sleep(2000);
				 System.out.println("Value found"+listtext);
				 //Highlighter.highLightElement(driver,click);
			 }
			}catch(Exception e){
	    		e.printStackTrace();
				System.out.println("value not found in this li");
				break;
				}
		 }
	   
   }
   
   
   public static void Change_Workspace(String Workspace_Name) throws InterruptedException
   {
	   
	   WebElement workspace_click=driver.findElement(By.xpath("//ul/li/a/span[@class='active-workspacename']"));
	   fn_CheckVisibility_Button(workspace_click);
	   System.out.println("User clicked on workspace management");
	   Thread.sleep(1000);
	   List<WebElement> liList2 = driver.findElements(By.xpath("//ul/li/a[@data-type='workspaceSwitch']/h4"));
		 
		 int listcount1=liList2.size();
		 for(int list=0;list<listcount1;list++)
		 {
			 System.out.println("Number of List "+listcount1);
			try
			{
			 String listtext=liList2.get(list).getText();
			 if(listtext.equals(Workspace_Name))
			 {
				 
				 liList2.get(list).click(); 
				 System.out.println("Value found"+listtext);
				 //Highlighter.highLightElement(driver,click);
				 Alert alert1=driver.switchTo().alert();      
               
		            // Capturing alert message.    
		            String alertMessage1=driver.switchTo().alert().getText();        
		                     
		            // Displaying alert message     
		            System.out.println(alertMessage1);           
		                     
		            // Accepting alert      
		            alert1.accept();    
		            break;
			 }
			}catch(Exception e){
	    		e.printStackTrace();
				System.out.println("value not found in this li");
				break;
				}
		 }
		 
	   
   }
   
 public static void AutoIt_FileUpload(WebElement ElementToClick, String wordpath ) throws IOException, InterruptedException
 {
	 fn_CheckVisibility_Button( ElementToClick);
	Process P = Runtime.getRuntime().exec(wordpath);
	 P.waitFor();
 }
 
 public static void setClipboardData(String string) 
 {
	    StringSelection stringSelection = new StringSelection(string);
	    Toolkit.getDefaultToolkit().getSystemClipboard()
	             .setContents(stringSelection, null);
 }
 
 
 
 public static void File_Upload_Page(String filepath) throws AWTException
 {
	 
	 setClipboardData(filepath);

	 
	  try {
		  Robot robot = new Robot();
		  robot.keyPress(KeyEvent.VK_CONTROL);
		  robot.keyPress(KeyEvent.VK_V); 
		  robot.keyRelease(KeyEvent.VK_V); 
		  robot.keyRelease(KeyEvent.VK_CONTROL); 
		  robot.keyPress(KeyEvent.VK_ENTER); 
		  robot.keyRelease(KeyEvent.VK_ENTER); 
		  
		  } catch (AWTException e) 
	  {
		         e.printStackTrace();
	  }
	 
	 
 }
   
}






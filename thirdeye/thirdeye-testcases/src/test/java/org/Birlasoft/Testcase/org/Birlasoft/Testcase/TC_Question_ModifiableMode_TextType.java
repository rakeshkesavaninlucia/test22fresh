package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.io.IOException;

import org.Birlasoft.POM.Create_Question;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.View_Question;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Question_ModifiableMode_TextType extends Util{

	public String x= "What is end date"+java.time.LocalDateTime.now();
	
	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}

  @Test
  (priority = 0)
  public void f() throws InterruptedException 
  {
	
	//logging in Application
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			Thread.sleep(1000);
			obj1.Login_username("manoj.shrivas@birlasoft.com");
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
	
}
 
  /**
   * Selecting question management from side bar
   *
   **/

  @Test(priority = 1)
  public void create_template() throws InterruptedException
  {
	  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		obj2.Homepage_titleverify("Portfolio Home");
	    obj2.Homepage_QuestionManagement();
	    obj2.Homepage_QuestionManagement_CreateQuestion();
	    
		 }
  
 
  /**
   *Creating new question
   *Mode = Modifiable
   *Type = Text
   **/
  
  
 @Test(priority=2)
  public void Create() throws InterruptedException
 {
  
  Create_Question obj4 = new Create_Question(driver);
  
  obj4.getWrkSpc();
  obj4.getWrkSpcSelect("testing_workspace_1.9");
  obj4.getMode();
  obj4.getMode1("MODIFIABLE");
  obj4.getcategory();
  obj4.getcategory1("Functional Assessment");
  obj4.getdisName("Dis_name_"+java.time.LocalDateTime.now());
  System.out.println("Dis_name_"+java.time.LocalDateTime.now());
  obj4.get_title(x);
  obj4.getHelpText("Help");
  obj4.getQuestion_type1("Text");
  obj4.complete_creation(); 
  
  }
 
 /**
  * Checking newly created question in available question page
  * 
  * **/
 
 @Test(priority=3)
 public void Check_Question_Availibility() throws InterruptedException
{
 
	View_Question obj5 = new View_Question(driver);
   obj5.get_PageTitle();
   obj5.search_Question();
   obj5.Click_input(x);
   obj5.Click_input1();

  }
	
}

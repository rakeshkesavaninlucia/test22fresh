package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.awt.AWTException;
import java.io.IOException;

import org.Birlasoft.POM.Add_CostStructTO_COA;
import org.Birlasoft.POM.Chart_of_account;
import org.Birlasoft.POM.Create_Chart_Of_Account;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Select_Asset_Type;
import org.Birlasoft.POM.Select_CostStructure_Modal;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Create_ChartOfAccount extends Util {

	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}
	
	
	  @Test
	  (priority = 0)
	  public void login_page() throws InterruptedException  {
		
		  
		
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			Thread.sleep(1000);
		    obj1.Login_username("manoj.shrivas@birlasoft.com");
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
  }

	  
 @Test(priority = 1)
	  
	  public void Chart_of_account_tab() throws InterruptedException
	  {
		  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
			obj2.Homepage_titleverify("Portfolio Home");
			
			obj2.Homepage_TCOManagement();
			obj2.Homepage_TCOManagement_ChartOfAccount();
		}
	
	  
	  
	  @Test(priority = 2 )
	  public void Available_chart_of_account() throws InterruptedException, AWTException
	  {
	  
		  Chart_of_account objChartofAccount = new Chart_of_account (driver);
	 	  
		  objChartofAccount.get_PageTitle();
		  objChartofAccount.pageetitle_verify();
		  //objChartofAccount.Chart_of_account_method("Chart_no_555");
		  objChartofAccount.Chart_of_account_creation();
		  
	 	 	}
	  
	  @Test(priority = 3 )
	  public void Creation_chart_of_account() throws InterruptedException, AWTException
	  {
	  
		  Create_Chart_Of_Account objChartofAccount = new Create_Chart_Of_Account (driver);
		  
		  objChartofAccount.get_PageTitle();
		  objChartofAccount.pageetitle_verify();
		  objChartofAccount.Chart_of_account_name("Chart_of account_testing4");
		  objChartofAccount.Chart_of_account_Desc("Chart_of account_desc");
		  //objChartofAccount.Chart_of_account_Dropdown();
		  Thread.sleep(5000);
		  objChartofAccount.Chart_of_account_Dropdown1("Database");
		  Thread.sleep(2000);
		  objChartofAccount.Click_next();
		  Thread.sleep(2000);
		  
		 Select_Asset_Type c;
	  c = new Select_Asset_Type(driver);
	  
	  Thread.sleep(10000);
	  c.Select_Search("DB");
	  c.Select_Click();
	  Thread.sleep(2000);
	  c.Delete_Click();
	  c.Select_Search1("Mysql");
	  c.Select_Click1();
	  Thread.sleep(2000);
	  c.Delete_Click();
	  c.Click_Next();
	  }
	  
	  @Test(priority = 4 )
	  public void Add_coststructure() throws InterruptedException, AWTException
	  {
	  
		  Add_CostStructTO_COA objCoststructMod = new Add_CostStructTO_COA (driver);
		  
		  objCoststructMod.get_PageTitle();
		  objCoststructMod.pageetitle_verify();
		  objCoststructMod.Chart_of_account_creation();
		 
		  
		
	  }
	  
	  @Test(priority = 5 )
	  public void Select_CostStructure() throws InterruptedException, AWTException
	  {
	  
		  Select_CostStructure_Modal objCoststructMod = new Select_CostStructure_Modal (driver);
		  objCoststructMod.Cost_StructureModal();
		  objCoststructMod.get_PageTitle();
		  objCoststructMod.pageetitle_verify();
		  
		  objCoststructMod.Click_Cost_Struct("Finance");
		  objCoststructMod.AvaialbleCostStructure_ActionIcon();
		  Thread.sleep(2000);
		  objCoststructMod.Click_next_button();
		  
		
	  }
	  
	
}

package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.util.Map;

import org.Birlasoft.POM.Create_Questionnaire;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.TemplateManagementAssetViewPage;
import org.Birlasoft.POM.Template_CreateTemplate_PageObject;
import org.Birlasoft.POM.Template_Inventory;
import org.Birlasoft.POM.Template_Inventory_AddAsset;
import org.Birlasoft.POM.Template_ViewTemplate_Pageobject;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Us2120Tc003DeleteAssetVerifyinQuestionniare extends Util
{
	@BeforeMethod
	public void launchApp()
	{
		
		browser();
		
	}
	
	@Test(dataProvider="dp_us2120Tc3",dataProviderClass=org.Birlasoft.DataProvider.DataProviderLoadData.class)
	public void createTemplateandAsset(Map<String,String> addAsset) throws InterruptedException
	{
		String Username=addAsset.get("Username");
		String Password=addAsset.get("Password");
		String AccountId=addAsset.get("Tenant_ID");
		String Order_ID=addAsset.get("Order_Set");
		String TC_ID=addAsset.get("TC_ID");
		String Asset_Type=addAsset.get("Asset_Type");
		String Asset_Column1type=addAsset.get("Asset_Column1type");
		String Asset_Column1=addAsset.get("Asset_Column1");
		String Asset_column1Response1=addAsset.get("Asset_column1Response1");
		String Asset_column1Response2=addAsset.get("Asset_column1Response2");
		String Workspace=addAsset.get("Workspace");
		String QuestionniareTitle =addAsset.get("QuestionniareTitle");
		String QuestionniareName = addAsset.get("Questionniare_Name");
		String Questionniare_Desc = addAsset.get("Questionniare_Desc");
		
		// create a object reference for the page objects involved in this scenario
		Login_page_object loginpage =new Login_page_object(driver);
		Home_PageObjecet homepage=new Home_PageObjecet(driver);
		Create_Questionnaire questionniare= new Create_Questionnaire(driver);
		
		//sequence of flow
		//1 .login into the application
		Thread.sleep(2000);
		loginpage.Login_username(Username);
		loginpage.Login_Password(Password);
		loginpage.Login_Accountid1(AccountId);
		loginpage.Login_Submit();
		Thread.sleep(2000);
		
		//2.navigate to create template page
		homepage.Homepage_Workspace_change(Workspace);
		Thread.sleep(2000);
		homepage.Homepage_Configuration();
		homepage.Homepage_QuestionnaireManagement();
		homepage.Homepage_QuestionnaireManagement_CreateQuestionnaire();
		questionniare.questionniareTitleVerify(QuestionniareTitle);
		questionniare.get_Name(QuestionniareName);
		questionniare.get_Desc(Questionniare_Desc);
		questionniare.select_asset_type();
		questionniare.select_asset_type(Asset_Type);
		questionniare.get_Next();
	
		//viewAssetData.assetVerify(Asset_column1Response2);
	}

}

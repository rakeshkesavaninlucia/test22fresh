package org.Birlasoft.Testcase.org.Birlasoft.Testcase;


import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.Birlasoft.POM.Create_Color_Scheme_Add_EditCondition;
import org.Birlasoft.POM.Create_Color_Scheme_QualityGate;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.Assert;
/**
* @author meghna.agarwal
*
*/


/**
*Opening Browser and navigating to the required URL
*
*/
public class TC_Color_Scheme extends Util{

	
    SoftAssert verify = new SoftAssert();//soft assertion object creation.
    
    @BeforeClass(description = "navigating to URL")
    public void browserinit() throws IOException {
                    // intialization of the browser
                    Util.browser();
                    String URL = Util.driver.getCurrentUrl();
                    Assert.assertTrue(URL.contains("http://10.193.146.223:8080/thirdeye-web/login"));
//@BeforeClass
//public void browserinit() throws IOException
//{
//	//Initialization of the browser
//	Util.browser();
//
//}
    }

     @Test(priority = 0 ,description="Login to the application")

     public void f() throws InterruptedException  {

	//login page
	
	Login_page_object objq = new Login_page_object(driver);
	objq.verifyLogInPageTitle();
    Util.Wait_sleep();
	objq.Login_username("meghna1.agarwal@birlasoft.com");
    Util.Wait_sleep();
	objq.Login_Password("juhi@08");
    Util.Wait_sleep();
	objq.Login_Accountid1("qadm");
    Util.Wait_sleep();
	objq.Login_Submit();
//	driver.navigate().to("http://130.1.4.252:8080/home"); 
	objq.Login_ErrorMsg();
    Util.Wait_sleep();
	
	
     }	

     @Test(priority =1,description="Clicking on Template management")
     public void create_template() throws InterruptedException
     {
      Home_PageObjecet obj2 = new Home_PageObjecet(Util.driver);
      obj2.Homepage_titleverify("Portfolio Home");
      obj2.Homepage_Workspace_change("Sprint_1.14");
	  Thread.sleep(1000);                         
      obj2.Homepage_Configuration();
      Util.Wait_sleep();
      obj2.Homepage_ColorScheme();
      obj2.Homepage_ColorScheme_QualityGates();
     }
     

    //calling all objects
     @Test(priority = 2, description="Clicking on Color Scheme")
     public void Navigate_Color_Scheme() throws InterruptedException
     {
	 Home_PageObjecet obj3 = new Home_PageObjecet(Util.driver);
	 obj3.Homepage_titleverify("Color_Scheme");
	 verify.assertTrue(obj3.Homepage_ColorScheme.isDisplayed(), "Color Scheme not available");
	 obj3.Homepage_ColorScheme();
	 Util.Wait_sleep();
	 verify.assertTrue(obj3.Homepage_ColorScheme_QualityGates.isDisplayed(),"Quality Gates not available");
	 obj3.Homepage_ColorScheme_QualityGates();
     }


 
    @Test(priority = 3,description="Clicking on Quality Gate")
    public void Naviagate_Create_QualityGate()
    {
    	
	Create_Color_Scheme_QualityGate obj4 = new Create_Color_Scheme_QualityGate(Util.driver);
    verify.assertTrue(obj4.Create_Quality_Gate_Page_Button.isDisplayed(), "Quality Gate Button not available"); 
	obj4.Create_Quality_Gate_Page_Button();
	 
    }

   @Test(priority = 4,description="Clicking on Quality gate Popup")
   public void Quality_Name()throws Exception
{
	Create_Color_Scheme_QualityGate obj5 = new Create_Color_Scheme_QualityGate(driver);
    Util.Wait_sleep();

//  verify.assertTrue(obj4.Quality_Gate_Name.isSelected(), "Quality Gate Button not available"); 
	obj5.get_Text("ColorNamen");
    Util.Wait_sleep();
    obj5.Create_Quality_Gate_Page_Button1();
	 
}
   @Test(priority = 5,description="Moving to Add/Edit Condition page")
   public void Add_Edit_condition() throws Exception
{
	Create_Color_Scheme_Add_EditCondition obj6 = new Create_Color_Scheme_Add_EditCondition(driver);
	  Thread.sleep(1000);
	  obj6.Color_Scale_Dropdown("3");
	    Util.Wait_sleep();
	  obj6. get_Text("Low");
	    Util.Wait_sleep();
	  obj6. get_Name("Medium");
	    Util.Wait_sleep();
	  obj6. get_Value("High");
	    Util.Wait_sleep();
	  obj6.Actions_Save_Button1();
	    Util.Wait_sleep();
	    
//	    verify.assertTrue(obj5.Parameter_dropdown1.isDisplayed(), "Select parameter type dropdown is not present");
//      verify.assertTrue(obj5.Parameter_dropdown1.getText().contains("Application"), obj5.Parameter_dropdown1.getText()+" Knowledge Management is not available in dropdown");
      obj6.Parameter_Dropdown1("Knowledge Management");
      Util.Wait_sleep();
	  obj6.Add_Row();
	    Util.Wait_sleep();
	  obj6. get_Text("2");
	    Util.Wait_sleep();
	  obj6. get_Name("3");
	    Util.Wait_sleep();
      obj6.get_Weight("1");
      Util.Wait_sleep();
	  obj6.Actions_Pull_Left();
	    Util.Wait_sleep();
	  obj6.Actions_Delete();
	    Util.Wait_sleep();
}


}
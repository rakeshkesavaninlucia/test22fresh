package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.io.IOException;

import org.Birlasoft.POM.BenchMark_AvailableBenchmark_Page;
import org.Birlasoft.POM.BenchMark_Item_Score_Create;
import org.Birlasoft.POM.BenchMark_Create_Page;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
* @author meghna.agarwal
*
*/

public class TC_BenchMark extends Util{
	@BeforeClass
	public void browserinit() throws IOException
	{
		//Initialization of the browser
		Util.browser();
		
	}
	
    @Test(priority = 0, description="Login to the application")
    public void f() throws InterruptedException  {


		//login page
		
		Login_page_object objq = new Login_page_object(driver);
		objq.verifyLogInPageTitle();
		Thread.sleep(2000);
		objq.Login_username("meghna1.agarwal@birlasoft.com");
		Thread.sleep(2000);
		objq.Login_Password("juhi@08");
		Thread.sleep(2000);
		objq.Login_Accountid1("qadm"); 
		Thread.sleep(2000);
		objq.Login_Submit();
        objq.Login_ErrorMsg();
		Thread.sleep(2000);
		
		
	     }	
    //calling all objects
    @Test(priority = 1, description="Clicking on Dashboard")
    public void Naviagate_To_Parameter() throws Exception
	{
		 Home_PageObjecet obj2 = new Home_PageObjecet(driver);
		 obj2.Homepage_titleverify("Portfolio Home");
		 obj2.Homepage_Workspace_change("Sprint_1.14");
		 Thread.sleep(1000);
		 obj2.Homepage_Configuration();
		 obj2.HomePage_BenchMark();
		 obj2.HomePage_BenchMark_View();
	}
    
  
    @Test(priority = 2, description="Clicking on Benchmark")
    public void Naviagate_To_BenchMark()
	{
    	
    	BenchMark_AvailableBenchmark_Page obj3 = new BenchMark_AvailableBenchmark_Page(driver);
		 obj3.Create_BenchMark();
		
	}
	
    
    @Test(priority = 3, description="Clicking on Create Benchmark  page")
    public void Naviagate_To_BenchMark1() throws Exception
	{
    	 BenchMark_Create_Page obj4 = new BenchMark_Create_Page(driver);
    	 
    
//		     obj4.BenchMark_Workspace_Dropdown();
		     obj4.BenchMark_Workspace_Dropdown("Sprint_1.14");
			 obj4.get_Desc("project_undercover1");
	       	 Thread.sleep(1000);
			 obj4.BenchMark_Button();
			 Thread.sleep(1000);
			 obj4.BenchMark_Button();
			 Thread.sleep(1000);
			 obj4.get_Text("parameter");
			 Thread.sleep(1000);
			 obj4.get_Version("server_1");
			 Thread.sleep(1000);
			 obj4.get_Display("configuration");
             obj4.Actions_Save_Button();
			 Thread.sleep(1000); 
//			 obj4.Actions_Delete_Button();
//			 Thread.sleep(1000);
//			 obj4.fn_CheckVisibility_Button();
//			 Thread.sleep(1000); 
             obj4.Actions_Row_Button();
			 Thread.sleep(1000);
			  
	}
	

        @Test(priority = 4, description="Clicking on Add/edit Item score")
        public void Naviagate_To_BenchMark3() throws Exception
	    {
        BenchMark_Item_Score_Create obj5 = new BenchMark_Item_Score_Create(driver);
 
         obj5.BenchMark_Button();
	     Thread.sleep(1000);
	     obj5.get_Text("03/28/2017");
       	 Thread.sleep(2000);
	     obj5.get_Text1("09/30/2017");
	     Thread.sleep(2000);
         obj5.get_Score("7.5");
	     Thread.sleep(2000);
         obj5.Actions_Benchmark_Save_Button();
	     Thread.sleep(1000);
    //	 obj5.Actions_Benchmark_Edit_Button();
   //	 Thread.sleep(1000);
	     obj5.Actions_Delete_Button();
	     Thread.sleep(1000);
	      }
        }


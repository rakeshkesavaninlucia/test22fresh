package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.awt.AWTException;
import java.io.IOException;

import org.Birlasoft.POM.Available_CStruct_edit;
import org.Birlasoft.POM.Cost_structure_search;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TC_Edit_Cost_Structure extends Util{

	
	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}
	
	
	  @Test
	  (priority = 0)
	  public void login_page() throws InterruptedException  {
		
		  
		
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			Thread.sleep(1000);
		    obj1.Login_username("manoj.shrivas@birlasoft.com");
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
  }
	
	  @Test(priority = 1)
	  
	  public void Cost_structure_tab() throws InterruptedException
	  {
		  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
			obj2.Homepage_titleverify("Portfolio Home");
			
			obj2.Homepage_TCOManagement();
			obj2.Homepage_TCOManagement_CostStructure();
		}
	
	
	  //Verifying the edit icon on the available cost structure page
	   @Test(priority = 3 )
	   
	    public void Edit_CostStructure_icon() throws InterruptedException, AWTException
	    {
	    
	 	   Available_CStruct_edit  objCstructEditicon = new Available_CStruct_edit (driver);
	   	  
	   	  //objCstructMod.Cost_StructureModal();
	   	  Thread.sleep(2000);
	   	objCstructEditicon.get_PageTitle();
	   	  Thread.sleep(2000);
	   	objCstructEditicon.pageetitle_verify();
	   	objCstructEditicon.Search_Cost_Structure("CostStructure_name_40");
	   	 Thread.sleep(2000);
	   	objCstructEditicon.Click_Edit_Cost_Structure();
	   	//Create_CostStructure_Modal();
	   	
	   
	    }
	  
	  
	  
	  
	 
	
	
}
